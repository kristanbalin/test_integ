-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2022 at 04:48 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homies`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts_receivable_ledgers`
--

CREATE TABLE `accounts_receivable_ledgers` (
  `id` char(36) NOT NULL,
  `inpatient_bill_id` char(36) NOT NULL,
  `inpatient_payment_id` varchar(36) NOT NULL,
  `amount_outstanding` float NOT NULL DEFAULT 0,
  `days_overdue` decimal(10,0) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Pending',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `budget_plan`
--

CREATE TABLE `budget_plan` (
  `id` char(36) NOT NULL,
  `given_budget` float NOT NULL,
  `total_spent` float NOT NULL,
  `year` varchar(255) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `status` varchar(255) NOT NULL,
  `department_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` char(36) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` char(36) NOT NULL,
  `department_name` varchar(255) NOT NULL,
  `department_head` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `department_name`, `department_head`, `contact_no`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
('3633562d-a91b-49f4-bc1a-143236f60947', 'AP AR', 'string', 'string', 'active', NULL, NULL, '2022-03-04 11:18:43', '2022-03-04 11:18:43'),
('58ddb697-8db0-403b-a945-4e618c9ed876', 'Human Capital', 'string', 'string', 'active', NULL, NULL, '2022-03-04 11:19:19', '2022-03-04 11:19:19'),
('dbfbb48e-6d41-4dbe-9f13-70343f5a6384', 'Procurement', 'string', 'string', 'active', NULL, NULL, '2022-03-04 11:18:55', '2022-03-04 11:18:55'),
('e7730bc6-3c34-42be-b7dd-3076834d2c93', 'Collection Disbursement', 'string', 'string', 'active', NULL, NULL, '2022-03-04 11:19:08', '2022-03-04 11:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `discharge_management`
--

CREATE TABLE `discharge_management` (
  `discharge_id` varchar(255) NOT NULL,
  `discharge_no` varchar(255) NOT NULL,
  `patient_id` varchar(255) DEFAULT NULL,
  `admission_id` varchar(255) DEFAULT NULL,
  `reason_of_admittance` varchar(255) NOT NULL,
  `diagnosis_at_admittance` varchar(255) NOT NULL,
  `date_admitted` datetime DEFAULT NULL,
  `treatment_summary` varchar(255) NOT NULL,
  `discharge_date` datetime DEFAULT NULL,
  `physician_approved` varchar(255) NOT NULL,
  `discharge_diagnosis` varchar(255) NOT NULL,
  `further_treatment_plan` varchar(255) NOT NULL,
  `next_check_up_date` date DEFAULT NULL,
  `client_consent_approval` varchar(255) NOT NULL,
  `active_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `discount_privillages`
--

CREATE TABLE `discount_privillages` (
  `dp_id` varchar(36) NOT NULL,
  `ph_id` varchar(255) DEFAULT NULL,
  `end_of_validity` varchar(255) DEFAULT NULL,
  `sc_id` varchar(255) DEFAULT NULL,
  `municipality` varchar(255) DEFAULT NULL,
  `pwd_id` varchar(255) DEFAULT NULL,
  `type_of_disability` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `doctor_fee_bill`
--

CREATE TABLE `doctor_fee_bill` (
  `id` char(36) NOT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `invoice_date` datetime NOT NULL,
  `inpatient_bill_id` varchar(36) DEFAULT NULL,
  `doctor_id` varchar(36) NOT NULL,
  `actual_pf` float NOT NULL,
  `sc_pwd_discount` float DEFAULT NULL,
  `philhealth` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `hmo` float DEFAULT NULL,
  `patient_due` float NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `doctor_profile`
--

CREATE TABLE `doctor_profile` (
  `doctor_id` char(36) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `label` varchar(5) NOT NULL,
  `doctor_first_name` varchar(255) NOT NULL,
  `doctor_middle_name` varchar(255) DEFAULT NULL,
  `doctor_last_name` varchar(255) NOT NULL,
  `doctor_home_address` varchar(255) DEFAULT NULL,
  `doctor_location` varchar(255) DEFAULT NULL,
  `doctor_mobile` varchar(255) DEFAULT NULL,
  `doctor_schedule` varchar(255) DEFAULT NULL,
  `specialization_id` char(36) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` char(36) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `extension_name` varchar(255) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `birthplace` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `civil_status` varchar(255) NOT NULL,
  `house_number` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `barangay` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `job` varchar(255) NOT NULL,
  `hire_date` date NOT NULL,
  `manager` varchar(255) NOT NULL,
  `department_id` char(36) DEFAULT NULL,
  `employee_type_id` char(36) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active',
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `photo`, `first_name`, `middle_name`, `last_name`, `extension_name`, `birthdate`, `birthplace`, `gender`, `civil_status`, `house_number`, `street`, `barangay`, `city`, `province`, `country`, `contact_number`, `email`, `job`, `hire_date`, `manager`, `department_id`, `employee_type_id`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
('155a91f3-4857-44bd-ae20-000807536e74', 'image-upload/Microsoft_Azure.svg.png', 'dexter', 'test', 'test221', NULL, '2000-02-02', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test2223', 'officer_manager@gmail.com', 'test', '2020-02-02', 'test', 'dbfbb48e-6d41-4dbe-9f13-70343f5a6384', '07a67745-f63d-498c-a25b-02b724891c88', 'Active', NULL, '2022-03-04 11:38:07', NULL, NULL),
('191ad783-07cb-439d-90b5-1c115d562b50', 'image-upload/Microsoft_Azure.svg.png', 'denden', 'test', 'test221', NULL, '2000-02-02', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test22231', 'hr@gmail.com', 'test', '2020-02-02', 'test', '58ddb697-8db0-403b-a945-4e618c9ed876', '4072979b-f6fe-4633-8483-cb6bbb374ff2', 'Active', NULL, '2022-03-04 11:39:28', NULL, NULL),
('1d4a49ab-af7a-44ab-8c64-14cf7052669b', 'image-upload/Microsoft_Azure.svg.png', 'dexter', 'test', 'test22', NULL, '2000-02-02', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test222', 'officer_proc@gmail.com', 'test', '2020-02-02', 'test', 'dbfbb48e-6d41-4dbe-9f13-70343f5a6384', 'e0350e12-323b-42dc-aadd-1c9cf53c8653', 'Active', NULL, '2022-03-04 11:37:20', NULL, NULL),
('66f75371-1b12-4163-b2de-e6802fce6141', 'image-upload/Microsoft_Azure.svg.png', 'test22', 'test', 'test22', NULL, '2000-02-02', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test22', 'test2@gmail.com', 'test', '2020-02-02', 'test', '3633562d-a91b-49f4-bc1a-143236f60947', '6daacb75-6d90-4c0b-bd74-dd00e4d6a4a0', 'Active', NULL, '2022-03-04 11:35:50', NULL, NULL),
('caefbe83-3d90-4aa4-b47f-c0c37bd344e1', '<starlette.datastructures.UploadFile object at 0x000001BE895B98E0>', 'test', 'test', 'test', NULL, '2000-02-02', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test@gmail.com', 'test', '2020-02-02', 'test', '3633562d-a91b-49f4-bc1a-143236f60947', '6daacb75-6d90-4c0b-bd74-dd00e4d6a4a0', 'Active', NULL, '2022-03-04 11:34:45', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_types`
--

CREATE TABLE `employee_types` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee_types`
--

INSERT INTO `employee_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
('07a67745-f63d-498c-a25b-02b724891c88', 'Procurement Manager', 'string', 'Active', '2022-03-04 11:20:11', '2022-03-04 11:20:11'),
('4072979b-f6fe-4633-8483-cb6bbb374ff2', 'Human Resource Manager', 'string', 'Active', '2022-03-04 11:38:51', '2022-03-04 11:38:51'),
('6daacb75-6d90-4c0b-bd74-dd00e4d6a4a0', 'Accountant', 'string', 'Active', '2022-03-04 11:19:47', '2022-03-04 11:19:47'),
('e0350e12-323b-42dc-aadd-1c9cf53c8653', 'Procurement Officer', 'string', 'Active', '2022-03-04 11:20:04', '2022-03-04 11:20:04');

-- --------------------------------------------------------

--
-- Table structure for table `hospital_charges_bill`
--

CREATE TABLE `hospital_charges_bill` (
  `id` char(36) NOT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `invoice_date` datetime NOT NULL,
  `inpatient_bill_id` varchar(36) DEFAULT NULL,
  `hospital_services_id` varchar(36) NOT NULL,
  `total_amount` float NOT NULL,
  `cancellation_return` float DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `hospital_services`
--

CREATE TABLE `hospital_services` (
  `id` varchar(36) NOT NULL,
  `admission_id` varchar(36) NOT NULL,
  `hospital_service_name_id` varchar(36) NOT NULL,
  `quantity` float NOT NULL,
  `date` date NOT NULL,
  `total_amount` float NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `hospital_service_name`
--

CREATE TABLE `hospital_service_name` (
  `id` varchar(36) NOT NULL,
  `description_name` varchar(255) NOT NULL,
  `unit_price` float NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inpatients`
--

CREATE TABLE `inpatients` (
  `admission_id` varchar(255) NOT NULL,
  `inpatient_no` varchar(255) NOT NULL,
  `patient_id` varchar(36) DEFAULT NULL,
  `date_admitted` datetime NOT NULL,
  `reason_of_admittance` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `diagnosis` varchar(255) DEFAULT NULL,
  `tests` varchar(255) DEFAULT NULL,
  `treatments` varchar(255) DEFAULT NULL,
  `surgery` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `is_accepting_visits` varchar(255) DEFAULT NULL,
  `patient_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inpatient_bills`
--

CREATE TABLE `inpatient_bills` (
  `id` char(36) NOT NULL,
  `inpatient_bill_no` varchar(255) NOT NULL,
  `admission_id` varchar(36) NOT NULL,
  `inpatient_payment_id` varchar(36) DEFAULT NULL,
  `date_of_billing` date NOT NULL,
  `due_date` date NOT NULL,
  `balance_due` float NOT NULL DEFAULT 0,
  `status` varchar(255) NOT NULL DEFAULT 'Pending',
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inpatient_payments`
--

CREATE TABLE `inpatient_payments` (
  `id` char(36) NOT NULL,
  `or_no` varchar(255) NOT NULL,
  `inpatient_bill_id` char(36) NOT NULL,
  `total_amount_paid` float NOT NULL,
  `payment_term_id` char(36) NOT NULL,
  `date_of_payment` datetime NOT NULL DEFAULT current_timestamp(),
  `patient_cash_payment_id` char(36) DEFAULT NULL,
  `patient_check_payment_id` char(36) DEFAULT NULL,
  `payment_method_id` char(36) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `insurances`
--

CREATE TABLE `insurances` (
  `insurance_id` varchar(36) NOT NULL,
  `policy_holder` varchar(255) DEFAULT NULL,
  `policy_number` varchar(255) DEFAULT NULL,
  `company_phone` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lab_requests`
--

CREATE TABLE `lab_requests` (
  `id` varchar(36) NOT NULL,
  `lab_test_id` varchar(36) NOT NULL,
  `patient_id` varchar(36) NOT NULL,
  `lab_request_no` varchar(100) NOT NULL,
  `quantity` float NOT NULL,
  `cancellation_return` float NOT NULL,
  `is_active` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lab_requests_bill`
--

CREATE TABLE `lab_requests_bill` (
  `id` char(36) NOT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `invoice_date` datetime NOT NULL,
  `inpatient_bill_id` varchar(36) DEFAULT NULL,
  `lab_requests_id` varchar(36) NOT NULL,
  `total_amount` float NOT NULL,
  `cancellation_return` float DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Pending',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lab_service_name`
--

CREATE TABLE `lab_service_name` (
  `id` varchar(36) NOT NULL,
  `lab_service_name` varchar(255) NOT NULL,
  `lab_test_types_id` varchar(36) NOT NULL,
  `unit_price` float NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lab_test_types`
--

CREATE TABLE `lab_test_types` (
  `id` char(36) NOT NULL,
  `lab_test_type_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `maintenances`
--

CREATE TABLE `maintenances` (
  `maintenance_id` varchar(36) NOT NULL,
  `maintenance_provider_id` varchar(36) NOT NULL,
  `asset_id` varchar(36) NOT NULL,
  `maintenance_name` varchar(255) DEFAULT NULL,
  `maintenance_details` varchar(255) DEFAULT NULL,
  `maintenance_cost` decimal(10,0) DEFAULT NULL,
  `maintenance_day` int(11) DEFAULT NULL,
  `maintenance_due` datetime DEFAULT NULL,
  `maintenance_completed` datetime DEFAULT NULL,
  `maintenance_repeatable` varchar(255) DEFAULT NULL,
  `maintenance_status` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `active_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_providers`
--

CREATE TABLE `maintenance_providers` (
  `maintenance_provider_id` varchar(36) NOT NULL,
  `maintenance_provider_name` varchar(255) DEFAULT NULL,
  `maintenance_provider_contact` varchar(255) DEFAULT NULL,
  `maintenance_provider_email` varchar(255) DEFAULT NULL,
  `active_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_reports`
--

CREATE TABLE `maintenance_reports` (
  `maintenance_report_id` varchar(36) NOT NULL,
  `maintenance_id` varchar(36) NOT NULL,
  `maintenance_cost` decimal(10,0) DEFAULT NULL,
  `completed_date` datetime DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `medicalsupplies`
--

CREATE TABLE `medicalsupplies` (
  `id` char(36) NOT NULL,
  `ms_product_name` varchar(255) NOT NULL,
  `ms_quantity` int(11) NOT NULL,
  `ms_manufacturer` char(36) DEFAULT NULL,
  `ms_manufactured_date` date NOT NULL,
  `ms_import_date` date NOT NULL,
  `ms_expiration_date` date NOT NULL,
  `ms_batch_number` int(11) NOT NULL,
  `ms_unit_price` float NOT NULL,
  `ms_tax` int(11) DEFAULT NULL,
  `ms_purpose` varchar(255) NOT NULL,
  `condition` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `medicalsupplies_pr`
--

CREATE TABLE `medicalsupplies_pr` (
  `medicsupp_prid` char(36) NOT NULL,
  `ms_no` int(11) NOT NULL,
  `medical_id` char(36) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `prescription_id` char(36) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `medicines`
--

CREATE TABLE `medicines` (
  `id` char(36) NOT NULL,
  `med_product_name` varchar(255) NOT NULL,
  `med_quantity` int(11) NOT NULL,
  `med_manufacturer` char(36) DEFAULT NULL,
  `med_manufactured_date` date NOT NULL,
  `med_import_date` date NOT NULL,
  `med_expiration_date` date NOT NULL,
  `med_batch_number` int(11) NOT NULL,
  `med_unit_price` float NOT NULL,
  `med_tax` int(11) DEFAULT NULL,
  `med_purpose` varchar(255) NOT NULL,
  `condition` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `dosage` int(11) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `medicine_pr`
--

CREATE TABLE `medicine_pr` (
  `medpr_id` char(36) NOT NULL,
  `medicine_no` int(11) NOT NULL,
  `medicine_id` char(36) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `cancellation_return` float NOT NULL,
  `intake` varchar(255) NOT NULL,
  `frequency` varchar(255) NOT NULL,
  `dosage` varchar(255) NOT NULL,
  `doctor_prescribed` varchar(255) NOT NULL,
  `prescription_id` char(36) DEFAULT NULL,
  `med_pres_status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `notif_to` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `vendor_id` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `patient_registration`
--

CREATE TABLE `patient_registration` (
  `patient_id` varchar(36) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `weight` varchar(255) NOT NULL,
  `height` varchar(255) NOT NULL,
  `blood_type` varchar(255) NOT NULL,
  `guardian` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `medical_history_number` varchar(36) DEFAULT NULL,
  `dp_id` varchar(36) DEFAULT NULL,
  `insurance_id` varchar(36) DEFAULT NULL,
  `patient_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` char(36) NOT NULL,
  `method_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payment_terms`
--

CREATE TABLE `payment_terms` (
  `id` char(36) NOT NULL,
  `method_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pharmacy_bill`
--

CREATE TABLE `pharmacy_bill` (
  `id` char(36) NOT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `invoice_date` datetime NOT NULL,
  `inpatient_bill_id` varchar(36) DEFAULT NULL,
  `medpr_id` char(36) NOT NULL,
  `total_amount` float NOT NULL,
  `cancellation_return` float DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Pending',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `prescriptions`
--

CREATE TABLE `prescriptions` (
  `prescription_id` char(36) NOT NULL,
  `prescription_no` varchar(255) NOT NULL,
  `admission_id` char(36) DEFAULT NULL,
  `outpatient_id` varchar(255) DEFAULT NULL,
  `date_prescribed` date NOT NULL,
  `patient_status` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` char(36) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `estimated_price` float NOT NULL,
  `status` varchar(255) NOT NULL,
  `category_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `project_request`
--

CREATE TABLE `project_request` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `background` text NOT NULL,
  `coverage` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `target_beneficiaries` varchar(255) NOT NULL,
  `objectives` text NOT NULL,
  `expected_output` text NOT NULL,
  `assumptions` varchar(255) NOT NULL,
  `constraints` varchar(255) NOT NULL,
  `cost` float NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `approval_status` varchar(255) NOT NULL,
  `active_status` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` char(36) NOT NULL,
  `purchase_order_number` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `expected_delivery_date` date NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `subtotal` float NOT NULL,
  `discount` float NOT NULL,
  `tax` float NOT NULL,
  `total_amount` float NOT NULL,
  `shipping_method` varchar(255) NOT NULL,
  `payment_terms_id` char(36) NOT NULL,
  `payment_method_id` char(36) NOT NULL,
  `vendor_id` char(36) NOT NULL,
  `vendor_proposal_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_detail`
--

CREATE TABLE `purchase_order_detail` (
  `id` char(36) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `product_price` float NOT NULL,
  `status` varchar(255) NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `purchase_order_id` char(36) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_invoice`
--

CREATE TABLE `purchase_order_invoice` (
  `id` char(36) NOT NULL,
  `prepared_by` varchar(255) NOT NULL,
  `message` text DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `invoice_date` date NOT NULL,
  `due_date` date NOT NULL,
  `billing_address` varchar(255) NOT NULL,
  `amount_paid` varchar(255) DEFAULT NULL,
  `purchase_order_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_vendor_payments`
--

CREATE TABLE `purchase_order_vendor_payments` (
  `id` char(36) NOT NULL,
  `or_no` varchar(255) NOT NULL,
  `purchase_order_vendor_bill_id` char(36) NOT NULL,
  `total_amount_paid` float NOT NULL,
  `date_of_payment` datetime NOT NULL DEFAULT current_timestamp(),
  `payment_term_id` char(36) NOT NULL,
  `payment_method_id` char(36) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_requisition`
--

CREATE TABLE `purchase_requisition` (
  `id` char(36) NOT NULL,
  `purchase_requisition_number` int(11) DEFAULT NULL,
  `purpose` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_approved` datetime DEFAULT NULL,
  `department_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `given_budget` float DEFAULT NULL,
  `estimated_amount` float DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_requisition_detail`
--

CREATE TABLE `purchase_requisition_detail` (
  `id` char(36) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `new_category` varchar(255) DEFAULT NULL,
  `new_product_name` varchar(255) DEFAULT NULL,
  `estimated_price` float DEFAULT NULL,
  `purchase_requisition_id` char(36) NOT NULL,
  `product_id` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `related_documents`
--

CREATE TABLE `related_documents` (
  `id` char(36) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `request_quotation_id` char(36) DEFAULT NULL,
  `terms_of_reference_id` char(36) DEFAULT NULL,
  `vendor_proposal_id` char(36) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `request_quotation`
--

CREATE TABLE `request_quotation` (
  `id` char(36) NOT NULL,
  `request_quotation_number` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `due_date` datetime NOT NULL,
  `prepared_by` varchar(255) NOT NULL,
  `quotation_code` varchar(255) NOT NULL,
  `rfq_type` varchar(255) NOT NULL,
  `purchase_requisition_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `request_quotation_vendor`
--

CREATE TABLE `request_quotation_vendor` (
  `vendor_id` char(36) NOT NULL,
  `rfq_pr_id` char(36) DEFAULT NULL,
  `rfq_status` varchar(255) DEFAULT NULL,
  `approver_name` varchar(255) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `reject_reason` text DEFAULT NULL,
  `request_quotation_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `returns`
--

CREATE TABLE `returns` (
  `id` char(36) NOT NULL,
  `return_date` date NOT NULL,
  `return_status` varchar(255) NOT NULL,
  `return_type` varchar(255) NOT NULL,
  `returner` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `return_details`
--

CREATE TABLE `return_details` (
  `id` char(36) NOT NULL,
  `quantity` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `purchase_order_detail_id` char(36) DEFAULT NULL,
  `return_id` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `room_id` varchar(36) NOT NULL,
  `room_number` varchar(255) NOT NULL,
  `date_admitted` datetime DEFAULT NULL,
  `admission_id` varchar(255) NOT NULL,
  `room_type_id` varchar(36) NOT NULL,
  `location` varchar(36) DEFAULT NULL,
  `room_count` int(11) DEFAULT NULL,
  `room_status` varchar(36) NOT NULL,
  `active_status` varchar(36) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `room_bill`
--

CREATE TABLE `room_bill` (
  `id` char(36) NOT NULL,
  `invoice_no` varchar(100) NOT NULL,
  `invoice_date` datetime NOT NULL,
  `admission_id` varchar(36) NOT NULL,
  `inpatient_bill_id` varchar(36) DEFAULT NULL,
  `total_amount` float NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Pending',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `room_types`
--

CREATE TABLE `room_types` (
  `id` char(36) NOT NULL,
  `room_type_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `amount` float NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `specialization`
--

CREATE TABLE `specialization` (
  `specialization_id` char(36) NOT NULL,
  `specialization_name` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `surgeries`
--

CREATE TABLE `surgeries` (
  `id` varchar(36) NOT NULL,
  `surgery_no` varchar(100) NOT NULL,
  `patient_id` varchar(36) NOT NULL,
  `room` varchar(100) DEFAULT NULL,
  `surgery_type_id` varchar(36) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` varchar(100) DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `surgery_in_charge`
--

CREATE TABLE `surgery_in_charge` (
  `id` char(36) NOT NULL,
  `dr_in_charge_id` char(36) NOT NULL,
  `head_surgeon_id` varchar(36) DEFAULT 'No',
  `nurse_charge_id` char(36) DEFAULT NULL,
  `surgery_id` varchar(36) NOT NULL,
  `status` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `surgery_types`
--

CREATE TABLE `surgery_types` (
  `id` char(36) NOT NULL,
  `surgery_type_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `terms_of_reference`
--

CREATE TABLE `terms_of_reference` (
  `id` char(36) NOT NULL,
  `tor_number` int(11) DEFAULT NULL,
  `background` text NOT NULL,
  `objective` text NOT NULL,
  `scope_of_service` text NOT NULL,
  `tor_deliverables` text NOT NULL,
  `qualifications` text NOT NULL,
  `reporting_and_working_arrangements` text NOT NULL,
  `tor_annex_technical_specifications` text NOT NULL,
  `tor_annex_key_experts` text NOT NULL,
  `tor_annex_deliverables` text NOT NULL,
  `tor_annex_terms_conditions` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `prepared_by` varchar(255) NOT NULL,
  `approver_name` varchar(255) DEFAULT NULL,
  `approval_date` date DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `project_request_id` char(36) DEFAULT NULL,
  `vendor_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `treatments`
--

CREATE TABLE `treatments` (
  `id` varchar(36) NOT NULL,
  `treatment_no` varchar(100) NOT NULL,
  `patient_id` varchar(36) NOT NULL,
  `treatment_service_name_id` varchar(36) NOT NULL,
  `doctor_profile_id` varchar(36) NOT NULL,
  `description` text DEFAULT NULL,
  `quantity` float NOT NULL,
  `cancellation_return` float NOT NULL,
  `room` varchar(100) DEFAULT NULL,
  `session_no` text DEFAULT NULL,
  `session_datetime` datetime NOT NULL,
  `drug` text DEFAULT NULL,
  `dose` text DEFAULT NULL,
  `next_schedule` datetime DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  `is_active` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `treatment_bill`
--

CREATE TABLE `treatment_bill` (
  `id` char(36) NOT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `invoice_date` datetime NOT NULL,
  `inpatient_bill_id` varchar(36) DEFAULT NULL,
  `treatment_id` varchar(36) NOT NULL,
  `total_amount` float NOT NULL,
  `cancellation_return` float DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Pending',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `treatment_service_name`
--

CREATE TABLE `treatment_service_name` (
  `id` varchar(36) NOT NULL,
  `treatment_service_name` varchar(255) NOT NULL,
  `treatment_types_id` varchar(36) NOT NULL,
  `unit_price` float NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `treatment_types`
--

CREATE TABLE `treatment_types` (
  `id` char(36) NOT NULL,
  `treatment_type_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Active',
  `created_by` char(36) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` char(36) DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `user_type_id` char(36) DEFAULT NULL,
  `vendor_id` char(36) DEFAULT NULL,
  `employee_id` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `status`, `user_type_id`, `vendor_id`, `employee_id`, `created_at`, `updated_at`) VALUES
('342526b1-a419-4acd-9248-f5e7f447b92f', 'accountant@gmail.com', '$2b$12$Rxt15cbAPNunLh1r.LUENOkHumxTQDP6FJBwKOXY2B7o7.SOzK9v2', 'Active', '730f19b4-4f57-4ad7-91a7-7c51614a1000', NULL, '66f75371-1b12-4163-b2de-e6802fce6141', '2022-03-04 11:46:11', '2022-03-04 11:46:11'),
('35ad7fad-5d50-4c0b-801d-0c8d7594ecb7', 'proc_manager@gmail.com', '$2b$12$u4UpdFfPLU0RtwKq/n3zAu7Ykqiq2SWCUFQECK32zu//Y3hfLo5YW', 'Active', '730f19b4-4f57-4ad7-91a7-7c51614a1000', NULL, '155a91f3-4857-44bd-ae20-000807536e74', '2022-03-04 11:44:35', '2022-03-04 11:44:35'),
('a79e9866-9533-4320-8f74-072b5bdf7db1', 'hr@gmail.com', '$2b$12$UkKlZgT1EHAyLTFlaD6xP.01yTcLKkR.FTmVNYrignSgRL.Uo5ctu', 'Active', '730f19b4-4f57-4ad7-91a7-7c51614a1000', NULL, '191ad783-07cb-439d-90b5-1c115d562b50', '2022-03-04 11:46:35', '2022-03-04 11:46:35'),
('a9b0d57d-67dc-4b64-84f6-fa9595ced733', 'officer_proc@gmail.com', '$2b$12$XtGbkHBVNp6./jie4a5mRuHkEmMpopO9sQw7UURi59Kc.bsF7Y8IS', 'Active', '730f19b4-4f57-4ad7-91a7-7c51614a1000', NULL, '1d4a49ab-af7a-44ab-8c64-14cf7052669b', '2022-03-04 11:45:41', '2022-03-04 11:45:41');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
('0f0cdb5c-0eb2-44dd-9e53-476236327774', 'Outsider', 'string', 'Active', '2022-03-04 11:20:43', '2022-03-04 11:20:43'),
('730f19b4-4f57-4ad7-91a7-7c51614a1000', 'Insider', 'string', 'Active', '2022-03-04 11:20:36', '2022-03-04 11:20:36');

-- --------------------------------------------------------

--
-- Table structure for table `utilities`
--

CREATE TABLE `utilities` (
  `id` char(36) NOT NULL,
  `utility_type` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `utility_amount` varchar(255) NOT NULL,
  `due_date` varchar(255) NOT NULL,
  `notes` text DEFAULT NULL,
  `vendor_id` char(36) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` char(36) NOT NULL,
  `vendor_logo` varchar(255) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `vendor_website` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `organization_type` varchar(255) DEFAULT NULL,
  `region` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `municipality` varchar(255) DEFAULT NULL,
  `barangay` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `category_id` char(36) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_audit_trail`
--

CREATE TABLE `vendor_audit_trail` (
  `id` char(36) NOT NULL,
  `crud` varchar(255) NOT NULL,
  `client_ip` varchar(255) DEFAULT NULL,
  `table` varchar(255) NOT NULL,
  `payload` text DEFAULT NULL,
  `vendor_id` char(36) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_bidding_item`
--

CREATE TABLE `vendor_bidding_item` (
  `id` char(36) NOT NULL,
  `category_id` char(36) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `price_per_unit` float NOT NULL,
  `status` varchar(255) NOT NULL,
  `vendor_proposal_id` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_blacklist`
--

CREATE TABLE `vendor_blacklist` (
  `id` char(36) NOT NULL,
  `vendor_id` varchar(255) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_performance_evaluation`
--

CREATE TABLE `vendor_performance_evaluation` (
  `id` char(36) NOT NULL,
  `message` text DEFAULT NULL,
  `cost` varchar(255) NOT NULL,
  `timeliness` varchar(255) NOT NULL,
  `reliability` varchar(255) NOT NULL,
  `quality` varchar(255) NOT NULL,
  `availability` varchar(255) NOT NULL,
  `reputation` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `purchase_order_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_proposal`
--

CREATE TABLE `vendor_proposal` (
  `id` char(36) NOT NULL,
  `proposal_number` int(11) DEFAULT NULL,
  `subtotal` float NOT NULL,
  `discount` float NOT NULL,
  `tax` float NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `prepared_by` varchar(255) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `notes` text DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `arrival_date` date NOT NULL,
  `is_ordered` tinyint(1) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `updated_by` char(36) DEFAULT NULL,
  `awarded_by` varchar(255) DEFAULT NULL,
  `request_quotation_id` char(36) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_time_log`
--

CREATE TABLE `vendor_time_log` (
  `id` char(36) NOT NULL,
  `logged_date` datetime NOT NULL,
  `logged_type` varchar(255) NOT NULL,
  `client_ip` varchar(255) DEFAULT NULL,
  `vendor_id` char(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts_receivable_ledgers`
--
ALTER TABLE `accounts_receivable_ledgers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inpatient_bill_id` (`inpatient_bill_id`),
  ADD KEY `inpatient_payment_id` (`inpatient_payment_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `budget_plan`
--
ALTER TABLE `budget_plan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department_id` (`department_id`,`year`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `discharge_management`
--
ALTER TABLE `discharge_management`
  ADD PRIMARY KEY (`discharge_id`),
  ADD KEY `admission_id` (`admission_id`);

--
-- Indexes for table `discount_privillages`
--
ALTER TABLE `discount_privillages`
  ADD PRIMARY KEY (`dp_id`);

--
-- Indexes for table `doctor_fee_bill`
--
ALTER TABLE `doctor_fee_bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`),
  ADD KEY `inpatient_bill_id` (`inpatient_bill_id`),
  ADD KEY `doctor_id` (`doctor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_doctor_fee_bill_id` (`id`);

--
-- Indexes for table `doctor_profile`
--
ALTER TABLE `doctor_profile`
  ADD PRIMARY KEY (`doctor_id`),
  ADD KEY `specialization_id` (`specialization_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_employees_contact_number` (`contact_number`),
  ADD UNIQUE KEY `ix_employees_email` (`email`),
  ADD UNIQUE KEY `first_name` (`first_name`,`middle_name`,`last_name`,`extension_name`) USING HASH,
  ADD KEY `ix_employees_id` (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `employee_type_id` (`employee_type_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `employee_types`
--
ALTER TABLE `employee_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospital_charges_bill`
--
ALTER TABLE `hospital_charges_bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hospital_services_id` (`hospital_services_id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`),
  ADD KEY `inpatient_bill_id` (`inpatient_bill_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_hospital_charges_bill_id` (`id`);

--
-- Indexes for table `hospital_services`
--
ALTER TABLE `hospital_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admission_id` (`admission_id`),
  ADD KEY `hospital_service_name_id` (`hospital_service_name_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_hospital_services_id` (`id`);

--
-- Indexes for table `hospital_service_name`
--
ALTER TABLE `hospital_service_name`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_hospital_service_name_description_name` (`description_name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_hospital_service_name_id` (`id`);

--
-- Indexes for table `inpatients`
--
ALTER TABLE `inpatients`
  ADD PRIMARY KEY (`admission_id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `inpatient_bills`
--
ALTER TABLE `inpatient_bills`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `inpatient_bill_no` (`inpatient_bill_no`),
  ADD KEY `inpatient_payment_id` (`inpatient_payment_id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `admission_id` (`admission_id`);

--
-- Indexes for table `inpatient_payments`
--
ALTER TABLE `inpatient_payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `or_no` (`or_no`),
  ADD KEY `inpatient_bill_id` (`inpatient_bill_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `insurances`
--
ALTER TABLE `insurances`
  ADD PRIMARY KEY (`insurance_id`);

--
-- Indexes for table `lab_requests`
--
ALTER TABLE `lab_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lab_test_id` (`lab_test_id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `lab_requests_bill`
--
ALTER TABLE `lab_requests_bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`),
  ADD KEY `inpatient_bill_id` (`inpatient_bill_id`),
  ADD KEY `lab_requests_id` (`lab_requests_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_lab_requests_bill_id` (`id`);

--
-- Indexes for table `lab_service_name`
--
ALTER TABLE `lab_service_name`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lab_service_name` (`lab_service_name`),
  ADD KEY `lab_test_types_id` (`lab_test_types_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_lab_service_name_id` (`id`);

--
-- Indexes for table `lab_test_types`
--
ALTER TABLE `lab_test_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_lab_test_types_lab_test_type_name` (`lab_test_type_name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_lab_test_types_id` (`id`);

--
-- Indexes for table `maintenances`
--
ALTER TABLE `maintenances`
  ADD PRIMARY KEY (`maintenance_id`),
  ADD KEY `maintenance_provider_id` (`maintenance_provider_id`);

--
-- Indexes for table `maintenance_providers`
--
ALTER TABLE `maintenance_providers`
  ADD PRIMARY KEY (`maintenance_provider_id`);

--
-- Indexes for table `maintenance_reports`
--
ALTER TABLE `maintenance_reports`
  ADD PRIMARY KEY (`maintenance_report_id`),
  ADD KEY `maintenance_id` (`maintenance_id`);

--
-- Indexes for table `medicalsupplies`
--
ALTER TABLE `medicalsupplies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ms_product_name` (`ms_product_name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_medicalsupplies_id` (`id`);

--
-- Indexes for table `medicalsupplies_pr`
--
ALTER TABLE `medicalsupplies_pr`
  ADD PRIMARY KEY (`medicsupp_prid`),
  ADD UNIQUE KEY `ix_medicalsupplies_pr_ms_no` (`ms_no`),
  ADD KEY `medical_id` (`medical_id`),
  ADD KEY `prescription_id` (`prescription_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_medicalsupplies_pr_medicsupp_prid` (`medicsupp_prid`);

--
-- Indexes for table `medicines`
--
ALTER TABLE `medicines`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `med_product_name` (`med_product_name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_medicines_id` (`id`);

--
-- Indexes for table `medicine_pr`
--
ALTER TABLE `medicine_pr`
  ADD PRIMARY KEY (`medpr_id`),
  ADD UNIQUE KEY `ix_medicine_pr_medicine_no` (`medicine_no`),
  ADD KEY `medicine_id` (`medicine_id`),
  ADD KEY `prescription_id` (`prescription_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_medicine_pr_medpr_id` (`medpr_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Indexes for table `patient_registration`
--
ALTER TABLE `patient_registration`
  ADD PRIMARY KEY (`patient_id`),
  ADD KEY `dp_id` (`dp_id`),
  ADD KEY `insurance_id` (`insurance_id`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `payment_terms`
--
ALTER TABLE `payment_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `pharmacy_bill`
--
ALTER TABLE `pharmacy_bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`),
  ADD KEY `inpatient_bill_id` (`inpatient_bill_id`),
  ADD KEY `medpr_id` (`medpr_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_pharmacy_bill_id` (`id`);

--
-- Indexes for table `prescriptions`
--
ALTER TABLE `prescriptions`
  ADD PRIMARY KEY (`prescription_id`),
  ADD UNIQUE KEY `ix_prescriptions_prescription_no` (`prescription_no`),
  ADD KEY `admission_id` (`admission_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_prescriptions_prescription_id` (`prescription_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `project_request`
--
ALTER TABLE `project_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_order_number` (`purchase_order_number`),
  ADD UNIQUE KEY `vendor_proposal_id` (`vendor_proposal_id`),
  ADD KEY `payment_terms_id` (`payment_terms_id`),
  ADD KEY `payment_method_id` (`payment_method_id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `purchase_order_detail`
--
ALTER TABLE `purchase_order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `purchase_order_id` (`purchase_order_id`);

--
-- Indexes for table `purchase_order_invoice`
--
ALTER TABLE `purchase_order_invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_order_id` (`purchase_order_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `purchase_order_vendor_payments`
--
ALTER TABLE `purchase_order_vendor_payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `or_no` (`or_no`),
  ADD KEY `purchase_order_vendor_bill_id` (`purchase_order_vendor_bill_id`),
  ADD KEY `payment_term_id` (`payment_term_id`),
  ADD KEY `payment_method_id` (`payment_method_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `purchase_requisition`
--
ALTER TABLE `purchase_requisition`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_requisition_number` (`purchase_requisition_number`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `purchase_requisition_detail`
--
ALTER TABLE `purchase_requisition_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_requisition_id` (`purchase_requisition_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `related_documents`
--
ALTER TABLE `related_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `request_quotation_id` (`request_quotation_id`),
  ADD KEY `terms_of_reference_id` (`terms_of_reference_id`),
  ADD KEY `vendor_proposal_id` (`vendor_proposal_id`);

--
-- Indexes for table `request_quotation`
--
ALTER TABLE `request_quotation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `request_quotation_number` (`request_quotation_number`),
  ADD KEY `purchase_requisition_id` (`purchase_requisition_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `request_quotation_vendor`
--
ALTER TABLE `request_quotation_vendor`
  ADD PRIMARY KEY (`vendor_id`,`request_quotation_id`),
  ADD UNIQUE KEY `vendor_id` (`vendor_id`,`rfq_pr_id`),
  ADD KEY `rfq_pr_id` (`rfq_pr_id`),
  ADD KEY `request_quotation_id` (`request_quotation_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `returns`
--
ALTER TABLE `returns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `return_details`
--
ALTER TABLE `return_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_order_detail_id` (`purchase_order_detail_id`),
  ADD KEY `return_id` (`return_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`room_id`),
  ADD KEY `admission_id` (`admission_id`),
  ADD KEY `room_type_id` (`room_type_id`);

--
-- Indexes for table `room_bill`
--
ALTER TABLE `room_bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`),
  ADD KEY `admission_id` (`admission_id`),
  ADD KEY `inpatient_bill_id` (`inpatient_bill_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `room_types`
--
ALTER TABLE `room_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_room_types_room_type_name` (`room_type_name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_room_types_id` (`id`);

--
-- Indexes for table `specialization`
--
ALTER TABLE `specialization`
  ADD PRIMARY KEY (`specialization_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `surgeries`
--
ALTER TABLE `surgeries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_id` (`patient_id`),
  ADD KEY `surgery_type_id` (`surgery_type_id`);

--
-- Indexes for table `surgery_in_charge`
--
ALTER TABLE `surgery_in_charge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dr_in_charge_id` (`dr_in_charge_id`),
  ADD KEY `nurse_charge_id` (`nurse_charge_id`),
  ADD KEY `surgery_id` (`surgery_id`);

--
-- Indexes for table `surgery_types`
--
ALTER TABLE `surgery_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_surgery_types_surgery_type_name` (`surgery_type_name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_surgery_types_id` (`id`);

--
-- Indexes for table `terms_of_reference`
--
ALTER TABLE `terms_of_reference`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tor_number` (`tor_number`),
  ADD KEY `project_request_id` (`project_request_id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `treatments`
--
ALTER TABLE `treatments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_id` (`patient_id`),
  ADD KEY `treatment_service_name_id` (`treatment_service_name_id`),
  ADD KEY `doctor_profile_id` (`doctor_profile_id`);

--
-- Indexes for table `treatment_bill`
--
ALTER TABLE `treatment_bill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`),
  ADD KEY `inpatient_bill_id` (`inpatient_bill_id`),
  ADD KEY `treatment_id` (`treatment_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_treatment_bill_id` (`id`);

--
-- Indexes for table `treatment_service_name`
--
ALTER TABLE `treatment_service_name`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `treatment_service_name` (`treatment_service_name`),
  ADD KEY `treatment_types_id` (`treatment_types_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_treatment_service_name_id` (`id`);

--
-- Indexes for table `treatment_types`
--
ALTER TABLE `treatment_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ix_treatment_types_treatment_type_name` (`treatment_type_name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `ix_treatment_types_id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `user_type_id` (`user_type_id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utilities`
--
ALTER TABLE `utilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vendor_name` (`vendor_name`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `vendor_audit_trail`
--
ALTER TABLE `vendor_audit_trail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Indexes for table `vendor_bidding_item`
--
ALTER TABLE `vendor_bidding_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `vendor_proposal_id` (`vendor_proposal_id`);

--
-- Indexes for table `vendor_blacklist`
--
ALTER TABLE `vendor_blacklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `vendor_performance_evaluation`
--
ALTER TABLE `vendor_performance_evaluation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_order_id` (`purchase_order_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `vendor_proposal`
--
ALTER TABLE `vendor_proposal`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `created_by` (`created_by`,`request_quotation_id`),
  ADD UNIQUE KEY `proposal_number` (`proposal_number`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `request_quotation_id` (`request_quotation_id`);

--
-- Indexes for table `vendor_time_log`
--
ALTER TABLE `vendor_time_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts_receivable_ledgers`
--
ALTER TABLE `accounts_receivable_ledgers`
  ADD CONSTRAINT `accounts_receivable_ledgers_ibfk_1` FOREIGN KEY (`inpatient_bill_id`) REFERENCES `inpatient_bills` (`id`),
  ADD CONSTRAINT `accounts_receivable_ledgers_ibfk_2` FOREIGN KEY (`inpatient_payment_id`) REFERENCES `inpatient_payments` (`id`),
  ADD CONSTRAINT `accounts_receivable_ledgers_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `accounts_receivable_ledgers_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `budget_plan`
--
ALTER TABLE `budget_plan`
  ADD CONSTRAINT `budget_plan_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `budget_plan_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `budget_plan_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `category_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `department_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `discharge_management`
--
ALTER TABLE `discharge_management`
  ADD CONSTRAINT `discharge_management_ibfk_1` FOREIGN KEY (`admission_id`) REFERENCES `inpatients` (`admission_id`);

--
-- Constraints for table `doctor_fee_bill`
--
ALTER TABLE `doctor_fee_bill`
  ADD CONSTRAINT `doctor_fee_bill_ibfk_1` FOREIGN KEY (`inpatient_bill_id`) REFERENCES `inpatient_bills` (`id`),
  ADD CONSTRAINT `doctor_fee_bill_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `doctor_profile` (`doctor_id`),
  ADD CONSTRAINT `doctor_fee_bill_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `doctor_fee_bill_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `doctor_profile`
--
ALTER TABLE `doctor_profile`
  ADD CONSTRAINT `doctor_profile_ibfk_1` FOREIGN KEY (`specialization_id`) REFERENCES `specialization` (`specialization_id`),
  ADD CONSTRAINT `doctor_profile_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `doctor_profile_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `employees_ibfk_2` FOREIGN KEY (`employee_type_id`) REFERENCES `employee_types` (`id`),
  ADD CONSTRAINT `employees_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `employees_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `hospital_charges_bill`
--
ALTER TABLE `hospital_charges_bill`
  ADD CONSTRAINT `hospital_charges_bill_ibfk_1` FOREIGN KEY (`inpatient_bill_id`) REFERENCES `inpatient_bills` (`id`),
  ADD CONSTRAINT `hospital_charges_bill_ibfk_2` FOREIGN KEY (`hospital_services_id`) REFERENCES `hospital_services` (`id`),
  ADD CONSTRAINT `hospital_charges_bill_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `hospital_charges_bill_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `hospital_services`
--
ALTER TABLE `hospital_services`
  ADD CONSTRAINT `hospital_services_ibfk_1` FOREIGN KEY (`admission_id`) REFERENCES `inpatients` (`admission_id`),
  ADD CONSTRAINT `hospital_services_ibfk_2` FOREIGN KEY (`hospital_service_name_id`) REFERENCES `hospital_service_name` (`id`),
  ADD CONSTRAINT `hospital_services_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `hospital_services_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `hospital_service_name`
--
ALTER TABLE `hospital_service_name`
  ADD CONSTRAINT `hospital_service_name_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `hospital_service_name_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `inpatients`
--
ALTER TABLE `inpatients`
  ADD CONSTRAINT `inpatients_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient_registration` (`patient_id`);

--
-- Constraints for table `inpatient_bills`
--
ALTER TABLE `inpatient_bills`
  ADD CONSTRAINT `inpatient_bills_ibfk_1` FOREIGN KEY (`inpatient_payment_id`) REFERENCES `inpatient_payments` (`id`),
  ADD CONSTRAINT `inpatient_bills_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `inpatient_bills_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `inpatient_bills_ibfk_4` FOREIGN KEY (`admission_id`) REFERENCES `inpatients` (`admission_id`);

--
-- Constraints for table `inpatient_payments`
--
ALTER TABLE `inpatient_payments`
  ADD CONSTRAINT `inpatient_payments_ibfk_1` FOREIGN KEY (`inpatient_bill_id`) REFERENCES `inpatient_bills` (`id`),
  ADD CONSTRAINT `inpatient_payments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `inpatient_payments_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `lab_requests`
--
ALTER TABLE `lab_requests`
  ADD CONSTRAINT `lab_requests_ibfk_1` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_service_name` (`id`),
  ADD CONSTRAINT `lab_requests_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patient_registration` (`patient_id`);

--
-- Constraints for table `lab_requests_bill`
--
ALTER TABLE `lab_requests_bill`
  ADD CONSTRAINT `lab_requests_bill_ibfk_1` FOREIGN KEY (`inpatient_bill_id`) REFERENCES `inpatient_bills` (`id`),
  ADD CONSTRAINT `lab_requests_bill_ibfk_2` FOREIGN KEY (`lab_requests_id`) REFERENCES `lab_requests` (`id`),
  ADD CONSTRAINT `lab_requests_bill_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `lab_requests_bill_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `lab_service_name`
--
ALTER TABLE `lab_service_name`
  ADD CONSTRAINT `lab_service_name_ibfk_1` FOREIGN KEY (`lab_test_types_id`) REFERENCES `lab_test_types` (`id`),
  ADD CONSTRAINT `lab_service_name_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `lab_service_name_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `lab_test_types`
--
ALTER TABLE `lab_test_types`
  ADD CONSTRAINT `lab_test_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `lab_test_types_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `maintenances`
--
ALTER TABLE `maintenances`
  ADD CONSTRAINT `maintenances_ibfk_1` FOREIGN KEY (`maintenance_provider_id`) REFERENCES `maintenance_providers` (`maintenance_provider_id`);

--
-- Constraints for table `maintenance_reports`
--
ALTER TABLE `maintenance_reports`
  ADD CONSTRAINT `maintenance_reports_ibfk_1` FOREIGN KEY (`maintenance_id`) REFERENCES `maintenances` (`maintenance_id`);

--
-- Constraints for table `medicalsupplies`
--
ALTER TABLE `medicalsupplies`
  ADD CONSTRAINT `medicalsupplies_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `medicalsupplies_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `medicalsupplies_pr`
--
ALTER TABLE `medicalsupplies_pr`
  ADD CONSTRAINT `medicalsupplies_pr_ibfk_1` FOREIGN KEY (`medical_id`) REFERENCES `medicalsupplies` (`id`),
  ADD CONSTRAINT `medicalsupplies_pr_ibfk_2` FOREIGN KEY (`prescription_id`) REFERENCES `prescriptions` (`prescription_id`),
  ADD CONSTRAINT `medicalsupplies_pr_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `medicalsupplies_pr_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `medicines`
--
ALTER TABLE `medicines`
  ADD CONSTRAINT `medicines_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `medicines_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `medicine_pr`
--
ALTER TABLE `medicine_pr`
  ADD CONSTRAINT `medicine_pr_ibfk_1` FOREIGN KEY (`medicine_id`) REFERENCES `medicines` (`id`),
  ADD CONSTRAINT `medicine_pr_ibfk_2` FOREIGN KEY (`prescription_id`) REFERENCES `prescriptions` (`prescription_id`),
  ADD CONSTRAINT `medicine_pr_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `medicine_pr_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`);

--
-- Constraints for table `patient_registration`
--
ALTER TABLE `patient_registration`
  ADD CONSTRAINT `patient_registration_ibfk_1` FOREIGN KEY (`dp_id`) REFERENCES `discount_privillages` (`dp_id`),
  ADD CONSTRAINT `patient_registration_ibfk_2` FOREIGN KEY (`insurance_id`) REFERENCES `insurances` (`insurance_id`);

--
-- Constraints for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD CONSTRAINT `payment_method_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `payment_method_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `payment_terms`
--
ALTER TABLE `payment_terms`
  ADD CONSTRAINT `payment_terms_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `payment_terms_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `pharmacy_bill`
--
ALTER TABLE `pharmacy_bill`
  ADD CONSTRAINT `pharmacy_bill_ibfk_1` FOREIGN KEY (`inpatient_bill_id`) REFERENCES `inpatient_bills` (`id`),
  ADD CONSTRAINT `pharmacy_bill_ibfk_2` FOREIGN KEY (`medpr_id`) REFERENCES `medicine_pr` (`medpr_id`),
  ADD CONSTRAINT `pharmacy_bill_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `pharmacy_bill_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `prescriptions`
--
ALTER TABLE `prescriptions`
  ADD CONSTRAINT `prescriptions_ibfk_1` FOREIGN KEY (`admission_id`) REFERENCES `inpatients` (`admission_id`),
  ADD CONSTRAINT `prescriptions_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `prescriptions_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `project_request`
--
ALTER TABLE `project_request`
  ADD CONSTRAINT `project_request_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `project_request_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD CONSTRAINT `purchase_order_ibfk_1` FOREIGN KEY (`payment_terms_id`) REFERENCES `payment_terms` (`id`),
  ADD CONSTRAINT `purchase_order_ibfk_2` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`),
  ADD CONSTRAINT `purchase_order_ibfk_3` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `purchase_order_ibfk_4` FOREIGN KEY (`vendor_proposal_id`) REFERENCES `vendor_proposal` (`id`),
  ADD CONSTRAINT `purchase_order_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `purchase_order_ibfk_6` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `purchase_order_detail`
--
ALTER TABLE `purchase_order_detail`
  ADD CONSTRAINT `purchase_order_detail_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `purchase_order_detail_ibfk_2` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`);

--
-- Constraints for table `purchase_order_invoice`
--
ALTER TABLE `purchase_order_invoice`
  ADD CONSTRAINT `purchase_order_invoice_ibfk_1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`),
  ADD CONSTRAINT `purchase_order_invoice_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `purchase_order_invoice_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `vendor` (`id`);

--
-- Constraints for table `purchase_order_vendor_payments`
--
ALTER TABLE `purchase_order_vendor_payments`
  ADD CONSTRAINT `purchase_order_vendor_payments_ibfk_1` FOREIGN KEY (`purchase_order_vendor_bill_id`) REFERENCES `purchase_order_invoice` (`id`),
  ADD CONSTRAINT `purchase_order_vendor_payments_ibfk_2` FOREIGN KEY (`payment_term_id`) REFERENCES `payment_terms` (`id`),
  ADD CONSTRAINT `purchase_order_vendor_payments_ibfk_3` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`),
  ADD CONSTRAINT `purchase_order_vendor_payments_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `purchase_order_vendor_payments_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `purchase_requisition`
--
ALTER TABLE `purchase_requisition`
  ADD CONSTRAINT `purchase_requisition_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `purchase_requisition_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `purchase_requisition_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `purchase_requisition_detail`
--
ALTER TABLE `purchase_requisition_detail`
  ADD CONSTRAINT `purchase_requisition_detail_ibfk_1` FOREIGN KEY (`purchase_requisition_id`) REFERENCES `purchase_requisition` (`id`),
  ADD CONSTRAINT `purchase_requisition_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `purchase_requisition_detail_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `related_documents`
--
ALTER TABLE `related_documents`
  ADD CONSTRAINT `related_documents_ibfk_1` FOREIGN KEY (`request_quotation_id`) REFERENCES `request_quotation` (`id`),
  ADD CONSTRAINT `related_documents_ibfk_2` FOREIGN KEY (`terms_of_reference_id`) REFERENCES `terms_of_reference` (`id`),
  ADD CONSTRAINT `related_documents_ibfk_3` FOREIGN KEY (`vendor_proposal_id`) REFERENCES `vendor_proposal` (`id`);

--
-- Constraints for table `request_quotation`
--
ALTER TABLE `request_quotation`
  ADD CONSTRAINT `request_quotation_ibfk_1` FOREIGN KEY (`purchase_requisition_id`) REFERENCES `purchase_requisition` (`id`),
  ADD CONSTRAINT `request_quotation_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `request_quotation_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `request_quotation_vendor`
--
ALTER TABLE `request_quotation_vendor`
  ADD CONSTRAINT `request_quotation_vendor_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `request_quotation_vendor_ibfk_2` FOREIGN KEY (`rfq_pr_id`) REFERENCES `request_quotation` (`purchase_requisition_id`),
  ADD CONSTRAINT `request_quotation_vendor_ibfk_3` FOREIGN KEY (`request_quotation_id`) REFERENCES `request_quotation` (`id`),
  ADD CONSTRAINT `request_quotation_vendor_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `request_quotation_vendor_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `returns`
--
ALTER TABLE `returns`
  ADD CONSTRAINT `returns_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `returns_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `return_details`
--
ALTER TABLE `return_details`
  ADD CONSTRAINT `return_details_ibfk_1` FOREIGN KEY (`purchase_order_detail_id`) REFERENCES `purchase_order_detail` (`id`),
  ADD CONSTRAINT `return_details_ibfk_2` FOREIGN KEY (`return_id`) REFERENCES `returns` (`id`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`admission_id`) REFERENCES `inpatients` (`admission_id`),
  ADD CONSTRAINT `rooms_ibfk_2` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`);

--
-- Constraints for table `room_bill`
--
ALTER TABLE `room_bill`
  ADD CONSTRAINT `room_bill_ibfk_1` FOREIGN KEY (`admission_id`) REFERENCES `inpatients` (`admission_id`),
  ADD CONSTRAINT `room_bill_ibfk_2` FOREIGN KEY (`inpatient_bill_id`) REFERENCES `inpatient_bills` (`id`),
  ADD CONSTRAINT `room_bill_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `room_bill_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `room_types`
--
ALTER TABLE `room_types`
  ADD CONSTRAINT `room_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `room_types_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `specialization`
--
ALTER TABLE `specialization`
  ADD CONSTRAINT `specialization_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `specialization_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `surgeries`
--
ALTER TABLE `surgeries`
  ADD CONSTRAINT `surgeries_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient_registration` (`patient_id`),
  ADD CONSTRAINT `surgeries_ibfk_2` FOREIGN KEY (`surgery_type_id`) REFERENCES `surgery_types` (`id`);

--
-- Constraints for table `surgery_in_charge`
--
ALTER TABLE `surgery_in_charge`
  ADD CONSTRAINT `surgery_in_charge_ibfk_1` FOREIGN KEY (`dr_in_charge_id`) REFERENCES `doctor_profile` (`doctor_id`),
  ADD CONSTRAINT `surgery_in_charge_ibfk_2` FOREIGN KEY (`nurse_charge_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `surgery_in_charge_ibfk_3` FOREIGN KEY (`surgery_id`) REFERENCES `surgeries` (`id`);

--
-- Constraints for table `surgery_types`
--
ALTER TABLE `surgery_types`
  ADD CONSTRAINT `surgery_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `surgery_types_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `terms_of_reference`
--
ALTER TABLE `terms_of_reference`
  ADD CONSTRAINT `terms_of_reference_ibfk_1` FOREIGN KEY (`project_request_id`) REFERENCES `project_request` (`id`),
  ADD CONSTRAINT `terms_of_reference_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `terms_of_reference_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `terms_of_reference_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `treatments`
--
ALTER TABLE `treatments`
  ADD CONSTRAINT `treatments_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient_registration` (`patient_id`),
  ADD CONSTRAINT `treatments_ibfk_2` FOREIGN KEY (`treatment_service_name_id`) REFERENCES `treatment_service_name` (`id`),
  ADD CONSTRAINT `treatments_ibfk_3` FOREIGN KEY (`doctor_profile_id`) REFERENCES `doctor_profile` (`doctor_id`);

--
-- Constraints for table `treatment_bill`
--
ALTER TABLE `treatment_bill`
  ADD CONSTRAINT `treatment_bill_ibfk_1` FOREIGN KEY (`inpatient_bill_id`) REFERENCES `inpatient_bills` (`id`),
  ADD CONSTRAINT `treatment_bill_ibfk_2` FOREIGN KEY (`treatment_id`) REFERENCES `treatments` (`id`),
  ADD CONSTRAINT `treatment_bill_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `treatment_bill_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `treatment_service_name`
--
ALTER TABLE `treatment_service_name`
  ADD CONSTRAINT `treatment_service_name_ibfk_1` FOREIGN KEY (`treatment_types_id`) REFERENCES `treatment_types` (`id`),
  ADD CONSTRAINT `treatment_service_name_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `treatment_service_name_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `treatment_types`
--
ALTER TABLE `treatment_types`
  ADD CONSTRAINT `treatment_types_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `treatment_types_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`);

--
-- Constraints for table `utilities`
--
ALTER TABLE `utilities`
  ADD CONSTRAINT `utilities_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `utilities_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `utilities_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `vendor`
--
ALTER TABLE `vendor`
  ADD CONSTRAINT `vendor_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `vendor_audit_trail`
--
ALTER TABLE `vendor_audit_trail`
  ADD CONSTRAINT `vendor_audit_trail_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`);

--
-- Constraints for table `vendor_bidding_item`
--
ALTER TABLE `vendor_bidding_item`
  ADD CONSTRAINT `vendor_bidding_item_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `vendor_bidding_item_ibfk_2` FOREIGN KEY (`vendor_proposal_id`) REFERENCES `vendor_proposal` (`id`);

--
-- Constraints for table `vendor_blacklist`
--
ALTER TABLE `vendor_blacklist`
  ADD CONSTRAINT `vendor_blacklist_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `vendor_blacklist_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `vendor_blacklist_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `vendor_performance_evaluation`
--
ALTER TABLE `vendor_performance_evaluation`
  ADD CONSTRAINT `vendor_performance_evaluation_ibfk_1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`),
  ADD CONSTRAINT `vendor_performance_evaluation_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `vendor_performance_evaluation_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `vendor_proposal`
--
ALTER TABLE `vendor_proposal`
  ADD CONSTRAINT `vendor_proposal_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `vendor_proposal_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `vendor_proposal_ibfk_3` FOREIGN KEY (`request_quotation_id`) REFERENCES `request_quotation` (`id`);

--
-- Constraints for table `vendor_time_log`
--
ALTER TABLE `vendor_time_log`
  ADD CONSTRAINT `vendor_time_log_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
