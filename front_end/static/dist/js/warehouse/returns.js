$(function () {
    // getRFQNo()
  
    
    loadTable();
  
  
    pr_detail_table();
  
  });
  
  
  // // all approved request
  loadTable = () => {
    $.ajaxSetup({
      headers: {
        Accept: "application/json",
        Authorization: "Bearer " + localStorage.getItem("TOKEN"),
        ContentType: "application/x-www-form-urlencoded",
      },
    });
    // '+status+'
    $("#data-table").dataTable().fnClearTable();
    $("#data-table").dataTable().fnDraw();
    $("#data-table").dataTable().fnDestroy();
    $("#data-table").DataTable({
      ajax: { url: apiURL + "returns/", dataSrc: "" },
  
      responsive: true,
      serverSide: false,
      dataType: "json",
      type: "GET",
      columns: [
        // {
        //   data: null,
        //   searchable: true,
        //   width: "15%",
        //   render: function (aData, type, row) {
        //     return formatPurchaseRequestNo(aData.purchase_requisition_number);
        //   },
        // },
        {
          data: null,
          name: null,
          searchable: true,
          width: "10%",
          // className: "dtr-control",
          render: function (aData, type, row) {
            // return new Date(aData.purchase_requesition.created_at)
            //   .toString()
            //   .slice(0, 24);
            return moment(aData["return_date"]).format("MMMM D, YYYY");
          },
        },
        {
            data: "returner",
            name: "returner",
            searchable: true,
            width: "10%",
            // className: "dtr-control",
          },

          {
            data: "return_type",
            name: "return_type",
            searchable: true,
            width: "10%",
            // className: "dtr-control",
          },

     
        {
          data: null,
          name: null,
          searchable: true,
          width: "15%",
          render: function (aData, type, row) {
           
            let status =""
            if(aData.return_status == "Pending"){
                status +=
                  '<label class="text-left badge badge-warning p-2 w-auto"> ' +
                  aData.return_status +
                  "</label> ";

            }
            else if(aData.return_status == "Returned"){
                status +=
                  '<label class="text-left badge badge-primary p-2 w-auto"> ' +
                  aData.return_status +
                  "</label> ";

            }
            else{
                status +=
                '<label class="text-left badge badge-danger p-2 w-auto"> ' +
                aData.return_status +
                "</label> ";
            }


              return status;
         
            
          },
        },
        {
          data: null,
          width: "15%",
          render: function (aData, type, row) {
            let buttons = "";
            // info
            buttons +=
              '<div class="text center dropdown"><div class="btn btn-sm btn-default" data-toggle="dropdown" role="button">' +
              '<i class="fas fa-ellipsis-v"></i></div><div class="dropdown-menu dropdown-menu-right">';
            //view
            buttons +=
              '<div class="dropdown-item d-flex role="button" onClick="return dataInfo(\'' +
              aData["id"] +
              "',0)\">" +
              '<div style="width: 2rem"> <i class= "fas fa-file-alt mr-1"></i></div>' +
              "<div> View</div></div>";
  
            buttons += "</div></div>";
  
            return buttons; // same class in i element removed it from a element
          },
        },
      ],
    });
  };
  
  //   show pr
  dataInfo = (id, type) => {
    $("#modal-xl").modal("show");
  
    //   console.log(id);
    $.ajax({
      url: apiURL + "project-request/" + id,
      type: "GET",
      dataType: "json",
      success: function (data) {
        if (data) {
      
            console.log(data)
  
       
          if (type == 0) {
            $(".print").hide();
          } else {
            $(".print").show();
            $(".print").html("Print" + '<i class="fas fa-print ml-1"></i>');
          }
        } else {
          notification("error", "Error!", data.detail);
  
          console.log("error" + data);
          loadTable();
        }
      },
      error: function ({ responseJSON }) {},
    });
  };
  
  //   pr table
  pr_detail_table = () => {
    $("#pr-detail-table").dataTable().fnClearTable();
    $("#pr-detail-table").dataTable().fnDraw();
    $("#pr-detail-table").dataTable().fnDestroy();
    prd_table = $("#pr-detail-table").DataTable({
      info: false,
      paging: false,
      searching: false,
      ordering: false,
    });
  };
  
  