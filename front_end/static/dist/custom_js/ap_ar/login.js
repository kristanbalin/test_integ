$(document).ready(function () {
	$.removeCookie("TOKEN");
	$("#login_form").validate({
		submitHandler: function (form) {
			//trimInputFields();
			$.ajax({
				url: "http://localhost:8000/login",
				type: "POST", // post, put, delete, get
				data: {
					username: $("#username").val(),
					password: $("#password").val(),
				},
				dataType: "json",
				success: function (data) {
					$.cookie("TOKEN", data.access_token);
					sessionStorage.setItem("USERNAME", data.data[0].username);
					sessionStorage.setItem("USER_ID", data.data[0].id);
					sessionStorage.setItem("USER_TYPE", data.data[0].user_type);
					sessionStorage.setItem("EMPLOYEE_ID", data.data[0].employee_id);

					window.location.replace("/AR_AP/admin/dashboard");
				},
				error: function (data) {
					toastr.warning(data.responseJSON.detail);
				},
			});
		},
		errorElement: "span",
		errorPlacement: function (error, element) {
			error.addClass("invalid-feedback");
			element.closest(".input-group ").append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass("is-invalid");
		},
	});
});
