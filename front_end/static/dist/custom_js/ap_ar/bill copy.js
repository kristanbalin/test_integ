$(document).ready(function () {

	// function to load all patient (name) admissions
	loadInpatientManagement = () => {
		$.ajax({
		url: apiURL + "patient_registration/datatable",
		dataSrc: "",
		type: "GET",
		success: function (data) {
			$("#patient_full_name").empty();
			$.each(data, function (i, dataOptions) {
			var options = "";
			console.log(data)

			if (data.middle_name == null) {
				data.middle_name = "";
			} else {
				data.middle_name = data.middle_name;}
			
			options =
				"<option value='" +
				dataOptions.patient_id +
				"'>" +
				" " +
				dataOptions.first_name +" "+data.middle_name + " "+dataOptions.last_name;
			("</option>");
			$("#patient_full_name").append(options);
			
			});

			$("#patient_full_name").append('<option value="" selected>Select Patient Name</option>');
		},
		
		error: function ({ responseJSON }) {
		},
		});
	};
	loadInpatientManagement();

	// load date admitted by patient id
	$("#patient_full_name").change(function () {
		$("#date_admitted").empty()
		$.ajax({
			url: 
			apiURL + 
			"patient_registration/"+ 
			$("#patient_full_name").val(),
			dataSrc: "",
			type: "GET",
			
			success:function (data) {
				let patient_admission_length = data.patient_admission.length;
				let patient_admission_name = "";
				let patient_value_admission_id ="";
				for (let j = 0; j < patient_admission_length; j++) {
					console.log(j)
					patient_admission_name =  data.patient_admission[j].date_admitted;
					patient_value_admission_id = data.patient_admission[j].admission_id;
					options =
					"<option value='" + patient_value_admission_id +"'>" +
					" " + patient_admission_name;
				("</option>");
				$("#date_admitted").append(options);
				}
				$("#date_admitted").append('<option value="" selected>Select date admitted</option>');
			},
			error: function ({ responseJSON }) {},
		});
	});

	// load date discharged by patient id
	$("#patient_full_name").change(function () {
		$("#date_discharged").empty()
		$.ajax({
			url: 
			apiURL + 
			"patient_registration/"+ 
			$("#patient_full_name").val(),
			dataSrc: "",
			type: "GET",
			
			success:function (data) {
				let inpatient_discharged_length = data.inpatient_discharged.length;
				let inpatient_discharged_name = "";
				for (let j = 0; j < inpatient_discharged_length; j++) {
					console.log(j)
						inpatient_discharged_name =  data.inpatient_discharged[j].discharge_date;
						options =
						"<option value='" +
						inpatient_discharged_name +
						"'>" +
						" " +
						inpatient_discharged_name;
					("</option>");
					$("#date_discharged").append(options);
					}
					$("#date_discharged").append('<option value="" selected>Select date discharged</option>');
			},
			error: function ({ responseJSON }) {},
		});
	});

	// Patient Birthdate
	$("#patient_full_name").change(function () {
		$("#birthdate").empty();
		$.ajax({
			url: 
			apiURL + 
			"patient_registration/"+ 
			$("#patient_full_name").val(),
			dataSrc: "",
			type: "GET",
			success: function (data) {
				$("#birthdate").text(data.pt_birthdate);
			},
			error: function ({ responseJSON }) {},
		});
	});

	// room number
	$("#date_admitted").change(function () {
		$("#room_number").empty();
		$.ajax({
			url: 
			apiURL + 
			"inpatient_management/"+ 
			$("#date_admitted").val(),
			dataSrc: "",
			type: "GET",
			success: function (data) {
				$("#room_number").text(data.room_number);
			},
			error: function ({ responseJSON }) {},
		});
	});

	// room number empty when pt name change
	$("#patient_full_name").change(function () {
		$("#room_number").empty();
	});

	// room rate
	$("#date_admitted").change(function () {
		$("#room_rate").empty();
		$.ajax({
			url: 
			apiURL + 
			"inpatient_management/"+ 
			$("#date_admitted").val(),
			dataSrc: "",
			type: "GET",
			success: function (data) {
				$("#room_rate").text(data.room_type.amount);
			},
			error: function ({ responseJSON }) {},
		});
	});

	// room rate empty when pt name change
	$("#patient_full_name").change(function () {
		$("#room_rate").empty();
	});

	// load address
	$("#patient_full_name").change(function () {
		$("#address").empty();
		$.ajax({
			url: 
			apiURL + 
			"patient_registration/"+ 
			$("#patient_full_name").val(),
			dataSrc: "",
			type: "GET",
			success: function (data) {
				if (data.pt_house_number == null) {
					data.pt_house_number = "";
				} else {
					data.pt_house_number = data.pt_house_number;
				}
				if (data.pt_street == null) {
					data.pt_street = "";
				} else {
					data.pt_street = data.pt_street;
				}

				$("#address").text(data.pt_house_number + " "+ data.pt_street +" "+  data.pt_barangay + " "+ data.pt_city + " "+ data.pt_province + " "+ data.pt_country);
			},
			error: function ({ responseJSON }) {},
		});
	});


	// Particulars
	$("#date_admitted").change(function () {
		// $("#room_rate").empty();
		patient_id = "a1b1932f-4bab-48a2-a75f-cf73cd6d4965"
		date_admitted = "2022-01-04"
		upto = "2022-01-10"
		type = 0;
		$.ajax({
			url: apiURL + "hospital_charges/find_hospital_charges/" +patient_id +"/"+ date_admitted +"/"+upto,
			type: "GET", // post, put, delete, get
			dataType: "json",
			success: function (data) {	
				console.log(data)			
				if (type == 0) {
					
					 
					var r = data.filter((t={},a=>!(t[a]=a in t)));
					let hcs = (JSON.stringify(r));
				 	// console.log(hcs);	
					//hospital charges
					let hospital_charges_length = data.length;
					let hospital_charges = "";
					
					let service_type="";
					let hc_date ="";
					let hc_quantity="";
					let hc_description="";
					let hc_unit_price="";
					let hc_amount="";
					let hc_types ="";
					let hc ="";
					for (let i = 0; i < hospital_charges_length; i++) {
						service_type += data[i].TreatmentServiceTypes.treatment_service_type+ "<br>" ;
						hc_date += data[i].HospitalCharges.date+"<br>" ;
						hc_quantity += data[i].HospitalCharges.quantity+"<br>" ;
						hc_description += data[i].TreatmentServices.description_name+"<br>" ;
						hc_unit_price += data[i].HospitalCharges.unit_price +"<br>" ;
						hc_amount +=data[i].HospitalCharges.amount +"<br>" ;

						hc_types += data[i].TreatmentServiceTypes.treatment_service_type+"<br>";
						// console.log(hc_types.length);
						// hc = hc_types+[i];
						// console.log(hc)
						
						// let result = data.map(({ TreatmentServiceTypes }) => TreatmentServiceTypes)
						// console.log(result)
						
						
					}
						var newArray = [];
						for (let i = 0; i < hospital_charges_length; i++) {
							var dataa = data[i].TreatmentServices	;
							newArray.push(dataa);	
						}
						// console.log(newArray)

						
						const mergeIds = (list) => {
							const subarrsById = {};
							for (const { treatment_department_id, treatment_services_type_id } of list) {
							if (!subarrsById[treatment_department_id]) {
								subarrsById[treatment_department_id] = {treatment_department_id, treatment_services_type_id: [treatment_services_type_id] };
							} else {
								subarrsById[treatment_department_id].treatment_services_type_id.push(treatment_services_type_id);
							}
							}
							return Object.values(subarrsById);
						}
						console.log(mergeIds(newArray));
			
						/////////// correct
						var array1 = [];
						for (let i = 0; i < hospital_charges_length; i++) {
							var arr1 = hc_all[i][0];
							var arr2 = hc_all[i][1];
							// array1.push(arr1);	
							console.log(arr1)
							console.log(arr2)
							let hc_combined = Object.assign(arr1,arr2);
							array1.push(hc_combined);	
							
						}
						console.log(array1);
						/////////////		

					  // for dept name
					  var deptArray = [];
					  for (let i = 0; i < hospital_charges_length; i++) {
						  var dataa = data[i].TreatmentServices;
						  deptArray.push(dataa);	
					  }
					  console.log(deptArray)

					  	var hcArray = [];
					  	for (let i = 0; i < hospital_charges_length; i++) {
							var hcdata = data[i].HospitalCharges;
							hcArray.push(hcdata);	
					  	}
					  	console.log(hcArray)
					    const hc_all = hcArray.map((e,i) => [e,deptArray[i]])
						console.log(hc_all)
						
						
						

					  const mergeDept = (list) => {
						  const subarrsById = {};
						  for (const { treatment_department_id,treatment_services_type_id, description_name } of list) {
							if (!subarrsById[treatment_department_id,treatment_services_type_id]) {
							  subarrsById[treatment_department_id,treatment_services_type_id] = {treatment_department_id,treatment_services_type_id, description_name: [description_name] };
							} else {
							  subarrsById[treatment_department_id,treatment_services_type_id].description_name.push(description_name);
							}
						  }
						  return Object.values(subarrsById);
						}
						
						console.log(mergeDept(deptArray));
		  				//////////////////////

						var deptname =	mergeDept(deptArray)
						console.log(deptname)
						const mergeIds1 = (list) => {
							const subarrsById = {};
							for (const { treatment_department_id,treatment_services_type_id,description_name } of list) {
								if (!subarrsById[treatment_department_id]) {
								subarrsById[treatment_department_id] = {treatment_department_id,treatment_services_type_id: [treatment_services_type_id], description_name: [description_name] };
								} else {
								subarrsById[treatment_department_id].treatment_services_type_id.push(treatment_services_type_id);
								subarrsById[treatment_department_id].description_name.push(description_name);
								}
							}
							return Object.values(subarrsById);
							}
						
						console.log(mergeIds1(deptname));

						

					////////////
					const arr = [data];
					  
					  const uniqueIds = new Set();
					  
					  const unique = arr.filter(element => {
						const isDuplicate = uniqueIds.has(element.TreatmentServiceTypes);
					  
						uniqueIds.add(element.TreatmentServiceTypes);
					  
						if (!isDuplicate) {
						  return true;
						}
					  });
					  
					  //  [{id: 1, name: 'Tom'}, {id: 2, name: 'Nick'}]
					  console.log(unique);

					  


					  let uniqueArray = new Set(data);
					  console.log(uniqueArray);
					
					  let merge_department = mergeIds1(deptname);
					  let merge_length = merge_department.length;
					  
					  let department = "";
					  let services = "";
					  let name_description = "";


					for (let i = 0; i < merge_length; i++) {
	   					console.log(merge_department[i].treatment_department_id)//deptname		
						department += merge_department[i].treatment_department_id+ "<br>"+ "<br>"+ "<br>"+ "<br>";			
						

						let treatment_department_length = merge_department[i].treatment_services_type_id.length;
						console.log(treatment_department_length)
							for (let j = 0; j < treatment_department_length; j++) {
								console.log(merge_department[i].treatment_services_type_id[j])
								console.log(merge_department[i].description_name[j])
								services += department+ merge_department[i].treatment_services_type_id[j]+"<br>"+ merge_department[i].description_name[j]+"<br>";
								
							}
					}


					for (let i = 0; i < hospital_charges_length; i++) {
						//console.log(hospital_charges_length)
						//console.log(data[i].TreatmentServiceTypes);
						

						hospital_charges += 
                            "<tr id='"+   "'>"+
                                // surgery_type_name
                                "<td class='col-md-2 mb-2'>"+
                                    "<label style='font-weight: normal;'"+ 
                                        "class='form-control'>"+
                                        data[i].HospitalCharges.date+										
                                    "</label>"+
                                "</td>"+
                                // charge
                                "<td class='col-md-6 mb-6'>"+
                                    "<label style='font-weight: normal;'"+ 
                                        "class='form-control'>"+
										data[i].TreatmentServices.description_name+
                                    "</label>"+
                                "</td>"+
                                // cancellation amount
                                "<td class='col-md-2 mb-2'>"+
                                    "<input class='form-control' type='text' id='"+
                                        data[i].HospitalCharges.amount+
                                        "' oninput='return computeNetCharges(this);' placeholder='00'>"+
                                "</td>"+
                                // net charged
                                "<td class='col-md-2 mb-2'>"+
                                    "<label style='font-weight: normal;'"+
                                        "class='form-control'>"+
										data[i].TreatmentServiceTypes.treatment_service_type+
                                    "</label>"+
                                "</td>"+
                            "</tr>";

					}

					formParticulars("show");
					$("#uuid").val("899e62e3-42ea-4afe-92d9-1d3c8527c329")
					$(".view_div").empty();
					$("#save_button").show()
					$("#form_card_body").append(
						
						"<div class='row view_div'>"+ 
							"<div class='col-md-12'>"+ 
								"<h5 class='text-info'>"+
								"Particulars"+
								"</h5>"+ 
							"</div>"+ 

							"<div class='col-md-12'>"+ 
								
									
								//////////////////////////////////////////////////////
								"<div class='row invoice-info'>"+	
								"<table class='table table-bordered' id='myTable'>"+
									"<thead>"+
										"<tr>"+
											"<th class='col-md-4 mb-4'>Particulars</th>"+
											"<th class='col-md-2 mb-2'>Charges</th>"+
											"<th class='col-md-2 mb-2'>Cancellation|Return</th>"+
											"<th class='col-md-2 mb-2'>Net Charges</th>"+
										"</tr>"+
									"</thead>"+
									"<tbody class='tbodyclass'>"+
										hospital_charges+
									"</tbody>"+
								"</table>"+
								"</div>"+
								////////////////////////////////////////////////////				
								
										
								"<!-- info row -->" +
								"<div class='row invoice-info'>" +
									"<div class='col-sm-4 invoice-col'>" +
										"<b>Date</b>" +
									"</div>" +
									"<div class='col-sm-2 invoice-col' >" +
										"<b>Description</b>" +
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
										"<b>Quantity</b>" +
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
										"<b>Unit Price</b>" +
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
										"<b>Amount</b>" +
									"</div>" +
								"</div>" +
								"<div class='line'  style='width:98%;border-top: 1px solid black;position: absolute; '></div>" +
								"<!-- /.row -->" +
								
								"<!-- info row -->" +
								"<div class='row invoice-info'>" +
									"<div class='col-sm-2 invoice-col'>" +
									
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:left !important;'>" +
									department+
									"</div>" +
									"<div class='col-sm-3 invoice-col'  style='text-align:left !important;'>" +
									services+	
									"</div>" +
									"<div class='col-sm-1 invoice-col' style='text-align:right !important;'>" +
									hc_quantity+	
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
									hc_unit_price+
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
									hc_amount+
									"</div>" +
								"</div>" +
								"<div class='line'  style='width:98%;border-top: 1px solid black;position: absolute; '></div>" +
								"<!-- /.row -->" +
				
								"<!-- info row -->" +
								"<div class='row invoice-info'>" +
									"<div class='col-sm-4 invoice-col'>" +
										"Total PF Amount" +
									"</div>" +
									"<div class='col-sm-2 invoice-col'  style='text-align:right !important;'>" +
									"unitpriceeee"+
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
										"00.00" +
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
										"00.00" +
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
										"000000"+
									"</div>" +
								"</div>" +
								"<br> <br>" +
								"<!-- /.row -->" +
				
								"<!-- info row -->" +
								"<div class='row invoice-info'>" +
								"<div class='line'  style='width:98%;border-top: 1px solid black;position: absolute; '></div>" +
								"<div class='line'  style='width:98%;border-top: 1.5px solid black;position: absolute; margin-top: 2px; '></div>" +
									"<div class='col-sm-8 invoice-col'>" +
									+
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
										"<b>Balance Due:</b>" +
									"</div>" +
									"<div class='col-sm-2 invoice-col' style='text-align:right !important;'>" +
										"<b> "+"12555"+"</b>" +
									"</div>" +
								"</div>" +
								"<br> <br>" +
				
								"<!-- /.row -->" +
								"<div class='row'>" +
									"<!-- accepted payments column -->" +
									"<div class='col-6'>" +
									"<p>" +
										"PhilHealth deducted by: " +             
										"<br><br>" +
										"Billing Clerk:" +
									"</p>" +
									"</div>" +
								"</div>" +
								"<!-- /.row -->" +
								"<br>" +
								"<p class='lead' style='font-size: 12px; margin-bottom: 50px;'> NOTE:LATE CHARGES WILL BE BILLED TO YOU LATER</p>" +
								
							"</div>"+ 

							
							
						"</div>"
						////////////////////////////////////////////////////
					);
				}
			},
			error: function (data) {
				toastr.error(data.responseJSON.detail);
			},
		});
	});
	
	
	//date attribute
	//get current date to restrict input of future dates
	var current_date = new Date();
	current_date = current_date.toISOString().split("T")[0];
	$("#hire_date").attr("max", current_date);
	$("#birthdate").attr("max", current_date);

	// new bill validation
	$("#form_id").validate({
		submitHandler: function (form, e) {
			e.preventDefault();
			trimInputFields();
			var form_data = new FormData(form);

			if ($("#uuid").val() == "") {
				form_data.append("created_by", sessionStorage.getItem("USER_ID"));
				$.ajax({
					url: apiURL + "employee/",
					type: "POST", // post, put, delete, get
					data: form_data,
					dataType: "json",
					contentType: false,
					processData: false,
					cache: false,
					success: function (data) {
						toastr.success("New Bill has been created.");
						formReset("hide", "");
						resetFileInput();
						loadBillTable();
					},
					error: function (data) {
						toastr.error(data.responseJSON.detail);
					},
				});
			} else {
				form_data.append("updated_by", sessionStorage.getItem("USER_ID"));
				if ($("#photo").val() == "") {
					form_data.delete("photo");
				}
				$.ajax({
					url: apiURL + "employee/" + $("#uuid").val(),
					type: "PUT", // post, put, delete, get
					data: form_data,
					dataType: "json",
					contentType: false,
					processData: false,
					cache: false,
					success: function (data) {
						toastr.success("Bill has been updated.");
						formReset("hide", "");
						resetFileInput();
						loadBillTable();
					},
					error: function (data) {
						toastr.error(data.responseJSON.detail);
					},
				});
			}
		},
		errorElement: "span",
		errorPlacement: function (error, element) {
			error.addClass("invalid-feedback");
			element.closest(".form-group ").append(error);
		},
		highlight: function (element, errorClass, validClass) {
			$(element).addClass("is-invalid");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).removeClass("is-invalid");
		},
	});

	// DataTable
	$.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
		console.log(message);
	};
	loadBillTable = () => {
		$("#bill_table").dataTable().fnClearTable();
		$("#bill_table").dataTable().fnDraw();
		$("#bill_table").dataTable().fnDestroy();
		$("#bill_table").dataTable({
			responsive: true,
			fixedHeader: true,
			processing: true,
			order: [[0, "desc"]],
			dom:
				"<'row'<'col-sm-12 col-md-6'B>>" +
				"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
			buttons: [
				//https://datatables.net/reference/button/collection
				{
					extend: "collection",
					autoClose: "true",
					text: "Options",
					buttons: [
						{
							extend: "print",
							text: '<i class="fa fa-print"></i> Print',
							exportOptions: {
								stripHtml: false,
								columns: [0, 1, 2, 3, 4, 5],
							},
						},
						{
							extend: "pdf",
							text: '<i class="far fa-file-pdf"></i> PDF',
							exportOptions: {
								columns: ":visible",
							},
						},
						{
							extend: "excel",
							text: '<i class="far fa-file-excel"></i> Excel',
							exportOptions: {
								columns: ":visible",
							},
						},
					],
				},
			],
			columns: [
				{
					data: null,
					render: function (aData, type, row) {
						let photo = "";

						photo +=
							"<img src='http://localhost:8000/static/" +
							aData.photo +
							"' style='width: 40px'></img>";
						return photo;
					},

					searchable: true,
				},
				{
					data: null,
					render: function (aData, type, row) {
						let full_name = "";
						full_name += aData.first_name + " " + aData.last_name;
						return full_name;
					},

					searchable: true,
				},
				{
					data: "gender",
					name: "gender",

					searchable: true,
				},
				{
					data: null,
					render: function (aData, type, row) {
						let contact = "";
						contact += aData.contact_number + " " + aData.email;
						return contact;
					},

					searchable: true,
				},
				{
					data: "job",
					name: "job",

					searchable: true,
				},
				{
					data: null,
					render: function (aData, type, row) {
						let status = "";

						if (aData.status != "Inactive") {
							status +=
								'<span class="badge badge-success p-2 w-100">Active</span>';
						} else {
							status +=
								'<span class="badge badge-danger p-2 w-100">Inactive</span>';
						}
						return status;
					},

					searchable: true,
				},
				{
					data: null,
					render: function (aData, type, row) {
						let buttons = "";

						buttons +=
							'<div class="text-center dropdown">' +
							'<div class="btn btn-sm btn-default" data-toggle="dropdown" role="button">' +
							'<i class="fas fa-ellipsis-v"></i>' +
							"</div>" +
							'<div class="dropdown-menu dropdown-menu-right">' +
							'<div class="dropdown-item d-flex" role="button" onClick="return viewData(\'' +
							aData["id"] +
							"',0)\">" +
							'<div style="width: 2rem">' +
							'<i class="fas fa-eye mr-1 text-secondary"></i>' +
							"</div>" +
							"<div>View Profile</div>" +
							"</div>";
						if (aData.status != "Inactive") {
							buttons +=
								'<div class="dropdown-item d-flex" role="button" onClick="return viewData(\'' +
								aData["id"] +
								"',1)\">" +
								'<div style="width: 2rem">' +
								'<i class="fas fa-edit mr-1 text-info"></i>' +
								"</div>" +
								"<div>Edit Profile</div>" +
								"</div>";
						}
						if (aData.status != "Inactive") {
							buttons +=
								'<div class="dropdown-item d-flex" role="button" '+
								'onClick="return deleteData(\'' +
								aData["id"] +
								"')\">" +
								'<div style="width: 2rem">' +
								'<i class="fas fa-trash-alt mr-1 text-danger"></i>' +
								"</div>" +
								"<div>Delete Bill</div>" +
								"</div>";
						}
						buttons += "</div>" + "</div>";
						return buttons; // same class in i element removed it from a element
					},
					sortable: false,
				},
			],

			ajax: {
				url: apiURL + "employee/datatable",
				dataSrc: "",
				type: "GET",
				error: function (xhr, error, code) {
					// console.log(xhr);
					// console.log(code);
				},
			},
			fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				let full_name = "";
				full_name += aData.first_name + " " + aData.last_name;

				let contact = "";
				contact += aData.contact_number + " " + aData.email;

				let photo = "";

				photo +=
					"<img src='http://localhost:8000/static/" +
					aData.photo +
					"'}}' style='width: 40px'></img>";

				let status = "";

				if (aData.status != "Inactive") {
					status += '<span class="badge badge-success p-2 w-100">Active</span>';
				} else {
					status +=
						'<span class="badge badge-danger p-2 w-100">Inactive</span>';
				}

				let buttons = "";

				buttons +=
					'<div class="text-center dropdown">' +
					'<div class="btn btn-sm btn-default" data-toggle="dropdown" role="button">' +
					'<i class="fas fa-ellipsis-v"></i>' +
					"</div>" +
					'<div class="dropdown-menu dropdown-menu-right">' +
					'<div class="dropdown-item d-flex" role="button" onClick="return viewData(\'' +
					aData["id"] +
					"',0)\">" +
					'<div style="width: 2rem">' +
					'<i class="fas fa-eye mr-1 text-secondary"></i>' +
					"</div>" +
					"<div>View Profile</div>" +
					"</div>";
				if (aData.status != "Inactive") {
					buttons +=
						'<div class="dropdown-item d-flex" role="button" onClick="return viewData(\'' +
						aData["id"] +
						"',1)\">" +
						'<div style="width: 2rem">' +
						'<i class="fas fa-edit mr-1 text-info"></i>' +
						"</div>" +
						"<div>Edit Profile</div>" +
						"</div>";
				}
				if (aData.status != "Inactive") {
					buttons +=
						'<div class="dropdown-item d-flex" role="button" '+
						'onClick="return deleteData(\'' +
						aData["id"] +
						"')\">" +
						'<div style="width: 2rem">' +
						'<i class="fas fa-trash-alt mr-1 text-danger"></i>' +
						"</div>" +
						"<div>Delete Bill</div>" +
						"</div>";
				}
				buttons += "</div>" + "</div>";
				$("td:eq(0)", nRow).html(photo);
				$("td:eq(1)", nRow).html(full_name);
				$("td:eq(2)", nRow).html(aData["gender"]);
				$("td:eq(3)", nRow).html(contact);
				$("td:eq(4)", nRow).html(aData["job"]);
				$("td:eq(5)", nRow).html(status);
				$("td:eq(6)", nRow).html(buttons);
			},
			drawCallback: function (settings) {},
		});
	};

	loadBillTable();

	$(window).resize(function () {
		$("table.dataTable").resize();
	});

	//New Data
	$("#new_record").on("click", function () {
		formReset("new", "New Bill");
	});

	//view info
	viewData = (id, type) => {
		$.ajax({
			url: apiURL + "employee/" + id,
			type: "GET", // post, put, delete, get
			dataType: "json",
			success: function (data) {
				if (data.middle_name == null) {
					var middle_name = "";
				} else {
					var middle_name = data.middle_name;
				}
				if (data.extension_name == null) {
					var extension_name = " ";
				} else {
					var extension_name = data.extension_name;
				}
				if (data.house_number == null) {
					var house_number = " ";
				} else {
					var house_number = data.house_number;
				}
				if (data.street == null) {
					var street = " ";
				} else {
					var street = data.street;
				}
				if (type == 0) {
					formReset("show", "View Bill Information");
					$(".view_div").empty();
					$("#form_card_body").append(
						"<div class='row view_div'>" +
							"<div class='col-md-4 text-center'>" +
							"<div class='card'>" +
							"<div class='card-body'>" +
							"<img src='http://localhost:8000/static/" +
							data.photo +
							"'class='rounded-circle' width='150'></img><br>" +
							"<label>" +
							data.first_name +
							" " +
							data.last_name +
							"</label>" +
							"<p>" +
							data.job +
							"</p>" +
							"</div>" +
							"</div>" +
							"</div>" +
							"<div class='col-md-8'>" +
							"<div class='col-md-12'>" +
							"<h5 class='text-info'>Particulars</h5>" +
							"</div>" +
							"<div class='col-md-12'>" +
							"<table class='table table-borderless view-info-table'>" +
							"<tr>" +
							"<th>Full Name</th>" +
							"<td>" +
							data.first_name +
							" " +
							middle_name +
							" " +
							data.last_name +
							" " +
							extension_name +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Gender</th>" +
							"<td>" +
							data.gender +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Birthdate</th>" +
							"<td>" +
							moment(data.birthdate).format("MMMM D, YYYY") +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Birthplace</th>" +
							"<td>" +
							data.birthplace +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Phone Number</th>" +
							"<td>" +
							data.contact_number +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Email Address</th>" +
							"<td>" +
							data.email +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Civil Status</th>" +
							"<td>" +
							data.civil_status +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Address</th>" +
							"<td>" +
							house_number +
							" " +
							street +
							" " +
							data.barangay +
							" " +
							data.city +
							" " +
							data.province +
							" " +
							data.country +
							" " +
							"</td>" +
							"</tr>" +
							"</table>" +
							"</div>" +
							"<div class='col-md-12'>" +
							"<h5 class='text-info'>Job Information</h5>" +
							"</div>" +
							"<div class='col-md-12'>" +
							"<table class='table table-borderless view-info-table'>" +
							"<tr>" +
							"<th>Department</th>" +
							"<td>" +
							data.department +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Job</th>" +
							"<td>" +
							data.job +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Manager</th>" +
							"<td>" +
							data.manager +
							"</td>" +
							"</tr>" +
							"<tr>" +
							"<th>Hired Date</th>" +
							"<td>" +
							moment(data.hire_date).format("MMMM D, YYYY") +
							"</td>" +
							"</tr>" +
							"</table>" +
							"</div>" +
							"</div>" +
							"</div>"
					);
				} else if (type == 1) {
					formReset("update", "Update Bill Information");
					$("#uuid").val(data.id);
					$("#photo_path_placeholder").attr("src", baseURL + data.photo);
					$('[for="photo"]').removeClass("form-required-field");
					$("#photo").removeAttr("required");
					$("#first_name").val(data.first_name);
					$("#middle_name").val(middle_name);
					$("#last_name").val(data.last_name);
					$("#extension_name").val(extension_name);
					$("#birthdate").val(data.birthdate);
					$("#birthplace").val(data.birthplace);
					$("#gender").val(data.gender);
					$("#civil_status").val(data.civil_status);
					$("#house_number").val(data.house_number);
					$("#street").val(data.street);
					$("#barangay").val(data.barangay);
					$("#city").val(data.city);
					$("#province").val(data.province);
					$("#country").val(data.country);
					$("#contact_number").val(data.contact_number);
					$("#email").val(data.email);
					$("#department").val(data.department);
					$("#job").val(data.job);
					$("#manager").val(data.manager);
					$("#hire_date").val(data.hire_date);
				}
			},
			error: function (data) {
				toastr.error(data.responseJSON.detail);
			},
		});
	};

	deleteData = (id) => {
		$('#modal_delete').modal('show')
		$("#form_delete").validate({
			submitHandler: function (form, e) {
				e.preventDefault();
				trimInputFields();
				$.ajax({
					url: apiURL + "employee/" + id + "/" + sessionStorage.getItem("USER_ID"),
					type: "DELETE", // post, put, delete, get
					dataType: "json",
					success: function (data) {
						toastr.success("Record has been deactivated.");
						$('#modal_delete').modal('hide')
						loadBillTable();
					},
					error: function (data) {
						toastr.error(data.responseJSON.detail);
					},
				});
			},
		});

	};

	
	

});
