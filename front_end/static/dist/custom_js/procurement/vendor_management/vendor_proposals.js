$(function () {
  // initialized select2
  $(".select2").select2();
  // load datatable
  loadTable();
  loadTableComparisons();

  proposal_items();
  formReset("hide");
  // infoCardReset("hide")

  $("#form_id")
    .on("submit", function (e) {
      e.preventDefault();
      // trimInputFields();
    })
    .validate({
      rules: {
        // simple rule, converted to {required:true}
        rfq_id: {
          required: true,
        },
        // compound rule
        created_by: {
          required: true,
        },
        contact_no: {
          required: true,
        },

        subtotal: {
          required: true,
        },
        total: {
          required: true,
        },
      },
      messages: {
        rfq_id: {
          required: "please select rfq",
        },

        created_by: {
          required: "please provide a name",
        },
        contact_no: {
          required: "please provide a contact number",
        },
      },
      errorElement: "span",
      errorPlacement: function (error, element) {
        error.addClass("invalid-feedback");
        element.closest(".form-group").append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("is-invalid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("is-invalid");
        $(element).addClass("is-valid");
      },
      submitHandler: function () {
        // form_data.append("_method", "PUT");

        $.ajax({
          url: apiURL + "vendor-proposal/award/" + $("#uuid").val(),
          type: "PUT",
          // data: form_data,
          contentType: "application/json",
          data: JSON.stringify({
            status: $("#status").val(),
            subtotal: parseFloat($("#subtotal").html()),
            tax: parseFloat($("#tax").html()),
            total_amount: parseFloat($("#total").html()),


          }),
          dataType: "json",
          // contentType: false,
          processData: false,
          cache: false,
          success: function (data) {
            if (data) {
              notification("success", "Vendor Awarded");
              formReset("hide");
              loadTable();
            } else {
              console.log("error " + data.message);
            }
          },
          error: function ({ responseJSON }) {},
        });
      },
    });
});

loadTable = () => {
  $.ajaxSetup({
    headers: {
      Accept: "application/json",
      Authorization: "Bearer " + localStorage.getItem("TOKEN"),
      ContentType: "application/x-www-form-urlencoded",
    },
  });
  $("#data-table").dataTable().fnClearTable();
  $("#data-table").dataTable().fnDraw();
  $("#data-table").dataTable().fnDestroy();
  $("#data-table").DataTable({
    ajax: {
      url: apiURL + "vendor-proposal/",
      dataSrc: "",
    },
    aLengthMenu: [5, 10, 20, 30, 50, 100],
    responsive: true,
    serverSide: false,
    dataType: "json",
    type: "GET",
    columns: [
      {
        data: null,
        name: null,
        searchable: true,
        width: "10%",
        render: function (aData, type, row) {
          return moment(aData["created_at"]).format("MMMM D, YYYY");
        },
      },
      {
        data: "request_quotation.quotation_code",
        searchable: "request_quotation.quotation_code",
        width: "5%",
        // render: function (aData, type, row) {
        //   return formatRfqNo(aData.request_quotation.request_quotation_number);
        // },
      },
      {
        data: "u_created_by.vendor.vendor_name",
        name: "u_created_by.vendor.vendor_name",
        searchable: true,
        width: "10%",
      },
      // {
      //   data: null,
      //   name: null,
      //   searchable: true,
      //   width: "10%",
      //   render: function (aData, type, row) {
      //     return "\u20B1" + numberWithCommas(aData.subtotal);
      //   },
      // },
      {
        data: "prepared_by",
        name: "prepared_by",
        searchable: true,
        width: "10%",
        // render: function (aData, type, row) {
        //   return aData.discount + "%";
        // },
      },
      {
        data: "contact_no",
        name: "contact_no",
        searchable: true,
        width: "10%",
        // render: function (aData, type, row) {
        //   return aData.tax + "%";
        // },
      },
      {
        data: null,
        name: null,
        searchable: true,
        width: "10%",
        render: function (aData, type, row) {
          return "\u20B1" + numberWithCommas(aData.total_amount);
        },
      },

      {
        data: null,
        name: null,
        searchable: true,
        width: "5%",
        render: function (aData, type, row) {
          if (aData.status === "Awarded") {
            let status =
              '<label class="text-left badge badge-primary p-2 w-100"> ' +
              aData.status +
              "</label> ";
            return status;
          } else if (aData.status === "Pending") {
            let status =
              '<label class="text-left badge badge-warning p-2 w-100"> ' +
              aData.status +
              "</label> ";
            return status;
          } else {
            let status =
              '<label class="text-left badge badge-danger p-2 w-100"> ' +
              aData.status +
              "</label> ";
            return status;
          }
        },
      },
      {
        data: null,
        width: "2%",
        render: function (aData, type, row) {
          let buttons = "";
          buttons +=
            '<div class="text center dropdown"><div class="btn btn-sm btn-default" data-toggle="dropdown" role="button">' +
            '<i class="fas fa-ellipsis-v"></i></div><div class="dropdown-menu dropdown-menu-right">';
          //view
          buttons +=
            '<div class="dropdown-item d-flex" role="button" onClick="return editData(\'' +
            aData["id"] +
            "',0)\">" +
            '<div style="width: 2rem"> <i class= "fas fa-eye mr-1"></i></div>' +
            "<div> View</div></div>";

            if(aData.status == "Pending"){
          //deactivate
          buttons +=
            '<div class="dropdown-item d-flex" role="button" data-toggle="modal" data-target="#modal-status" onClick="return deleteData(\'' +
            aData["id"] +
            "')\"  >" +
            '<div style="width: 2rem"> <i class= "fas fa-trash mr-1"></i></div>' +
            "<div> Deny</div></div>";
            }
          //activate
          // buttons +=
          //   '<div class="dropdown-item d-flex" role="button" data-toggle="modal" data-target="#modal-status" onClick="return deleteData(\'' +
          //   aData["id"] +
          //   "')\"  >" +
          //   '<div style="width: 2rem"> <i class="fas fa-redo"></i></div>' +
          //   "<div>Reactivate</div></div>";

          buttons += "</div></div>";

          return buttons;
        },
      },
    ],
  });
};

// datatable for pr products
proposal_items = () => {
  $("#proposal-items-table").dataTable().fnClearTable();
  $("#proposal-items-table").dataTable().fnDraw();
  $("#proposal-items-table").dataTable().fnDestroy();
  proposal_items_table = $("#proposal-items-table").DataTable({
    info: false,
    // paging: false,
    responsive: true,
    aLengthMenu: [5, 10, 20, 30, 50, 100],
    columns: [
  
      { width: "10%" },
      { width: "15%" },
      { width: "25%" },
      { width: "10%" },
      { width: "15%" },
      { width: "10%" },
      { width: "10%" },
    ],
  });
};
var new_product = [];
var edit_new_product = [];

// function to show details for viewing/updating
editData = (id,type) => {
  // insert tr??
  $(".is-invalid").removeClass("is-invalid");
  $(".is-valid").removeClass("is-valid");

  // remove all data from array

  new_product.splice(0, new_product.length);
 
  $.ajax({
    url: apiURL + "vendor-proposal/" + id,
    type: "GET",
    dataType: "json",
    success: function (data) {
      if (data) {
        if(type == 0){
          formReset("show");
          $("#subtotal").html(data["subtotal"]);

          $("#discount").html(data["discount"]);
          $("#tax").html(data["tax"]);
          $("#total").html(data["total_amount"]);
        }
     
       
        if (data["status"] == "Awarded") {
          $(".submit").hide();
          $(".proposal-status").removeClass("text-danger");
          $(".proposal-status").addClass("text-primary");
          $(".proposal-status").html("*Awarded Proposal");
          $(".proposal-status").show();
        } else if (data["status"] == "Inactive") {
          $(".submit").hide();
          $(".proposal-status").removeClass("text-primary");
          $(".proposal-status").addClass("text-danger");
          $(".proposal-status").html("*Inactive Proposal");
          $(".proposal-status").show();
        } else {
          $(".submit").show();
          $(".proposal-status").hide();
        }
        

        $("#uuid").val(data["id"]);
        $("#proposal_message").empty()
        $("#vendor").html(data.u_created_by.vendor["vendor_name"]);
        $("#proposal_date").html(
          moment(data["created_at"]).format("MMMM D, YYYY")
        );

        $("#prepared_by").html(data["prepared_by"]);
        $("#contact_no").html(data["contact_no"]);

        $("#rfq_id").val(data["id"]).trigger("change");

     

        $("#discount").html(data["discount"]);

        $("#proposal_message").append(data["message"]);
        $("#notes").html(data["notes"]);

    
            


        proposal_items_table.clear().draw();
        
        // Loop here
        let bg_color = ""
        let fas_status = ""
        let status_text = ""
        for (let i in data.vendor_bidding_item) {
          if(data.vendor_bidding_item[i].status == "active") {
            bg_color = "primary"
             fas_status = "trash"
            status_text = "Remove"
          }
          else {
            bg_color = "danger"
             fas_status = "redo"
            status_text = "Reactivate"


          }
          

          
            proposal_items_table.row
            .add([
              
              // product catagory
              data.vendor_bidding_item[i].category.category_name,

              // product name
              data.vendor_bidding_item[i].product_name,


              // price
              "\u20B1" +
                numberWithCommas(data.vendor_bidding_item[i].price_per_unit),

              // quantity input field
              // '<input class="form-control product_quantity_of_manual_item" type="number" min="1" value=' +
              data.vendor_bidding_item[i].quantity,
              //  +" />",

              // total cost
              "\u20B1" + numberWithCommas(data.vendor_bidding_item[i].price_per_unit * data.vendor_bidding_item[i].quantity),

              // status
              '<label class="text-left badge badge-'+bg_color+' p-2 w-100"> ' + data.vendor_bidding_item[i].status +"</label> ",
             
                
              //action
          
             '<div class="text center dropdown"> <div class="btn btn-sm btn-default" data-toggle="dropdown" role="button">' +
                '<i class="fas fa-ellipsis-v"></i></div> <div class="dropdown-menu dropdown-menu-right">' +
                // view
                '<div class="dropdown-item d-flex" role="button" data-toggle="modal" data-target="#modal-add-new-product" data-toggle="modal" onClick="return editPrDetail( \'' +
                data.vendor_bidding_item[i].id +
                '\',this.parentNode.parentNode.parentNode.parentNode);"><div style="width: 2rem"> <i class="fas fa-eye mr-1"></i></div><div> View</div></div>' +
                // remove
                '<div class="dropdown-item d-flex " role="button" data-toggle="modal" onClick="return removePrDetail(this.parentNode.parentNode.parentNode.parentNode, \'' +
             data.id + "', '"+ data.vendor_bidding_item[i].id + "', '" +data.vendor_bidding_item[i].status+
                '\');"><div style="width: 2rem"> <i class="fas fa-'+fas_status+' mr-1"></i></div><div>'+status_text+'</div></div>' +
                "</div></div>"
            ])
            .draw();

          
          // push data to new product array object
          edit_new_product.push({
            new_product_category:
              data.vendor_bidding_item[i].new_product_category,

            new_product_name: data.vendor_bidding_item[i].new_product_name,

            new_product_quantity: data.vendor_bidding_item[i].quantity,

            description: data.vendor_bidding_item[i].description,
            quantity: data.vendor_bidding_item[i].quantity,
          });
          // edit_new_product.filter(onlyUnique);
        }

        // console.log(new_product);

        $("#form_id input[type='number'],input[type='text'], textarea").prop(
          "disabled",
          true
        );
      } else {
        notification("error", "Error!", data.message);
      }
    },
    error: function (data) {},
  });
};

// 		action = show, hide
formReset = (action = "hide") => {
  $("html, body").animate({ scrollTop: 0 }, "slow");

  if (action == "hide") {
    $(".is-invalid").removeClass("is-invalid");
    $(".is-valid").removeClass("is-valid");
    $(".invalid-feedback").hide();
    // hide and clear form
    $("#uuid").val("");
    $("#div_form").hide();
    proposal_items_table.clear().draw();
    $("#form_id input, select, textarea").prop("disabled", false);
  } else {
    // show
    $("#div_form").show();
    $("#form_id input, select, textarea").prop("disabled", false);
    $("#form_id button").prop("disabled", false);
  }
};

loadTableComparisons = () => {
  $.ajaxSetup({
    headers: {
      Accept: "application/json",
      Authorization: "Bearer " + localStorage.getItem("TOKEN"),
      ContentType: "application/x-www-form-urlencoded",
    },
  });
  $("#data-table-comparisons").dataTable().fnClearTable();
  $("#data-table-comparisons").dataTable().fnDraw();
  $("#data-table-comparisons").dataTable().fnDestroy();
  $("#data-table-comparisons").DataTable({
    // dom: 'lr<"table-filter-container">tip',
    initComplete: function (settings) {
      $("#table-filter select").on("change", function () {
        $("#data-table-comparisons")
          .DataTable()
          .search(this.value)
          .draw(this.value);
      });
    },
    ajax: {
      // url: apiURL + "vendor-proposal/bid/" + '6b1e56e7-053f-4f26-b771-15835d49c0f8',
      url: apiURL + "vendor-proposal/",

      dataSrc: "",
    },
    aLengthMenu: [5, 10, 20, 30, 50, 100],
    responsive: true,
    serverSide: false,
    dataType: "json",
    type: "GET",
    columns: [
      {
        data: "request_quotation.quotation_code",
        name: "request_quotation.quotation_code",
        // visible: false,
        searchable: true,
        width: "10%",
      },
      {
        data: "u_created_by.vendor.vendor_name",
        name: "u_created_by.vendor.vendor_name",
        searchable: true,
        width: "10%",
      },

      {
        data: null,
        name: null,
        searchable: true,
        width: "10%",
        render: function (aData, type, row) {
          return "\u20B1" + numberWithCommas(aData.subtotal);
        },
      },
      {
        data: null,
        name: null,
        searchable: true,
        width: "10%",
        render: function (aData, type, row) {
          return "\u20B1" + aData.discount;
        },
      },
      {
        data: null,
        name: null,
        searchable: true,
        width: "10%",
        render: function (aData, type, row) {
          return "\u20B1" + aData.tax;
        },
      },
      {
        data: null,
        name: null,
        searchable: true,
        width: "10%",
        render: function (aData, type, row) {
          return "\u20B1" + numberWithCommas(aData.total_amount);
        },
      },
      {
        data: null,
        name: null,
        searchable: true,
        width: "10%",
        render: function (aData, type, row) {
          let buttons = "";
          buttons +=
            '<div class="text center dropdown"><div class="btn btn-sm btn-default" data-toggle="dropdown" role="button">' +
            '<i class="fas fa-ellipsis-v"></i></div><div class="dropdown-menu dropdown-menu-right">';
          //view
          buttons +=
            '<div class="dropdown-item d-flex" role="button" onClick="return editData(\'' +
            aData["id"] +
            "',0)\">" +
            '<div style="width: 2rem"> <i class= "fas fa-eye mr-1"></i></div>' +
            "<div> View</div></div>";

    

          buttons += "</div></div>";

          return buttons;
        },
      },
    ],
  });
};

loadRfqCode = () => {
  $.ajax({
    url: apiURL + "request-quotation/rfq_code/",
    type: "GET",
    dataType: "json",
    headers: {
      Accept: "application/json",
      Authorization: "Bearer " + localStorage.getItem("TOKEN"),
      ContentType: "application/x-www-form-urlencoded",
    },
    success: function (responseData) {
      if (responseData) {
        $("#rfq_code").empty();
        $("#rfq_code").append("<option disabled selected>Select Code</option>");
        $.each(responseData, function (i, dataOptions) {
          var options = "";
          options =
            "<option value='" +
            dataOptions.quotation_code +
            "'>" +
            dataOptions.quotation_code +
            "</option>";

          $("#rfq_code").append(options);
        });
      } else {
        // notification("error", "Error!", responseData.message);
        console.log("error", "Error!", responseData.success);
      }
    },
    error: function ({ responseJSON }) {},
  });
};

loadRfqCode();

// function to show details for viewing/updating
editPrDetail = (id, tr) => {
  // $(".modal-title").html("View Product");

  $(".is-invalid").removeClass("is-invalid");
  $(".is-valid").removeClass("is-valid");

  $.ajax({
    url: apiURL + "vendor-proposal/bidding-item/" + id,
    type: "GET",
    dataType: "json",
    success: function (data) {
      if (data) {
        $("#category_id").val(data.category["category_name"]).val();

        $("#new_item_name").val(data["product_name"]),
          $("#new_item_description").val(data["description"]),
          $("#new_item_price").val(data["price_per_unit"]),
          $("#new_item_quantity").val(data["quantity"]);

        $("#product_modal_form input, select, textarea").prop("disabled", true);
      } else {
        notification("error", "Error!", data.message);
      }
    },
    error: function (data) {},
  });
};

// remove purchase request items from table
removePrDetail = (tr, proposal_id,proposal_item_id,status) => {
  console.log( $("td:eq(4)", tr).html().replace(/[^0-9\.-]+/g,""))
  let new_status= ""
  let subtotal = parseInt($('#subtotal').html())
  let tax = parseInt($('#tax').html())
  let total = parseInt($('#total').html())

  if(status =="removed"){
    new_status = "active"
    subtotal += parseInt($("td:eq(4)", tr).html().replace(/[^0-9\.-]+/g,""))
    
  } 
  else{
    subtotal -= parseInt($("td:eq(4)", tr).html().replace(/[^0-9\.-]+/g,""))

    new_status = "removed"  
  }

  tax = parseInt(subtotal) * 0.1;
  total = parseInt(subtotal) + parseFloat(tax.toFixed(2));

  $.ajax({
    url: apiURL + "bidding-item/status/" + proposal_item_id,
    type: "DELETE",
    data: JSON.stringify({
      status: new_status
    }),
    contentType: "application/json",
    processData: false,
    cache: false,
    dataType: "json",
    success: function (data) {
      console.log(subtotal)
  //       // notification("success", "Success!", data.message);
        editData(proposal_id,1)
          $('#subtotal').html(parseInt(subtotal))
          $('#tax').html(tax.toFixed(2))
          $('#total').html(total)
    
        loadTable();
     
        // notification("info", "Deleted!", "Record Deleted");

    },
    error: function ({ responseJSON }) {},
  });
};

deleteData = (id) => {
  $("#del_uuid").val(id);

  console.log(id);
  $("#changeStatus").click(() => {
    $.ajax({
      url: apiURL + "vendor-proposal/" + id,
      type: "DELETE",
      dataType: "json",
      success: function (data) {
        if (data) {
          // notification("success", "Success!", data.message);
          console.log("success" + data);
          loadTable();
        } else {
          notification("info", "Deleted!", "Record Deleted");

          console.log("error" + data);
          loadTable();
        }
      },
      error: function ({ responseJSON }) {},
    });
    $("#changeStatus").attr("data-dismiss", "modal");
  });
};
