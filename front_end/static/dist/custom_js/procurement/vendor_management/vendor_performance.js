var token = localStorage.getItem("TOKEN");
var user_id = localStorage.getItem("ID");



$(function () {
  // load datatable
  $(".select2").select2();

  loadEval("");
  



});


loadEval = (category_id) =>{

  if( category_id !=""){
    filteredURL = apiURL + "vendor-evaluation/charts/ratings/"+category_id
   }


   else{
    filteredURL = apiURL + "vendor-evaluation/charts/ratings/none" 
     
   }



  $.ajax({
    url: filteredURL,
    type: "GET",
    dataType: "json",
    headers: {
      Accept: "application/json",
      Authorization: "Bearer " + localStorage.getItem("TOKEN"),
    },
    success: function (data) {
      if (data) {

        let bg_cost, bg_timeliness, bg_quality,bg_reliability,bg_availability,bg_reputation
        function getEval(key, score) {
          if(key == "cost"){
            return (score > 60) ? (bg_cost ='primary') : ((score <= 60 && score >= 30) ? (bg_cost ='warning') : (bg_cost= 'danger'));
          }
          else if(key == "availability"){
            return (score > 60) ? (bg_availability ='primary') : ((score <= 60 && score >= 30) ? (bg_availability ='warning') : (bg_availability= 'danger'));
          }
          else if(key == "timeliness"){
            return (score > 60) ? (bg_timeliness ='primary') : ((score <= 60 && score >= 30) ? (bg_timeliness ='warning') : (bg_timeliness= 'danger'));

          }
          else if(key == "quality"){
            return (score > 60) ? (bg_quality ='primary') : ((score <= 60 && score >= 30) ? (bg_quality ='warning') : (bg_quality= 'danger'));
          }
          else if(key == "reliability"){
            return (score > 60) ? (bg_reliability ='primary') : ((score <= 60 && score >= 30) ? (bg_reliability ='warning') : (bg_reliability= 'danger'));
            
          }
          else if(key == "reputation"){
            return (score > 60) ? (bg_reputation ='primary') : ((score <= 60 && score >= 30) ? (bg_reputation ='warning') : (bg_reputation= 'danger'));
            
          }

        }

        $("#vendor_rating_body").empty();
        let vendor_rating_body = ''
        
        
        for(let i in data){
          if(data[i] != 0){
   
              getEval("cost",data[i][0].eval_data.cost)
              getEval("availability",data[i][0].eval_data.availability)
              getEval("timeliness",data[i][0].eval_data.timeliness)
              getEval("quality",data[i][0].eval_data.quality)
              getEval("reliability",data[i][0].eval_data.reliability)
              getEval("reputation",data[i][0].eval_data.reputation)


    

            vendor_rating_body += ' <div class="col-md-4">' + '<div class="card">' 
            + '<div class="card-header">' + '<h3 class="card-title">'+data[i][0].vendor_name+' - '+data[i][0].category+'</h3>'+'</div>'+
            '<div class="card-body">' + '<label>Cost</label>' + 
            
            '<div class="progress mb-3">' + 
            '<div class="progress-bar bg-'+bg_cost+'" role="progressbar" aria-valuenow="'+data[i][0].eval_data.cost+'" aria-valuemin="0" aria-valuemax="100" style="width: '+data[i][0].eval_data.cost+'%">'
            + ''+data[i][0].eval_data.cost+'%</div>'+ '</div>' + 
            
            '<label>Timeliness</label>' + '<div class="progress mb-3">' + 
            '<div class="progress-bar bg-'+bg_timeliness+'" role="progressbar" aria-valuenow="'+data[i][0].eval_data.timeliness+'" aria-valuemin="0" aria-valuemax="100" style="width: '+data[i][0].eval_data.timeliness+'%">'
            + ''+data[i][0].eval_data.timeliness+'%</div>'+ '</div>' + 
            
            '<label>Reliability</label>' + '<div class="progress mb-3">' + 
            '<div class="progress-bar bg-'+bg_reliability+'" role="progressbar" aria-valuenow="'+data[i][0].eval_data.reliability+'" aria-valuemin="0" aria-valuemax="100" style="width: '+data[i][0].eval_data.reliability+'%">'
            + ''+data[i][0].eval_data.reliability+'%</div>'+ '</div>' + 
            
            '<label>Quality</label>' + '<div class="progress mb-3">' + 
            '<div class="progress-bar bg-'+bg_quality+'" role="progressbar" aria-valuenow="'+data[i][0].eval_data.quality+'" aria-valuemin="0" aria-valuemax="100" style="width: '+data[i][0].eval_data.quality+'%">'
            + ''+data[i][0].eval_data.quality+'%</div>'+ '</div>' +
            
            '<label>Availability</label>' + '<div class="progress mb-3">' + 
            '<div class="progress-bar bg-'+bg_availability+'" role="progressbar" aria-valuenow="'+data[i][0].eval_data.availability+'" aria-valuemin="0" aria-valuemax="100" style="width: '+data[i][0].eval_data.availability+'%">'
            + ''+data[i][0].eval_data.availability+'%</div>'+ '</div>' +
             
            '<label>Reputation</label>' + '<div class="progress mb-3">' + 
            '<div class="progress-bar bg-'+bg_reputation+'" role="progressbar" aria-valuenow="'+data[i][0].eval_data.reputation+'" aria-valuemin="0" aria-valuemax="100" style="width: '+data[i][0].eval_data.reputation+'%">'
            + ''+data[i][0].eval_data.reputation+'%</div>'+ '</div>' + '</div>' + '</div>' +'</div>'


          }
       
        }
        // vendor_rating_body+= '</div>'
        $("#vendor_rating_body").append(vendor_rating_body);

      } else {
        notification("error", "Error!", data.detail);

        console.log("error" + data);
        loadTable();
      }
    },
    complete: function (data) {
      
     },
    error: function ({ responseJSON }) {},
  });
}





// function to load category
loadCategory = () => {
  $.ajax({
    url: apiURL + "category/",
    type: "GET",
    headers: {
      Accept: "application/json",
      Authorization: "Bearer " + localStorage.getItem("TOKEN"),
    },
    dataType: "json",
    success: function (responseData) {
      if (responseData) {
        $("#category_id").empty();

        var options = "";
         options += "<option value=''>All Category</option>";

        $.each(responseData, function (i, dataOptions) {
          options +=
            "<option value='" +
            dataOptions.id +
            "'>" +
            dataOptions.category_name +
            "</option>";

          });
          $("#category_id").append(options);
      } else {
        // notification("error", "Error!", responseData.message);
        console.log("error", "Error!", responseData.success);
      }
    },
    error: function ({ responseJSON }) {},
  });
};

loadCategory();


$("[name=category_id]").on("change", function () {
 category_id = $(this).val();
 loadEval(category_id)
 

});



// $(function () {
//   window.stepper = new Stepper(document.querySelector(".bs-stepper"));

//   // load datatable
//   loadTable();

//   formReset('hide')
//   $("#form_id")
//     .on("submit", function (e) {
//       e.preventDefault();
//       // trimInputFields();
//     })
//     .validate({
//       rules: {
//         // simple rule, converted to {required:true}
//         message: {
//           required: true,
//         },
//         // compound rule
//         score: {
//           required: true,
//         },

//         satisfaction: {
//           required: true,
//         },
//       },
//       messages: {
//         message: {
//           required: "please provide a message",
//         },
//       },
//       errorElement: "span",
//       errorPlacement: function (error, element) {
//         error.addClass("invalid-feedback");
//         element.closest(".form-outline").append(error);
//       },
//       highlight: function (element, errorClass, validClass) {
//         $(element).addClass("is-invalid");
//       },
//       unhighlight: function (element, errorClass, validClass) {
//         $(element).removeClass("is-invalid");
//         $(element).addClass("is-valid");
//       },
//       submitHandler: function () {
//         var date = Date.now();

//         // var form_data = new FormData(this);
//         // var form_data = new FormData(document.getElementById("form_id"));
//         // console.log($("input:radio[name=quality]:checked").val());
//         console.log($("#message").val());
//         // console.log($("#vendor_name").val());

//         //   console.log();
//         $.ajax({
//           url: apiURL + "vendor-evaluation/",
//           type: "POST",
//           // data: form_data,
//           contentType: "application/json",
//           data: JSON.stringify({
//             // vendor_evaluation_schedule_id: $(
//             //   "#vendor_evaluation_schedule_id"
//             // ).val(),
//             vendor_id: $("#vendor_id").val(),
//             message: $("#message").val(),
//             defect_comments: $("#defect_comment").val(),

//             product_quality: $("input:radio[name=quality]:checked").val(),
//             on_time_delivery: $("input:radio[name=ontime]:checked").val(),

//             date: date,
//           }),
//           dataType: "json",
//           // contentType: false,
//           processData: false,
//           cache: false,
//           headers: {
//             Accept: "application/json",
//             Authorization: "Bearer " + localStorage.getItem("TOKEN"),
//           },
//           success: function (data) {
//             if (data) {
//               notification("success", "Evaluation Successfuly Submitted");

//               formReset("hide")

//               loadTable();

//             } else {
//               notification("error", "Error submitting evaluation");
//               console.log("error");
//             }
//           },
//           error: function ({ responseJSON }) {
//             console.log(responseJSON);
//           },
//         });
//       },
//     });
// });

// // datatable
// loadTable = () => {

//   $.ajaxSetup({
//     headers: {
//       Accept: "application/json",
//       Authorization: "Bearer " + localStorage.getItem("TOKEN"),
//       ContentType: "application/x-www-form-urlencoded",
//     },
//   });
//   $("#data-table").dataTable().fnClearTable();
//   $("#data-table").dataTable().fnDraw();
//   $("#data-table").dataTable().fnDestroy();
//   $("#data-table").DataTable({
//     ajax: {
//       url:
//         apiURL +
//         "vendor-evaluation/",
//       dataSrc: "",
//     },

//     responsive: true,
//     serverSide: false,
//     dataType: "json",
//     type: "GET",
//     columns: [

//       {
//         data: "vendor.vendor_name",
//         name: "vendor.vendor_name",
//         searchable: true,
//         width: "20%",
//         // className: "dtr-control",
//       },
//       {
//         data: "product_quality",
//         name: "product_quality",
//         searchable: true,
//         width: "10%",
//         // className: "dtr-control",
//       },

//       {
//         data: "on_time_delivery",
//         name: "on_time_delivery",
//         searchable: true,
//         width: "10%",
//       },
  
//       {
//         data: "message",
//         name: "message",
//         searchable: true,
//         width: "25%",
//         // className: "dtr-control",
//       },
//       {
//         data: null,
//         width: "10%",
//         render: function (aData, type, row) {
//           let buttons = "";
//           buttons +=
//             '<div class="text center dropdown"><div class="btn btn-sm btn-default" data-toggle="dropdown" role="button">' +
//             '<i class="fas fa-ellipsis-v"></i></div><div class="dropdown-menu dropdown-menu-right">';
//           //view
//           buttons +=
//             '<div class="dropdown-item d-flex role="button" onClick="return editData(\'' +
//             aData["id"] +
//             "',0)\">" +
//             '<div style="width: 2rem"> <i class= "fas fa-eye mr-1"></i></div>' +
//             "<div> View</div></div>";

//           //delete
//           buttons +=
//             '<div class="dropdown-item d-flex role="button" data-toggle="modal" data-target="#modal-status" onClick="return deleteData(\'' +
//             aData["id"] +
//             "')\"  >" +
//             '<div style="width: 2rem"> <i class= "fas fa-trash mr-1"></i></div>' +
//             "<div> Deactivate</div></div>";

//           buttons += "</div></div>";
//           return buttons; // same class in i element removed it from a element
//         },
//       },
//     ],
//   });
// };

// // function to show details for viewing/updating
// editData = (id, type) => {
//   $(".is-invalid").removeClass("is-invalid");
//   $(".is-valid").removeClass("is-valid");

//   $("#modal-xl").modal("show");

//   $.ajax({
//     url: apiURL + "vendor-evaluation/" + id,
//     type: "GET",
//     dataType: "json",
//     success: function (data) {
//       if (data) {
//         $("#uuid").val(data["id"]);
//         $("#product_quality").val(data["product_quality"]);
//         $("#user_satisfaction").val(data["user_satisfaction"]);
//         $("#on_time_delivery").val(data["on_time_delivery"]);
//         $("#product_defect").val(data["product_defect"]);

//         $("#defect_comments").val(data["defect_comments"]);
//         $("#message").val(data["message"]);

//         $("#form_id input, select, textarea").prop("disabled", true);
//       } else {
//         notification("error", "Error!", data.message);
//       }
//     },
//     error: function (data) {},
//   });
// };

// // delete purchase requisition
// deleteData = (id) => {
//   $("#del_uuid").val(id);

//   $("#changeStatus").click(() => {
//     $.ajax({
//       url: apiURL + "vendor-evaluation/del/" + id,
//       type: "Delete",
//       dataType: "json",
//       success: function (data) {
//         if (data) {
//           // notification("success", "Success!", data.message);
//           console.log("success" + data);
//           loadTable();
//         } else {
//           notification("info", "Deactivated!", "Record Deactivated");

//           console.log("error" + data);
//           loadTable();
//         }
//       },
//       error: function ({ responseJSON }) {},
//     });
//     $("#changeStatus").attr("data-dismiss", "modal");
//   });
// };



// // 		action = show, hide
// formReset = (action = "hide") => {
//   $("html, body").animate({ scrollTop: 0 }, "slow");

//   if (action == "hide") {
//     $(".is-invalid").removeClass("is-invalid");
//     $(".is-valid").removeClass("is-valid");
//     $(".invalid-feedback").hide();
//     // hide and clear form
//     $("#uuid").val("");
//     $("#div_form").hide();
//     // proposal_items_table.clear().draw();

//     $("#form_id input, select, textarea").prop("disabled", false);
//   } else {
//     // show
//     $("#div_form").show();
//     $("#form_id input, select, textarea").prop("disabled", false);
//     $("#form_id button").prop("disabled", false);
//   }
// };


// // all pending request
// loadVendor = () => {
//   $.ajax({
//     url: apiURL + "vendor/",
//     type: "GET",
//     dataType: "json",
//     headers: {
//       Accept: "application/json",
//       Authorization: "Bearer " + localStorage.getItem("TOKEN"),
//       ContentType: "application/x-www-form-urlencoded",
//     },
//     success: function (responseData) {
//       if (responseData) {
//         $("#vendor_id").empty();
//         $("#vendor_id").append("<option disabled selected>Select Vendor</option>");

//         $.each(responseData, function (i, dataOptions) {
//           var options = "";
    
//           options =
//             "<option value='" +
//             dataOptions.id +
//             "'>" +
//             dataOptions.vendor_name +
//             "</option>";

//           $("#vendor_id").append(options);
//         });
//       } else {
//         // notification("error", "Error!", responseData.message);
//         console.log("error", "Error!", responseData.success);
//       }
//     },
//     error: function ({ responseJSON }) {},
//   });
// };

// loadVendor();


// $.ajax({
//   url: apiURL + "vendor-evaluation/charts/ratings",
//   type: "GET",
//   dataType: "json",
//   headers: {
//     Accept: "application/json",
//     Authorization: "Bearer " + localStorage.getItem("TOKEN"),
//     ContentType: "application/x-www-form-urlencoded",
//   },
//   success: function (responseData) {
//     if (responseData) {
//       //   console.log(responseData)
//       console.log(responseData);
//       var vendorRatingCanvas = $("#vendor_chart_ratings").get(0).getContext("2d");
//       var horizontalBarData = {
//         // category_name
//         labels: Object.keys(responseData),
//         datasets: [
//           {
//               label: "Ratings",
//               data: Object.values(responseData),

//           }]
//       };

//       var barOptions = {
//         maintainAspectRatio: false,
//         responsive: true,
//         scales: {
//           xAxes: [{
//               ticks: {
//                 beginAtZero: true
//               }
//           }],
//           yAxes: [{
//               stacked: true
//           }]
//       }
//       };
//       //Create pie or douhnut chart
//       // You can switch between pie and douhnut using the method below.
//       new Chart(vendorRatingCanvas, {
//         type: "horizontalBar",
//         data: horizontalBarData,
//         options: barOptions,
//       });
//     } else {
//       // notification("error", "Error!", responseData.message);
//       // console.log("error", "Error!", "No data");
//     }
//   },
//   error: function ({ responseJSON }) {},
// });