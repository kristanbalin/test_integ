from datetime import date, datetime, timedelta
from typing import List, Optional
from pydantic import BaseModel



class Medicine_PR(BaseModel):
    medpr_id:str
    medicine_no:int
    medicine_id:Optional[str]  
    quantity:int
    prescription_id:Optional[str]  
    
    status:str
    created_by:Optional[str]  
    created_at:datetime
    updated_by:Optional[str]  
    updated_at:Optional[datetime]  
  


    class Config():
        orm_mode = True


class ShowMedicine_PR(Medicine_PR):
    class Config():
        orm_mode = True


class CreateMedicine_PR(BaseModel):
    medicine_id:Optional[str]  
    quantity:int
    prescription_id:Optional[str]  
   
    created_by:Optional[str]  
    created_at:datetime

    class Config():
        orm_mode = True

class UpdateMedicine_PR(BaseModel):
    medicine_id:Optional[str]  
    quantity:int
    prescription_id:Optional[str] 

    updated_by:Optional[str]  
    updated_at:Optional[datetime]   

    
  
    class Config():
        orm_mode = True
