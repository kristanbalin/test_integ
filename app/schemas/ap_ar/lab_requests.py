from datetime import date, datetime, timedelta
from typing import List, Optional
from pydantic import BaseModel



class LabRequest(BaseModel):
    id:str
    lab_test_id:str
    patient_id:str
    lab_request_no:str
    quantity:float
    cancellation_return:Optional[float]

    is_active:str
    status:str
    created_at:datetime
    updated_at:datetime



    class Config():
        orm_mode = True


class ShowLabRequest(LabRequest):
    class Config():
        orm_mode = True


class CreateLabRequest(BaseModel):
    lab_test_id:str
    patient_id:str
    quantity:float

   
    created_at:datetime

    class Config():
        orm_mode = True

class UpdateLabRequest(BaseModel):
    lab_test_id:str
    patient_id:str
    lab_request_no:str

    updated_at:datetime
  
    class Config():
        orm_mode = True
