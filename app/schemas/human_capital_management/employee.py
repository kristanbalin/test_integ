from datetime import date
from pydantic import BaseModel
from typing import Optional, Text, List

from . import employee_type,department


# Employee
class Employee(BaseModel):
    photo: str
    first_name: str
    middle_name: Optional[str]
    last_name: str
    extension_name: Optional[str]
    birthdate: date
    birthplace: str
    gender: str
    civil_status: str
    house_number: Optional[str]
    street: Optional[str]
    barangay: str
    city: str
    province: str
    country: str
    contact_number: str
    email: str
  
    job: str
    hire_date: date
    manager: str
    department_id: str 
    employee_type_id: str 
    class Config():
        orm_mode = True

class ShowEmployee(BaseModel):
  
    id: str
    photo: str
    first_name: str
    middle_name: Optional[str]
    last_name: str
    extension_name: Optional[str]
    birthdate: date
    birthplace: str
    gender: str
    civil_status: str
    house_number: Optional[str]
    street: Optional[str]
    barangay: str
    city: str
    province: str
    country: str
    contact_number: str
    email: str
  
    job: str
    hire_date: date
    manager: str
    status: str
    department: department.ShowDepartment 
    employee_types: Optional[employee_type.ShowEmployeeType]
  
    class Config():
        orm_mode = True