from datetime import date
from pydantic import BaseModel
from typing import Optional, Text, List




# Product
class Product(BaseModel):
    # product_pic: str
    product_name: str
    category_id: str 
    is_active: Optional[bool]
    class Config():
        orm_mode = True

class ProductCateg(BaseModel):
    category_name: str
    description: str


class ShowProductCateg(BaseModel):
    id: str
    category_name: str
    description: str

    # creator: List[User] =[]
    class Config():
        orm_mode = True


class ShowProduct(BaseModel):
    id: str
    # product_pic: str
    product_name: str
    # product_pic:str
    category: ShowProductCateg
    estimated_price: Optional[str]
    description:str
    created_at: date
    updated_at: date
    status:str


    class Config():
        orm_mode = True


class ProductStatus(BaseModel):
    status: str