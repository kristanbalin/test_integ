from pydantic import BaseModel
from typing import Optional, Text, List
from datetime import date




from ..human_capital_management import user
# from . import vendor






# Vendor
class VendorEvaluationResult(BaseModel):
    message: str
    purchase_order_id: str
    cost: int
    timeliness: int
    reliability: int
    quality: int
    availability: int
    reputation: int



    # purchase_requisition_id: int


class ShowVendorEvaluationResults(BaseModel):
    id:str
    message: Optional[str]
    purchase_order_id: str
    cost: int
    timeliness: int
    reliability: int
    quality: int
    availability: int
    reputation: int

    # vendor:vendor.ShowVendor
    u_created_by: Optional[user.ShowUser]

    # vendor: vendor.ShowVendor
    class Config():
        orm_mode = True



class ShowVendorEvaluationResultsVendor(BaseModel):
    id:str
    date:date
    message: str
    product_quality: str
    on_time_delivery: str
 
    # vendor_id:str


    # vendor: vendor.ShowVendor
    class Config():
        orm_mode = True




        
