from pydantic import BaseModel
from typing import Optional, Text, List



# PaymentTerms
class PaymentTerms(BaseModel):
    method_name: str
    description: str
    class Config():
        orm_mode = True

class ShowPaymentTerms(BaseModel):
    id: str
    method_name: str
    description: str
    # creator: List[User] =[]
    # budget_plan: List[ShowBudgetPlan]
    class Config():
        orm_mode = True