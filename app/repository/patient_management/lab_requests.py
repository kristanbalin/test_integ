from fastapi.encoders import jsonable_encoder
from AR_AP.schemas.lab_requests import CreateLabRequest, UpdateLabRequest
from datetime import datetime

from sqlalchemy.orm.session import Session
from .. import models
from fastapi import HTTPException, status
from uuid import uuid4
import random



def datatable(db: Session):
    lab_requests = db.query(models.LabRequest).all()
    return lab_requests

def find_all(db: Session):
    lab_requests = db.query(models.LabRequest).filter(models.LabRequest.status != "Completed").all()
    return lab_requests


def find_one(id, db: Session):
    lab_requests = db.query(models.LabRequest).filter(models.LabRequest.id == id).first()
    if not lab_requests:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Lab Request is not available.")
    return lab_requests

def find_by_lab_request_no(lab_request_no, db: Session):
    lab_requests = db.query(models.LabRequest).filter(models.LabRequest.lab_request_no == lab_request_no).first()
    if not lab_requests:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Lab Request is not available.")
    return lab_requests


def create(request: CreateLabRequest, db: Session):

    d = datetime.now()
    curr_date = d.strftime("%Y%m%d")
    new_uuid = str(uuid4())
    last_4_uuid = str(new_uuid[-4:])
    new_lab_requests = models.LabRequest(**request.dict(),
        id=str(uuid4()),
        lab_request_no="LAB"+ last_4_uuid + "-" + curr_date
        )
                            
    db.add(new_lab_requests)
    db.commit()
    db.refresh(new_lab_requests)
    return "Lab Request has been created."


def update(id, request: UpdateLabRequest, db: Session):
    lab_requests = db.query(models.LabRequest).filter(models.LabRequest.id == id)
    lab_requests_same_name = db.query(models.LabRequest).filter(models.LabRequest.id != id)

    for row in lab_requests_same_name:
        if row.lab_request_no == request.lab_request_no:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Lab Request already exists.")

    if not lab_requests.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Lab Request is not available.")

    
    lab_requests_json = jsonable_encoder(request)     
    lab_requests.update(lab_requests_json)
                            
    db.commit()
    return f"Lab Request has been updated."


def completed(id, updated_by:str, db: Session):
    lab_requests = db.query(models.LabRequest).filter(models.LabRequest.id == id)
    if not lab_requests.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Lab Request is not available.")
    lab_requests.update({
                    'status': 'Completed',
                    'updated_at': datetime.now(),
                     })
    db.commit()
    return f"LabRequest has been deleted."