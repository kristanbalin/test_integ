from statistics import mode
from fastapi.encoders import jsonable_encoder
from AR_AP.schemas.treatments import CreateTreatment, UpdateTreatment
from datetime import datetime

from sqlalchemy.orm.session import Session
from .. import models
from fastapi import HTTPException, status
from uuid import uuid4
import random
from sqlalchemy import false, null, or_, and_



def datatable(db: Session):
    treatments = db.query(models.Treatment).all()
    return treatments

def find_all_for_billing(id, db: Session):
    latest_admission_id = db.query(models.Inpatient).\
        select_from(models.Inpatient).\
        join(models.PatientRegistration).\
        filter(models.Inpatient.patient_id == id).\
        order_by(models.Inpatient.date_admitted.desc()).first()

    check_discharge_date = db.query(models.DischargeManagement).\
        select_from(models.DischargeManagement).\
        join(models.Inpatient).\
        filter(and_(models.Inpatient.patient_id == id, models.DischargeManagement.patient_id == id)).\
        order_by(models.Inpatient.date_admitted.desc()).first()

# Treatment DATE must be greater than or equal to DATE ADMITTED
# Treatment STATUS must be FOR BILLING
# Treatment DATE must be less than or equal to DATE DISCHARGE 
# OR Treatment DATE must be less than or equal to CURRENT DATETIME
    treatment_with_discharge = db.query(models.Treatment).\
        select_from(models.Inpatient).\
        join(models.PatientRegistration).\
        join(models.Treatment).\
        join(models.DischargeManagement).\
        filter(models.Treatment.patient_id == id).\
        filter(and_(models.DischargeManagement.patient_id == id,
                    models.Treatment.session_datetime >= latest_admission_id.date_admitted,\
                    models.Treatment.session_datetime <= models.DischargeManagement.discharge_date,\
                    models.Treatment.session_datetime <= datetime.now(),
                    models.Treatment.status == "FOR BILLING",)).\
        order_by(models.Treatment.session_datetime.asc()).all()

    treatment_current_time = db.query(models.Treatment).\
        select_from(models.Inpatient).\
        join(models.PatientRegistration).\
        join(models.Treatment).\
        filter(models.Treatment.patient_id == id).\
        filter(and_(models.Treatment.session_datetime >= latest_admission_id.date_admitted,\
                    models.Treatment.status == "FOR BILLING",
                    models.Treatment.session_datetime <= datetime.now() )).\
                    order_by(models.Treatment.session_datetime.asc()).all()

    if not check_discharge_date:
        return treatment_current_time  
    else: 
        if not treatment_with_discharge:  
            print("Has treatment but does not have discharge date")
        else:
            print("Both has treatment and discharge date!")
            return treatment_with_discharge
           
        
   




       
        # if not latest_admission_id.first():
        #     raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
        #     detail=f'Inpatient bill with the is not found')
                    # filter(models.Treatment.session_datetime >= models.Inpatient.date_admitted).\
                    # order_by(models.Inpatient.date_admitted.asc()).all()
    # treatment = db.query(models.Treatment).\
    #     join(models.PatientRegistration).\
    #     join(admission_list).\
    #     filter(models.Treatment.patient_id ==admission_list.patient_id).\
    #     filter(models.Treatment.patient_id == id).\
    #     order_by(models.Inpatient.date_admitted.asc()).all()
        
    # treatments = db.query(models.Treatment).\
    #     filter(models.Treatment.patient_id == id).\
    #     filter(models.Treatment.session_datetime <= date).\
    #     filter(models.Treatment.status == "FOR BILLING").all()

    # if admission_list:
    #     raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
    #                         detail="Treatment is not available.")
 
  

def find_one(id, db: Session):
    treatments = db.query(models.Treatment).filter(models.Treatment.id == id).first()
    if not treatments:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Treatment is not available.")
    return treatments

def find_by_treatment_no(treatment_no, db: Session):
    treatments = db.query(models.Treatment).filter(models.Treatment.treatment_no == treatment_no).first()
    if not treatments:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Treatment is not available.")
    return treatments


def create(request: CreateTreatment, db: Session):
    new_uuid = str(uuid4())
    last_4_uuid = str(new_uuid[-4:])
    new_treatments = models.Treatment(**request.dict(),
        id=str(uuid4()),
        treatment_no="TRTMNT"+ last_4_uuid + "-" + str(random.randint(1111, 9999)),
        )
        
    treatment_no_var="TRTMNT"+ last_4_uuid + "-" + str(random.randint(1111, 9999))
    if db.query(models.Treatment).filter_by(treatment_no= treatment_no_var).count() == 1:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Treatment already exists.")
                            
    db.add(new_treatments)
    db.commit()
    db.refresh(new_treatments)
    return "Treatment has been created."


def update(id, request: UpdateTreatment, db: Session):
    treatments = db.query(models.Treatment).filter(models.Treatment.id == id)
    treatments_same_name = db.query(models.Treatment).filter(models.Treatment.id != id)

    for row in treatments_same_name:
        if row.treatment_no == request.treatment_no:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Treatment already exists.")

    if not treatments.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Treatment is not available.")

    
    treatments_json = jsonable_encoder(request)     
    treatments.update(treatments_json)
                            
    db.commit()
    return f"Treatment has been updated."



def completed(id, updated_by:str, db: Session):
    treatments = db.query(models.Treatment).filter(models.Treatment.id == id)
    if not treatments.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Treatment is not available.")
    treatments.update({
                    'status': 'Inactive',
                    'updated_at': datetime.now(),
                     })
    db.commit()
    return f"Treatment has been deleted."
    