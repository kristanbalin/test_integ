from ntpath import join
import re
from fastapi.encoders import jsonable_encoder
from AR_AP.schemas.room_bill import CreateRoomBill, UpdateRoomBill
from datetime import datetime

from sqlalchemy.orm.session import Session
from .. import models
from fastapi import HTTPException, status
from uuid import uuid4
import random
from sqlalchemy import or_




def datatable(db: Session):
    room_bill = db.query(models.RoomBill).all()
    return room_bill

def find_all(db: Session):
    room_bill = db.query(models.RoomBill).filter(or_(models.RoomBill.status != "Inactive",models.RoomBill.status != "INACTIVE" )).all() 
    return room_bill


def find_one(id, db: Session):
    room_bill = db.query(models.RoomBill).filter(models.RoomBill.id == id).first()
    if not room_bill:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Room Bill is not available.")
    return room_bill

def find_by_invoice_no(invoice_no, db: Session):
    room_bill = db.query(models.RoomBill).filter(models.RoomBill.invoice_no == invoice_no).first()
    if not room_bill:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Room Bill is not available.")
  
    return room_bill

    
def create(request: CreateRoomBill, db: Session):
 
    tmp_room= db.query(models.Inpatient,models.Room,models.Room_type).\
        select_from(models.Inpatient).\
        join(models.Room).\
        join(models.Room_type).\
        filter(models.Inpatient.admission_id == request.inpatient_id).\
        first()
     
    tmp_rm_transfer= db.query(models.Inpatient,models.RoomTransfer,models.Room, models.Room_type).\
        select_from(models.Inpatient).\
        join(models.RoomTransfer).\
        join(models.Room).\
        join(models.Room_type).\
        filter(models.Inpatient.admission_id == request.inpatient_id).first()
    # print(tmp_room.Room_type.amount)
    total_amount = tmp_room.Room_type.amount + tmp_rm_transfer.Room_type.amount
    # print(f"tmp: {tmp} total_amount:{total_amount}")
    
    d = datetime.now()
    curr_date = d.strftime("%Y%m%d")
    new_uuid = str(uuid4())
    last_5_uuid = str(new_uuid[-5:])
    new_room_bill = models.RoomBill(**request.dict(),
        id=str(uuid4()),
        invoice_no="RMBLL"+ last_5_uuid + curr_date,
        total_amount=total_amount
        )
        
    if db.query(models.RoomBill).filter_by(inpatient_id= request.inpatient_id).count() == 1:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Room Bill invoice already exists.")
                            
    db.add(new_room_bill)
    db.commit()
    db.refresh(new_room_bill)
    return "Room Bill invoice has been created."
 

def update(id, request: UpdateRoomBill, db: Session):
    room_bill = db.query(models.RoomBill).filter(models.RoomBill.id == id)
    invoice_no_same_name = db.query(models.RoomBill).filter(models.RoomBill.id != id)

    for row in invoice_no_same_name:
        if row.invoice_no == request.invoice_no:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Room Bill already exists.")

    if not room_bill.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Room Bill is not available.")

    
    room_bill_json = jsonable_encoder(request)     
    room_bill.update(room_bill_json)
                            
    db.commit()
    return f"Room Bill has been updated."


def completed(id, updated_by:str, db: Session):
    room_bill = db.query(models.RoomBill).filter(models.RoomBill.id == id)
    if not room_bill.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Room Bill is not available.")
    room_bill.update({
                    'status': 'Completed',
                    'updated_at': datetime.now(),
                    'updated_by': updated_by})
    db.commit()
    return f"Room Bill has been completed."
