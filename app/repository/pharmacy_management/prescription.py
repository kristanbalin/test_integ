from datetime import date, datetime, timedelta
from AR_AP.schemas.prescription import CreatePrescription,Prescription
from sqlalchemy.orm.session import Session
from ... import models
from fastapi import HTTPException, status
from uuid import uuid4



def datatable(db: Session):
    prescription = db.query(models.Prescription).all()
    #users = db.query(models.User, models.Employee).join(models.User).join(models.Employee)
    return prescription

def find_one(id, db:Session):
    prescription = db.query(models.Prescription).filter(models.Prescription.prescription_id == id).first()
    if not prescription:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Prescription with the id {id} is not available.")
    return prescription

def create(request: CreatePrescription , db: Session):

    prescription = db.query(models.Prescription).order_by(models.Prescription.created_at.desc()).first()
    
    if not prescription:
        counter = 1001
    else:
        prescription = prescription.prescription_no
        curent_prescription_no = prescription.split("-")
        counter = int(curent_prescription_no[1])
        counter = counter + 1

    new_prescription = models.Prescription(
    prescription_id = str(uuid4()),
    
    admission_id= request.admission_id,
    prescription_no = "PR-" + str(counter),
    created_by=request.created_by,
    created_at=datetime.now(),
    date_prescribed= datetime.now()
   
   
   
    )
    db.add(new_prescription)
    db.commit()
    db.refresh(new_prescription)
    return  new_prescription

