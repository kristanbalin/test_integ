from fastapi.encoders import jsonable_encoder
from AR_AP.schemas.medicine_pr import CreateMedicine_PR, UpdateMedicine_PR
from datetime import datetime

from sqlalchemy.orm.session import Session
from .. import models
from fastapi import HTTPException, status
from uuid import uuid4
import random



def datatable(db: Session):
    medicine_pr = db.query(models.Medicine_PR).all()
    return medicine_pr

def find_all(db: Session):
    medicine_pr = db.query(models.Medicine_PR).filter(models.Medicine_PR.status != "Completed").all()
    return medicine_pr


def find_one(id, db: Session):
    medicine_pr = db.query(models.Medicine_PR).filter(models.Medicine_PR.id == id).first()
    if not medicine_pr:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Medicine prescription is not available.")
    return medicine_pr

def find_by_lab_request_no(medicine_no, db: Session):
    medicine_pr = db.query(models.Medicine_PR).filter(models.Medicine_PR.medicine_no == medicine_no).first()
    if not medicine_pr:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Medicine prescription is not available.")
    return medicine_pr


def create(request: CreateMedicine_PR, db: Session):
    d = datetime.now()
    curr_date = d.strftime("%Y%m%d")

    new_uuid = str(uuid4())
    last_4_uuid = str(new_uuid[-4:])
    new_medicine_pr = models.Medicine_PR(**request.dict(),
        medpr_id=str(uuid4()),
        medicine_no= str(random.randint(1111, 9999)) )
                            
    db.add(new_medicine_pr)
    db.commit()
    db.refresh(new_medicine_pr)
    return "Medicine prescription has been created."


def update(id, request: UpdateMedicine_PR, db: Session):
    medicine_pr = db.query(models.Medicine_PR).filter(models.Medicine_PR.medpr_id == id)
    medicine_pr_same_name = db.query(models.Medicine_PR).filter(models.Medicine_PR.medpr_id != id)

    for row in medicine_pr_same_name:
        if row.medicine_no == request.medicine_no:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f"Medicine prescription already exists.")

    if not medicine_pr.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Medicine prescription is not available.")

    
    medicine_pr_json = jsonable_encoder(request)     
    medicine_pr.update(medicine_pr_json)
                            
    db.commit()
    return f"Medicine prescription has been updated."


def completed(id, updated_by:str, db: Session):
    medicine_pr = db.query(models.Medicine_PR).filter(models.Medicine_PR.id == id)
    if not medicine_pr.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Medicine prescription is not available.")
    medicine_pr.update({
                    'status': 'Completed',
                    'updated_at': datetime.now(),
                    'updated_by': updated_by
                    })
    db.commit()
    return f"Medicine_PR has been deleted."