from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import BLOB, DATE, DATETIME, DECIMAL, TEXT, Float,CHAR
from ...database import Base
import uuid

class ReturnDetail(Base):
    __tablename__ = "return_details"

    id = Column(CHAR(36), primary_key=True, default=uuid.uuid4)
    quantity = Column(Integer, nullable=False)
    status = Column(String(255), nullable=False,default="Active")
    purchase_order_detail_id = Column(CHAR(36), ForeignKey("purchase_order_detail.id"), nullable=True)
    return_id = Column(CHAR(36), ForeignKey("returns.id"), nullable=True)
    created_at = Column(DATETIME, default=func.current_timestamp())
    updated_at = Column(DATETIME,
                    default=func.current_timestamp(),
                    onupdate=func.current_timestamp())

    # relation with purchase order detail
    purchase_order_detail = relationship("PurchaseOrderDetail", back_populates="return_details")
  
    # relation with returns
    returns = relationship("Return", back_populates="return_details")
    