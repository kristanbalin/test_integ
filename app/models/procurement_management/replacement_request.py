from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import BLOB, DATE, DATETIME, DECIMAL, TEXT, Float,CHAR
from ...database import Base
import uuid

class ReplacementRequest(Base):
    __tablename__ = "replacement_request"

    id = Column(CHAR(36), primary_key=True, default=uuid.uuid4)
    message = Column(TEXT, nullable=False)
    replacement_request_date = Column(DATETIME, nullable=False)
    request_type = Column(String(255), nullable=False)
    prepared_by = Column(String(255), nullable=False)
    status = Column(String(255), nullable=False,default="Pending")

    expected_arrival_date = Column(DATETIME, nullable=True)
    confirmed_by = Column(String(255), nullable=True)#vendor
    reason = Column(String(255), nullable=True)# if rejected

    return_id = Column(CHAR(36), ForeignKey("returns.id"), nullable=True)
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)  
    created_at = Column(DATETIME, default=func.current_timestamp())
    updated_at = Column(DATETIME,
                    default=func.current_timestamp(),
                    onupdate=func.current_timestamp())

  
    # relation with returns
    returns = relationship("Return", back_populates="replacement_request")

    # relation with replacement items
    # replacement_items = relationship("ReplacementItems", back_populates="replacement_request")

    # relation with user
    u_created_by = relationship("User",foreign_keys=[created_by])
    u_updated_by = relationship("User",foreign_keys=[updated_by])
    