from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import BLOB, DATE, DATETIME, DECIMAL, TEXT, Float,CHAR
from ...database import Base
import uuid

class User(Base):
    __tablename__ = "users"

    id = Column(CHAR(36), primary_key=True, default=uuid.uuid4)
    email = Column(String(255), nullable=False, unique=True)
    password = Column(String(255), nullable=False)
    status = Column(String(255), nullable=False,default="Active")
    user_type_id = Column(CHAR(36), ForeignKey("user_types.id"), nullable=True)
    vendor_id = Column(CHAR(36), ForeignKey("vendor.id"), nullable=True)
    employee_id = Column(CHAR(36), ForeignKey("employees.id"), nullable=True)  
    created_at = Column(DATETIME, default=func.current_timestamp())
    updated_at = Column(DATETIME,
                    default=func.current_timestamp(),
                    onupdate=func.current_timestamp())

    # relation with user type
    user_types = relationship("UserType", back_populates="users")
  
    # relation with vendor
    vendor = relationship("Vendor", back_populates="users")

    # relation with employee
    employees = relationship("Employee", primaryjoin="and_(User.employee_id==Employee.id)", back_populates="users")



