from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import BLOB, DATE, DATETIME, DECIMAL, TEXT, Float,CHAR
from ...database import Base
import uuid

class UserType(Base):
    __tablename__ = "user_types"

    id = Column(CHAR(36), primary_key=True, default=uuid.uuid4)
    name = Column(String(255), nullable=False)
    description = Column(TEXT, nullable=True)
    # created_by = Column(String(255), nullable=True)
    # updated_by = Column(String(255), nullable=True)
    status = Column(String(255), nullable=False,default="Active")
    created_at = Column(DATETIME, nullable=True,default=func.current_timestamp())
    updated_at = Column(DATETIME,nullable=True,
                    default=func.current_timestamp(),
                    onupdate=func.current_timestamp())

    # relation with user
    users = relationship("User", back_populates="user_types")