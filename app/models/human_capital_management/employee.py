from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import CHAR,TEXT, Date, DateTime, Float, Text,Integer,Time, Numeric
from ...database import Base
import uuid
from sqlalchemy.sql.schema import UniqueConstraint



class Employee(Base):
    __tablename__ = "employees"

    id = Column(CHAR(36), primary_key=True, index=True)
    photo = Column(String(255), nullable=False)
    first_name = Column(String(255), nullable=False)
    middle_name = Column(String(255), nullable=True)
    last_name = Column(String(255), nullable=False)
    extension_name = Column(String(255), nullable=True)
    birthdate = Column(Date, nullable=False)
    birthplace = Column(String(255), nullable=False)
    gender = Column(String(255), nullable=False)
    civil_status = Column(String(255), nullable=False)
    house_number = Column(String(255), nullable=True)
    street = Column(String(255), nullable=True)
    barangay = Column(String(255), nullable=True)
    city = Column(String(255), nullable=False)
    province = Column(String(255), nullable=False)
    country = Column(String(255), nullable=False)
    contact_number = Column(String(255), nullable=False,
                            unique=True, index=True)
    email = Column(String(255), nullable=False,
                            unique=True, index=True)
    job = Column(String(255), nullable=False)
    hire_date = Column(Date, nullable=False)
    manager = Column(String(255), nullable=False)
    department_id = Column(CHAR(36), ForeignKey("department.id"), nullable=True)
    employee_type_id = Column(CHAR(36), ForeignKey("employee_types.id"), nullable=True)

    status = Column(String(255), nullable=False, server_default="Active")
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    created_at = Column(DateTime, nullable=False)
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_at = Column(DateTime, nullable=True)

    UniqueConstraint(first_name, middle_name, last_name, extension_name)

    # relation with user
    users = relationship("User", primaryjoin="and_(Employee.id==User.employee_id)",back_populates="employees")
   
     # relation with department
    department = relationship("Department", back_populates="employees")

    # relation with employee types
    employee_types = relationship("EmployeeType", back_populates="employees")

