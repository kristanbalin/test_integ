from datetime import datetime
from tokenize import Number
from sqlalchemy import Boolean, Column, ForeignKey, String, null, text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import UniqueConstraint
from sqlalchemy.sql.sqltypes import CHAR,TEXT, Date, DateTime, Float, Text,Integer,Time, Numeric

from ...database import Base

# # 
# class Employee(Base):
#     __tablename__ = "employees"

#     id = Column(CHAR(36), primary_key=True, index=True)
#     photo = Column(String(255), nullable=False)
#     first_name = Column(String(255), nullable=False)
#     middle_name = Column(String(255), nullable=True)
#     last_name = Column(String(255), nullable=False)
#     extension_name = Column(String(255), nullable=True)
#     birthdate = Column(Date, nullable=False)
#     birthplace = Column(String(255), nullable=False)
#     gender = Column(String(255), nullable=False)
#     civil_status = Column(String(255), nullable=False)
#     house_number = Column(String(255), nullable=True)
#     street = Column(String(255), nullable=True)
#     barangay = Column(String(255), nullable=True)
#     city = Column(String(255), nullable=False)
#     province = Column(String(255), nullable=False)
#     country = Column(String(255), nullable=False)
#     contact_number = Column(String(255), nullable=False,
#                             unique=True, index=True)
#     email = Column(String(255), nullable=False,
#                             unique=True, index=True)
#     department = Column(String(255), nullable=False)
#     job = Column(String(255), nullable=False)
#     hire_date = Column(Date, nullable=False)
#     manager = Column(String(255), nullable=False)
#     status = Column(String(255), nullable=False, server_default="Active")
#     created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
#     created_at = Column(DateTime, nullable=False)
#     updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
#     updated_at = Column(DateTime, nullable=True)

#     UniqueConstraint(first_name, middle_name, last_name, extension_name)

#     user_credentials = relationship("User", primaryjoin="and_(Employee.id==User.employee_id)",back_populates="employee_information")

# class User(Base):
#     __tablename__ = "users"
#     id = Column(CHAR(36), primary_key=True, index=True)
#     employee_id = Column(CHAR(36), ForeignKey("employees.id"), nullable=False)
#     username = Column(String(255), nullable=False, unique=True, index=True)
#     password = Column(String(255), nullable=False)
#     user_type = Column(String(255), nullable=False)
#     status = Column(String(255), nullable=False, server_default="Active")
#     created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
#     created_at = Column(DateTime, nullable=False)
#     updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
#     updated_at = Column(DateTime, nullable=True)

#     employee_information = relationship("Employee", primaryjoin="and_(User.employee_id==Employee.id)", back_populates="user_credentials")

    

#===================================================================================================================#
#PATIENT MANAGEMENT
class PatientRegistration(Base):
    __tablename__ = 'patient_registration'

    patient_id = Column(String(36), primary_key=True, default=text('UUID()'))
    first_name = Column(String(255), nullable=False)
    middle_name = Column(String(255), nullable=False)      #NEW1 NULLABLE DAPAT TO
    last_name = Column(String(255), nullable=False)
    sex = Column(String(255), nullable=False)
    birthday = Column(Date, nullable=False)
    weight = Column(String(255), nullable=False)
    height = Column(String(255), nullable=False)
    blood_type = Column(String(255), nullable=False)
    guardian = Column(String(255), nullable=False)
    address = Column(String(255), nullable=False)
    contact_number = Column(String(255), nullable=False)
    medical_history_number = Column(String(36),  nullable=True)  # ForeignKey('medical_history.medical_history_number'),
    dp_id = Column(String(36), ForeignKey('discount_privillages.dp_id'), nullable=True)
    insurance_id = Column(String(36), ForeignKey("insurances.insurance_id"), nullable=True)  ####### NEW COLUMN #########
    patient_type = Column(String(255), nullable=True)
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    #medical_history = relationship('MedicalHistory')     ### COMMENT KO MUNA ITO

    discount_privillages = relationship(
         "Discount", primaryjoin="and_(PatientRegistration.dp_id==Discount.dp_id)") #CHANGE TO DISCOUNT /dp_id/ PatientRegistration


    treatments = relationship("Treatment",back_populates="patient")  #NEW1
    surgeries = relationship("Surgery",back_populates="patient")  #NEW1



    #===========================================================================================================#
    # insurance_id_info = relationship(
    #         "Insurance", primaryjoin="and_(Patient_registration.insurance_id==Insurance.insurance_id)")

    # patient_admission = relationship(
    #     "Inpatient_management", primaryjoin="and_(Patient_registration.patient_id==Inpatient_management.patient_id)", back_populates="patient_info")
   
    # inpatient_credentials = relationship(
    #     "Inpatient_management", primaryjoin="and_(Patient_registration.patient_id == Inpatient_management.patient_id)",back_populates="patient_info")
    
    # inpatient_discharged = relationship(
    #     "Discharge_management", primaryjoin="and_(Patient_registration.patient_id == Discharge_management.patient_id)",back_populates="inpatient_credentials")

    # inpatient_surgeries = relationship(
    #     "Surgery", primaryjoin="and_(Patient_registration.patient_id == Surgery.patient_id)",back_populates="patient_info")

    # inpatient_treatments = relationship(
    #     "Treatment", primaryjoin="and_(Patient_registration.patient_id == Treatment.patient_id)",back_populates="patient_info")

    # inpatient_hospital_charges = relationship(
    #     "HospitalCharges", primaryjoin="and_(Patient_registration.patient_id == HospitalCharges.patient_id)",back_populates="patient_info")
#PATIENT MANAGEMENT  
class Insurance(Base):   # wala dati (Base)
    __tablename__ = 'insurances'
    insurance_id = Column(String(36), primary_key=True) #, default=mydefault
    # patient_id = Column(String(255), ForeignKey('patient_registration.patient_id'), nullable=True)
    policy_holder = Column(String(255), nullable=True)
    policy_number = Column(String(255), nullable=True)
    company_phone = Column(String(255), nullable=True)
    company_address = Column(String(255), nullable=True)
    remarks = Column(String(255), nullable=True)
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))
    #====================================================================================================

#PATIENT MANAGEMENT
class Discount(Base):
    __tablename__ = 'discount_privillages'

    dp_id = Column(String(36), primary_key=True, default=text('UUID()'))
    ph_id = Column(String(255), nullable=True)                                  ###  PHILHEALTH PO BA ITO?
    end_of_validity = Column(String(255), nullable=True)   #NEW1 BAKIT HINDI DATE?
    sc_id = Column(String(255), nullable=True)                                  ###  SENIOR CITIZEN ID?
    municipality = Column(String(255), nullable=True)
    pwd_id = Column(String(255), nullable=True)
    type_of_disability = Column(String(255), nullable=True)
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))
    #====================================================================================================

#CMS
class Room_type(Base):
    __tablename__ = "room_types"    
    id = Column(CHAR(36), primary_key=True,index=True)
    room_type_name = Column(String(255),nullable=False,unique=True, index=True)
    description = Column(Text,nullable=False)
    amount = Column(Float,nullable=False)
    status = Column(String(255), nullable=False, server_default="Active")
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime,default=text('NOW()'),nullable=False) #NEW1 default=text('NOW()')
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)
    #====================================================================================================


#PATIENT MANAGEMENT
class Room(Base):
    __tablename__ = 'rooms' 
    room_id = Column(String(36), primary_key=True, default=text('UUID()'))
    room_number = Column(String(255), nullable=False)
    date_admitted = Column(DateTime, default=text('NOW()'))
    admission_id =  Column(String(255), ForeignKey('inpatients.admission_id'), nullable=False)
    room_type_id= Column(String(36), ForeignKey("room_types.id"), nullable=False)  ###     room_type = Column(String(36),  ForeignKey("room_types.id"), nullable=False)
    location = Column(String(36), nullable=True)
    room_count = Column(Integer, nullable=True)
    room_status = Column(String(36), nullable=False)
    active_status = Column(String(36), nullable=False)
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    room_type_info = relationship(
        "Room_type", primaryjoin="and_(Room.room_type_id==Room_type.id)") #NEW1
    # rooms_inpatientFk = relationship('Inpatient')
    room_inpatientFk = relationship('Inpatient', foreign_keys=[admission_id])

    #====================================================================================================

#PATIENT MANAGEMENT
class Inpatient(Base):
    __tablename__ = 'inpatients'

    admission_id = Column(String(255), primary_key=True, default=text('UUID()'))
    inpatient_no = Column(String(255), nullable=False)                                        ### NEW COLUMN DINAGDAG TUDEY ####
    patient_id = Column(String(36), ForeignKey('patient_registration.patient_id'), nullable=True)
    # room_number = Column(String(36), ForeignKey('patient_rooms.id'), nullable=False)
   
    date_admitted = Column(DateTime, default=text('NOW()'),nullable=False)  #nullable=False
    reason_of_admittance = Column(String(255), nullable=True)
    department = Column(String(255), nullable=True)
    diagnosis = Column(String(255), nullable=True)
    tests = Column(String(255), nullable=True)
    treatments = Column(String(255), nullable=True)
    surgery = Column(String(255), nullable=True)

    status = Column(String(255),nullable=True, default="Active")#NEW 2 NEW COLUMN
    is_accepting_visits = Column(String(255), nullable=True)#NEW 2 NEW COLUMN
 
    patient_status = Column(String(255), nullable=True)  #Discharged
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    admission_info = relationship(
        "Room", primaryjoin="and_(Inpatient.admission_id==Room.admission_id)")

    # room = relationship('Room', foreign_keys=[room_number])#NEW1 COMMENT KO MUNA

    inpatientsFk = relationship('PatientRegistration', foreign_keys=[patient_id])

    my_prescriptions = relationship("Prescription", primaryjoin="and_(Inpatient.admission_id==Prescription.admission_id)",back_populates="inpatient_info")           #NEW1 GALING SA PHARMACY

    patient_info = relationship(
        "PatientRegistration", primaryjoin="and_(Inpatient.patient_id==PatientRegistration.patient_id)")  #NEW1 PatientRegistration

    inpatient_bill_info = relationship(
        "InpatientBill", primaryjoin="and_(Inpatient.admission_id ==InpatientBill.admission_id)")

    discharge_management_info = relationship('DischargeManagement', back_populates='inpatient_info')  #NEW1

    admission_info = relationship(
        "Room", primaryjoin="and_(Inpatient.admission_id==Room.admission_id)")

    
    #====================================================================================================


# NEW TABLE FOR PATIENT MANAGEMENT //WALA NA
# class   PatientRoom(Base):
#     __tablename__ = "patient_rooms"

#     id = Column(String(36), primary_key=True,index=True)
#     room_number = Column(String(36), ForeignKey("rooms.room_id"),nullable=False) #NEW1 ,ForeignKey("rooms.id")
#     admission_id =  Column(String(255), ForeignKey('inpatients.admission_id'), nullable=False)
#     datetime_admitted =  Column(DateTime (timezone=True), nullable=False, server_default=func.now())
#     status = Column(String(255), nullable=False, server_default="Occupied")
#     created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=True)
#     created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
#     updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
#     updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())
#     room_number_info = relationship(
#         "Room", primaryjoin="and_(PatientRoom.room_number==Room.room_id)") 

    #====================================================================================================

#PATIENT MANAGEMENT                                              
class DischargeManagement(Base):
    __tablename__ = 'discharge_management'                                                         

    discharge_id = Column(String(255), primary_key=True, default=text('UUID()'))                   ## NEED PO NAMIN
    discharge_no = Column(String(255), nullable=False)                                             ### NEW COLUMN DINAGDAG TUDEY ####
    patient_id = Column(String(255), nullable=True) ##NEW1 , ForeignKey('patient_registration.patient_id') DELETED
    admission_id = Column(String(255), ForeignKey('inpatients.admission_id'), nullable=True)       ## NEED PO NAMIN
    reason_of_admittance = Column(String(255), nullable=False)
    diagnosis_at_admittance = Column(String(255), nullable=False)
    date_admitted = Column(DateTime, default=text('NOW()'))              #pwede po ito makuha sa admission_id po ata
    treatment_summary = Column(String(255), nullable=False)
    discharge_date = Column(DateTime, nullable=True)     #NEW1 CHANGE TO DateTime                                      ## NEED PO NAMIN
    physician_approved = Column(String(255), nullable=False)
    discharge_diagnosis = Column(String(255), nullable=False)
    further_treatment_plan = Column(String(255), nullable=False)
    next_check_up_date = Column(Date, nullable=True)
    client_consent_approval = Column(String(255), nullable=False)
    # medication = Column(String(255), nullable=True)
    # dosage = Column(String(255), nullable=True)
    # frequency = Column(String(255), nullable=True)
    # ending_date = Column(Date, nullable=True)
    active_status = Column(String(255), nullable=True)
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    # discharge_managementFk = relationship('PatientRegistration', foreign_keys=[patient_id])
    discharge_inpatientFk = relationship('Inpatient', foreign_keys=[admission_id])
    inpatient_info = relationship('Inpatient', back_populates='discharge_management_info')  #NEW1
    #====================================================================================================



################################################################################ TREATMENT MANAGEMENT

#GALING DOCTORS MANAGEMENT
class Specialization(Base):
    __tablename__= "specialization"
    specialization_id = Column(CHAR(36), primary_key=True) #, default= uuid4
    specialization_name = Column(String(255), nullable= False)
    status =  Column(String(255), default="Active")
    created_by= Column(CHAR(36), ForeignKey('users.id'), nullable= True)
    created_at =  Column(DateTime(timezone=True), server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey('users.id'), nullable= True)
    updated_at =  Column(DateTime(timezone=True), server_default=func.now())

#GALING DOCTORS MANAGEMENT
class Doctor_profile(Base):
    __tablename__ = "doctor_profile"
    doctor_id = Column(CHAR(36),  primary_key=True) #, default= uuid4
    photo = Column(String(255), nullable=True)
    label = Column(String(5), nullable= False, default="Dr.")
    doctor_first_name  = Column(String(255), nullable= False) #NEW1 NULLABLE FALSE
    doctor_middle_name  = Column(String(255), nullable= True)
    doctor_last_name = Column(String(255), nullable= False) #NEW1 NULLABLE FALSE
    doctor_home_address = Column(String(255), nullable= True)
    doctor_location = Column(String(255), nullable= True)
    doctor_mobile = Column(String(255), nullable= True)
    doctor_schedule =  Column(String(255), nullable=True)
    specialization_id  = Column(CHAR(36), ForeignKey('specialization.specialization_id'))
    status =  Column(String(255), default="Active")
    created_by= Column(CHAR(36), ForeignKey('users.id'), nullable= True)           ##User
    created_at =  Column(DateTime(timezone=True), server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey('users.id'), nullable= True)          ##User
    updated_at =  Column(DateTime(timezone=True), server_default=func.now())
    
    # treatments = relationship("Treatment",back_populates="patient")  #NEW1
    treatments = relationship('Treatment', back_populates='physician')  #NEW1
    handled_surgeries = relationship('SurgeryInCharge', back_populates="in_charge")
    #doctor_specialization = relationship("Specialization", backref="backref"("doctor_profile", uselist=False)) ###"backref" ###


#  GALING CMS ITU
class Treatment_type(Base):
    __tablename__ = "treatment_types"
    id = Column(CHAR(36), primary_key=True,index=True)
    treatment_type_name = Column(String(255),nullable=False,unique=True, index=True)
    description = Column(Text,nullable=False)

    status = Column(String(255), nullable=False, server_default="Active")
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime,server_default=func.now(), nullable=False) #NEW1 server_default=func.now()
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,server_default=func.now(), nullable=True)  #NEW1 server_default=func.now()


## NEW TABLE FOR TREATMENT
class TreatmentServiceName(Base):
    __tablename__ = "treatment_service_name"
    id = Column(String(36), primary_key=True,index=True)
    treatment_service_name = Column(String(255),nullable=False,unique=True)
    treatment_types_id = Column(String(36), ForeignKey("treatment_types.id"),nullable=False)
    unit_price = Column(Float, nullable=False)
  
    status = Column(String(100), default='Pending' ,nullable=False)        #nullable=False                         
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime, nullable=False, server_default=func.now()) #NEW1 server_default=func.now()
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime, nullable=True,server_default=func.now()) #NEW1 server_default=func.now()

    treatments = relationship("Treatment",back_populates="treatment_name")  #NEW1 ALL
    
    treatment_type_info =relationship(
        "Treatment_type", primaryjoin="and_(TreatmentServiceName.treatment_types_id==Treatment_type.id)")


# TREATMENT MANAGEMENT
class Treatment(Base):
    __tablename__ = "treatments"
    id = Column(String(36), primary_key=True, default=text('UUID()'))
    treatment_no = Column(String(100),nullable=False) #nullable=False
    patient_id = Column(String(36), ForeignKey("patient_registration.patient_id"), nullable=False)  #nullable=False #patient_registration.patient_id
    treatment_service_name_id = Column(String(36), ForeignKey("treatment_service_name.id"),nullable=False) #nullable=False
    doctor_profile_id= Column(String(36), ForeignKey("doctor_profile.doctor_id"),nullable=False) #nullable=False # DOCTOR IN CHARGE ### CHANGE COLUMN NAME TO Doctor_profile_id then FK sa HR Dr profile ####
    description = Column(Text)
    quantity= Column(Float,nullable=False)                                                    ### NEW COLUMN ####
    cancellation_return= Column(Float,nullable=False , default='0')  #NEW1 NEW COLUMN 
    room = Column(String(100))                                                                ### NEW COLUMN, YUNG GALING SA TYPES NIYO PO DATI NILIPAT NA PO DITO ####
    session_no = Column(Text)
    session_datetime = Column((DateTime),nullable=False)  #nullable=False                     ### NEED PO NAMIN ITONG DATE (ACTUALLY LAHAT PO) ### 
    drug = Column(Text)
    dose = Column(Text)
    next_schedule = Column(DateTime)
    comments = Column(Text)
    
    status = Column(String(100), default='PENDING', nullable=False) #nullable=False
    is_active = Column(String(100), default='ACTIVE' ,nullable=False) #nullable=False
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    patient = relationship('PatientRegistration', back_populates='treatments')
    physician = relationship('Doctor_profile', back_populates='treatments')  ##NEW1 Doctor_profile
    treatment_name = relationship('TreatmentServiceName', back_populates='treatments') #NEW1 TreatmentServiceName /treatment_name

#GALING SA CMS ITU
class Lab_test_type(Base):
    __tablename__ = "lab_test_types"    
    id = Column(CHAR(36), primary_key=True,index=True)
    lab_test_type_name = Column(String(255),nullable=False,unique=True, index=True)
    description = Column(Text,nullable=False)
  
    status = Column(String(255), nullable=False, server_default="Active")
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)

    #lab_requests = relationship('LabRequest', back_populates='lab_test')   #NEW1 WALA NA DAPAT ITO  #GALING LABREQUEST (DI KO NA TINANGGAL)


## NEW TABLE FOR LAB
class LabServiceName(Base):
    __tablename__ = "lab_service_name"
    id = Column(String(36), primary_key=True,index=True)
    lab_service_name = Column(String(255),nullable=False,unique=True)
    lab_test_types_id = Column(String(36), ForeignKey("lab_test_types.id"),nullable=False)
    unit_price = Column(Float, nullable=False)
  
    status = Column(String(100), default='Active')  #CHANGE TO ACTIVE
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)

    lab_test_types_id_info = relationship(
        "Lab_test_type", primaryjoin="and_(LabServiceName.lab_test_types_id==Lab_test_type.id)" ) #NEW1
   

#LAB REQUEST
class LabRequest(Base):
    __tablename__ = "lab_requests"

    id = Column(String(36), primary_key=True, default=text('UUID()'))
    lab_test_id = Column(String(36), ForeignKey("lab_service_name.id"), nullable=False) #nullable=False                      ### CHANGE FK ###  
    patient_id = Column(String(36), ForeignKey("patient_registration.patient_id"), nullable=False) #nullable=False           ### patient_registration.patient_id
    lab_request_no = Column(String(100), nullable=False)
    quantity= Column(Float,nullable=False , default='1')  #NEW1 NEW COLUMN 
    cancellation_return= Column(Float,nullable=False , default='0')  #NEW1 NEW COLUMN 
    is_active = Column(String(100), default='ACTIVE', nullable=False) #nullable=False                                         
    status = Column(String(100), default='PENDING' ,nullable=False) #nullable=False           ### CHANGE STATUS TO "FOR BILLING" PAG INEDIT NIYO NA YUNG STATUS NA FOR BILLING IBIG SABIHIN PINASA NIYO NA SA AMIN TAPOS HINDI NIYO NA SIYA MAUUPDATE/EDIT/DELETE OKI? ### 
    created_at = Column(DateTime, default=text('NOW()'))                                      ### NEED PO NAMIN ITONG DATE ###
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    # lab_result = relationship('LabResult', back_populates='lab_request') #NEW1 COMMENT KO MUNA
    # lab_test = relationship('LabTest', back_populates='lab_requests')  #NEW1 COMMENT KO MUNA
    # patient = relationship('Patient', back_populates='lab_requests') #NEW1 COMMENT KO MUNA
    
    lab_test_id_info = relationship('LabServiceName', back_populates='')


#GALING CMS /////////////////////////////////////////////////////////////////////// SA TREATMENT NA ITU
class Surgery_type(Base):
    __tablename__ = "surgery_types"    
    id = Column(CHAR(36), primary_key=True,index=True)
    surgery_type_name = Column(String(255),nullable=False,unique=True, index=True)
    description = Column(Text,nullable=False)
                                              
    status = Column(String(255), nullable=False, server_default="Active")
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)

    surgeries = relationship("Surgery",back_populates="surgery_type") #NEW1

# SURGERY
class Surgery(Base):
    __tablename__ = "surgeries"

    id = Column(String(36), primary_key=True, default=text('UUID()'))
    surgery_no = Column(String(100),nullable=False) #nullable=False
    patient_id = Column(String(36), ForeignKey("patient_registration.patient_id"),nullable=False)#nullable=False  ### patient_registration.patient_id
    room = Column(String(100))
    surgery_type_id = Column(String(36), ForeignKey("surgery_types.id"),nullable=False)#nullable=False
    start_time = Column((DateTime),nullable=False)#nullable=False
    end_time = Column((DateTime),nullable=False) #nullable=False
   # head_surgeon_id = Column(String(36), ForeignKey("users.id"))       ### NILIPAT NA SA SURGERY_IN_CHARGE
    description = Column(String(255))
    is_active = Column(String(100), default='ACTIVE')
    status = Column(String(100), default='PENDING' ,nullable=False) #nullable=False                    ### CHANGE STATUS TO "FOR BILLING" PAG INEDIT NIYO NA YUNG STATUS NA FOR BILLING IBIG SABIHIN PINASA NA NIYO SA AMIN TAPOS HINDI NIYO NA SIYA MAUUPDATE/EDIT/DELETE OKI? ### 
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))


    patient = relationship('PatientRegistration', back_populates='surgeries')
    surgery_type = relationship("Surgery_type", back_populates="surgeries") #NEW1 Surgery_type

    in_charge = relationship('SurgeryInCharge', back_populates="surgery")


class SurgeryInCharge(Base):
    __tablename__ = "surgery_in_charge"
    id = Column(CHAR(36), primary_key=True)                                         ### NEW COLUMN 
    dr_in_charge_id = Column(ForeignKey('doctor_profile.doctor_id'),nullable=False) #nullable=False                ### NEW COLUMN FOR DOCTORS PROFILE/ FK ONLY ### 
    head_surgeon_id = Column(String(36),  server_default="No")                      ### NEW COLUMN IF HEAD SURGEON OR NOT ###  
    nurse_charge_id = Column(ForeignKey('users.id'))                                ### GINAWANG FK ONLI :>### 
    surgery_id = Column(ForeignKey('surgeries.id'),nullable=False)#nullable=False  

    status = Column(String(100), default='PENDING' ,nullable=False) #NEW1 NEW COLUMN
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    #  status : str                   #NEW1 BAKIT WALANG GANITO PO
    # created_by : str                #NEW1
    # created_at : datetime           #NEW1
    # updated_by : Optional[str]      #NEW1
    # updated_at : Optional[datetime] #NEW1
                  

    in_charge = relationship('Doctor_profile', back_populates="handled_surgeries")   #NEW1 Doctor_profile
    surgery = relationship("Surgery", back_populates="in_charge")

    #####ADD RELATIONSHIP FOR DOCTORS PROFILE#########
    dr_profile =relationship(
        "Doctor_profile", primaryjoin="and_(SurgeryInCharge.dr_in_charge_id==Doctor_profile.doctor_id)")




#////////////////////////////////////////////////////////////////////////////////////////////////////////////////#
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////#
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////#
#MEDICINE PRESCRIPTION
class Medicine_PR(Base):
    __tablename__= 'medicine_pr'

    medpr_id= Column(CHAR(36), primary_key=True, index=True)
    medicine_no = Column(Integer, nullable=False,unique=True, index=True)
    medicine_id = Column(CHAR(36), ForeignKey("medicines.id"),nullable=True)  #rename
    quantity = Column(Integer,nullable=False)
    cancellation_return= Column(Float,nullable=False , default='0') #NEW2
    # new
    
    intake = Column(String(255),nullable=False)
    frequency = Column(String(255),nullable=False)
    dosage = Column(String(255),nullable=False)
    doctor_prescribed = Column(String(255),nullable=False)
   
    prescription_id = Column(CHAR(36), ForeignKey("prescriptions.prescription_id"),nullable=True)
    med_pres_status = Column(String(255),nullable=False, default="Unpaid")  #NEW rename med_pres_status
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=True)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)

    prescription_info = relationship("Prescription", primaryjoin="and_(Medicine_PR.prescription_id==Prescription.prescription_id)", back_populates="medicines_prescription")
    medicine_info = relationship("Medicine", primaryjoin="and_(Medicine_PR.medicine_id==Medicine.id)", back_populates="med_pr")
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////#
#MEDICINE TABLE 
class Medicine(Base):
     __tablename__= 'medicines'
     id= Column(CHAR(36), primary_key=True, index=True)
     med_product_name = Column (String(255), unique=True,nullable=False)
     med_quantity = Column(Integer,nullable=False)
     med_manufacturer = Column (CHAR(36), nullable=True) #NEW1 ForeignKey("manufacturers.id") comment
     med_manufactured_date = Column (Date,nullable=False)
     med_import_date = Column(Date,nullable=False)
     med_expiration_date = Column (Date,nullable=False)
     med_batch_number = Column (Integer,nullable=False)
     med_unit_price = Column (Float,nullable=False)
     med_tax = Column (Integer,nullable=True)
     med_purpose = Column (String(255),nullable=False)
     condition = Column(String(255),nullable=False, default="no issue")
     status = Column(String(255),nullable=False, default="High")
     dosage = Column (Integer,nullable=False)
     created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=True)
     created_at = Column(DateTime,nullable=False)
     updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
     updated_at = Column(DateTime,nullable=True)

    #  manufacturer_information = relationship("Manufacturer", primaryjoin="and_(Medicine.med_manufacturer==Manufacturer.id)", back_populates="medicine_info")    #NEW1 COMMENT MUNA 
 

     #PRESCRIPTION MEDICINE
     med_pr = relationship("Medicine_PR", primaryjoin="and_(Medicine.id==Medicine_PR.medicine_id)",back_populates="medicine_info")
     #POS MEDICINE
    #  medicine_item = relationship("Medicine_item", primaryjoin="and_(Medicine.id==Medicine_item.medicine_id)", back_populates="medicine_info")                                          #NEW1 COMMENT MUNA


#MEDICAL PRESCRIPTION
class MedicalSupplies_PR(Base):
    __tablename__= 'medicalsupplies_pr'

    medicsupp_prid= Column(CHAR(36), primary_key=True, index=True)
    ms_no = Column(Integer, nullable=False,unique=True, index=True)
    medical_id = Column(CHAR(36), ForeignKey("medicalsupplies.id"),nullable=True)
    quantity = Column(Integer,nullable=False)
    # cancellation_return= Column(Float,nullable=False , default='0') #NEW2
    prescription_id = Column(CHAR(36), ForeignKey("prescriptions.prescription_id"),nullable=True)
    status = Column(String(255),nullable=False, default="Unpaid")
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=True)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)
    prescription_info = relationship("Prescription", primaryjoin="and_(MedicalSupplies_PR.prescription_id==Prescription.prescription_id)", back_populates="medical_prescription")
    medical_info = relationship("MedicalSupplies", primaryjoin="and_(MedicalSupplies_PR.medical_id==MedicalSupplies.id)", back_populates="medical_pr")


#PRESCRIPTION
class Prescription(Base):
    __tablename__= 'prescriptions'

    prescription_id= Column(CHAR(36), primary_key=True, index=True)
    prescription_no = Column(String(255), nullable=False,unique=True, index=True)
    admission_id = Column(CHAR(36),ForeignKey("inpatients.admission_id"),nullable=True)
    outpatient_id = Column(String(255), nullable=True)#, ForeignKey('outpatients.outpatient_id') #NEW
    date_prescribed = Column(Date,nullable=False)
    patient_status = Column(String(255), nullable=True)  #nEW
    status = Column(String(255),nullable=False, default="Unpaid")
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=True)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)


    inpatient_info = relationship("Inpatient", primaryjoin="and_(Prescription.admission_id==Inpatient.admission_id)", back_populates="my_prescriptions")
    medicines_prescription = relationship("Medicine_PR", primaryjoin="and_(Prescription.prescription_id==Medicine_PR.prescription_id)",back_populates="prescription_info")
    medical_prescription = relationship("MedicalSupplies_PR", primaryjoin="and_(Prescription.prescription_id==MedicalSupplies_PR.prescription_id)",back_populates="prescription_info")
   

     
# MEDICAL SUPPLIES TABLE
class MedicalSupplies(Base):
    __tablename__= 'medicalsupplies'
    id= Column(CHAR(36), primary_key=True, index=True)
    ms_product_name = Column (String(255), unique=True,nullable=False)
    ms_quantity = Column(Integer,nullable=False)
    ms_manufacturer = Column (CHAR(36), nullable=True)  #NEW1 ,ForeignKey("manufacturers.id")
    ms_manufactured_date = Column (Date,nullable=False)
    ms_import_date = Column(Date,nullable=False)
    ms_expiration_date = Column (Date,nullable=False)
    ms_batch_number = Column (Integer,nullable=False)
    ms_unit_price = Column (Float,nullable=False)
    ms_tax = Column (Integer,nullable=True)
    ms_purpose = Column (String(255),nullable=False)
    condition = Column(String(255),nullable=False, default="no issue")
    status = Column(String(255),nullable=False, default="High")
    #dosage = Column (Integer,nullable=False)
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=True)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)

    # manufacturer_information = relationship("Manufacturer", primaryjoin="and_(MedicalSupplies.    ms_manufacturer==Manufacturer.id)", back_populates="medicalsupplies_info")                      #NEW1 COMMENT MUNA

   
    # PRESCRIPTION FOR MEDICAL SUPPPLIES
    medical_pr = relationship("MedicalSupplies_PR", primaryjoin="and_(MedicalSupplies.id==MedicalSupplies_PR.medical_id)",back_populates="medical_info")
    #POS Medical
    # medical_item = relationship("Medical_item", primaryjoin="and_(MedicalSupplies.id==Medical_item.medical_id)", back_populates="medical_info")                                                               #NEW1 COMMENT MUNA


#////////////////////////////////////////////////////////////////////////////////////////////////////////////////#
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////#
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////#

#GALING PHARMACY  #HINDI NAGAMIT
# class PharmacyInvoice(Base):
#     __tablename__= 'pharmacy_invoice'

#     id= Column(CHAR(36), primary_key=True, index=True)
#     invoice_no = Column(String(255), nullable=False,unique=True, index=True)
#     admission_id = Column(CHAR(36), ForeignKey("inpatients.admission_id"),nullable=True)  ## inpatients with s po
#     invoice_date = Column(Date,nullable=True)
#     medicine_pr_id= Column(CHAR(36),ForeignKey("medicine_pr.medpr_id"),nullable=True)  #medicalsupplies_pr
#     total_amount= Column(Float, nullable=False)

#     status = Column(String(255),nullable=False, default="Unpaid")
#     created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=True)
#     created_at = Column(DateTime,nullable=False)
#     updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
#     updated_at = Column(DateTime,nullable=True)


#     inpatient_info = relationship("Inpatient", primaryjoin="and_(PharmacyInvoice.admission_id==Inpatient.admission_id)")    ###NEW1 PharmacyInvoice  //Deleted , back_populates="invoice_info"
#     #sales_invoice = relationship("Sales", primaryjoin="and_(Inpatient_invoice.id==Sales.invoice_id)", back_populates="invoice_info")   ### DI NAMIN NEED ITO ##




################################################################################ BILLING
class HospitalServiceName(Base):
    __tablename__ = "hospital_service_name"
    id = Column(String(36), primary_key=True,index=True)
    description_name = Column(String(255),nullable=False,unique=True, index=True)
    unit_price = Column(Float, nullable=False)
    status = Column(String(100), default='Active')
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)

class HospitalServices(Base):
    __tablename__ = "hospital_services"
    id = Column(String(36), primary_key=True,index=True)
    admission_id = Column(String(36), ForeignKey("inpatients.admission_id"),nullable=False)
    hospital_service_name_id = Column(String(36), ForeignKey("hospital_service_name.id"),nullable=False)
    quantity= Column(Float,nullable=False)
    date= Column(Date, nullable=False)
    total_amount= Column(Float,nullable=False)
 
    status = Column(String(100), default='Pending',nullable=False) #nullable=False             ### STATUS TO FOR BILLING ###
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime,nullable=False)
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime,nullable=True)

    hc_treatment_services = relationship(
         "HospitalServiceName", primaryjoin="and_(HospitalServices.hospital_service_name_id==HospitalServiceName.id)")

    admission_info = relationship(
        "Inpatient", primaryjoin="and_(HospitalServices.admission_id==Inpatient.admission_id)")  #NEW1 PatientRegistration

# /////////////////////////////////////////////////////////////////////////////////////////////////////////////
class HospitalChargesBill(Base):
    __tablename__ = "hospital_charges_bill"
    id = Column(CHAR(36), primary_key=True,index=True)
    invoice_no= Column(String(100) ,unique=True)
    invoice_date = Column(DateTime, nullable=False)
    inpatient_bill_id = Column(String(36), ForeignKey("inpatient_bills.id"),nullable=True)
    hospital_services_id = Column(String(36), ForeignKey("hospital_services.id"),nullable=False,unique=True)
    total_amount= Column(Float,nullable=False)
    cancellation_return = Column(Float, nullable=True)

    status = Column(String(100), default='Active')
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())   
    
    hospital_charges_id_info = relationship(
         "HospitalServices", primaryjoin="and_(HospitalChargesBill.hospital_services_id==HospitalServices.id)")  #NEW1 HospitalServices hospital_services_id


class TreatmentBill(Base):
    __tablename__ = "treatment_bill"
    id = Column(CHAR(36), primary_key=True,index=True)
    invoice_no= Column(String(100) ,unique=True)
    invoice_date = Column(DateTime, nullable=False)
    inpatient_bill_id = Column(String(36), ForeignKey("inpatient_bills.id"),nullable=True)

    treatment_id = Column(String(36), ForeignKey("treatments.id"),nullable=False)
    total_amount= Column(Float,nullable=False)
    cancellation_return = Column(Float, nullable=True)

    status = Column(String(255), nullable=False, server_default="Pending")
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())   

    treatment_id_info = relationship(
         "Treatment", primaryjoin="and_(TreatmentBill.treatment_id==Treatment.id)")


class LabRequestBill(Base):
    __tablename__ = "lab_requests_bill"
    id = Column(CHAR(36), primary_key=True,index=True)
    invoice_no= Column(String(100) ,unique=True)
    invoice_date = Column(DateTime, nullable=False)

    inpatient_bill_id = Column(String(36), ForeignKey("inpatient_bills.id"),nullable=True)
    lab_requests_id = Column(String(36), ForeignKey("lab_requests.id"),nullable=False)
    total_amount= Column(Float,nullable=False)
    cancellation_return = Column(Float, nullable=True)

    status = Column(String(255), nullable=False, server_default="Pending")
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())   

    lab_requests_id_info = relationship(
         "LabRequest", primaryjoin="and_(LabRequestBill.lab_requests_id==LabRequest.id)")

class PharmacyBill(Base):
    __tablename__ = "pharmacy_bill"
    id = Column(CHAR(36), primary_key=True,index=True)
    invoice_no= Column(String(100) ,unique=True)
    invoice_date = Column(DateTime, nullable=False)

    inpatient_bill_id = Column(String(36), ForeignKey("inpatient_bills.id"),nullable=True)
    # pharmacy_invoice_id = Column(String(36), ForeignKey("pharmacy_invoice.id"),nullable=False) #NEW1 REMOVED COLUMN
    medpr_id= Column(CHAR(36),ForeignKey("medicine_pr.medpr_id"),nullable=False)  #NEW2 NEW COLUMN
    total_amount= Column(Float,nullable=False)
    cancellation_return = Column(Float, nullable=True  , default=00)

    status = Column(String(255), nullable=False, server_default="Pending")
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())   
    
    # pharmacy_invoice_id_info = relationship(
    #      "PharmacyInvoice", primaryjoin="and_(PharmacyBill.pharmacy_invoice_id==PharmacyInvoice.id)")

    # inpatient_bill_id_info = relationship(
    #     "InpatientBill", primaryjoin="and_(PharmacyBill.inpatient_bill_id==InpatientBill.id)")



class RoomBill(Base):
    __tablename__ = "room_bill"    
    id = Column(CHAR(36), primary_key=True)
    invoice_no = Column(String(100), nullable=False, unique=True)
    invoice_date = Column(DateTime, nullable=False)
    admission_id = Column(String(36), ForeignKey("inpatients.admission_id"), nullable=False) #NEW1 Deleted management
    inpatient_bill_id = Column(String(36), ForeignKey("inpatient_bills.id"), nullable=True)
    total_amount = Column(Float, nullable=False)

    status = Column(String(255), nullable=False, server_default="Pending")
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())   
    
    inpatient_management_id_info = relationship(
         "Inpatient", primaryjoin="and_(RoomBill.admission_id==Inpatient.admission_id)") #NEW1 Management deleted

    # inpatient_bill_id_info = relationship(
    #     "InpatientBill",primaryjoin="and_(RoomBill.inpatient_bill_id==InpatientBill.id)")

class DoctorFeeBill(Base):
    __tablename__ = "doctor_fee_bill"
    id = Column(CHAR(36), primary_key=True,index=True)
    invoice_no= Column(String(100) ,unique=True)
    invoice_date = Column(DateTime, nullable=False)

    inpatient_bill_id = Column(String(36), ForeignKey("inpatient_bills.id"),nullable=True)
    doctor_id = Column(String(36), ForeignKey("doctor_profile.doctor_id"),nullable=False)
    
    actual_pf = Column(Float,nullable=False)
    sc_pwd_discount = Column(Float, nullable=True)
    philhealth = Column(Float, nullable=True)
    discount = Column(Float, nullable=True)
    hmo = Column(Float, nullable=True)
    patient_due = Column(Float, nullable=False)

    status = Column(String(100), default='Active')
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())   
    
    # doctor_fee_id_info = relationship(
    #      "Doctor_fee", primaryjoin="and_(DoctorFeeBill.doctor_fee_id==Doctor_fee.doctor_fee_id)")


class InpatientBill(Base):
    __tablename__ = "inpatient_bills"    
    id = Column(CHAR(36), primary_key=True)
    inpatient_bill_no = Column(String(255), nullable=False, unique=True)
    admission_id = Column(String(36), ForeignKey("inpatients.admission_id"),nullable=False)
    inpatient_payment_id = Column(String(36), ForeignKey("inpatient_payments.id"),nullable=True)

    date_of_billing = Column(Date,nullable=False)
    due_date = Column(Date,nullable=False)
    balance_due = Column(Float, nullable=False, server_default="0")
    status = Column(String(255), nullable=False, server_default="Pending")
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=True)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())

    inpatient_info = relationship(
        "Inpatient", primaryjoin="and_(InpatientBill.admission_id==Inpatient.admission_id)")
    bill_treatments = relationship(
        "TreatmentBill", primaryjoin="and_(InpatientBill.id ==TreatmentBill.inpatient_bill_id)")
    bill_lab_requests = relationship(
        "LabRequestBill", primaryjoin="and_(InpatientBill.id ==LabRequestBill.inpatient_bill_id)")
    bill_pharmacy = relationship(
        "PharmacyBill", primaryjoin="and_(InpatientBill.id ==PharmacyBill.inpatient_bill_id)")
    bill_hospital_charges = relationship(
        "HospitalChargesBill", primaryjoin="and_(InpatientBill.id ==HospitalChargesBill.inpatient_bill_id)")
    bill_room = relationship(
        "RoomBill", primaryjoin="and_(InpatientBill.id ==RoomBill.inpatient_bill_id)")
    # bill_doctor_fee = relationship("DoctorFeeBill")
    bill_doctor_fee = relationship(
        "DoctorFeeBill", primaryjoin="and_(InpatientBill.id ==DoctorFeeBill.inpatient_bill_id)")

    # bill_doctor_fee = relationship("DoctorFeeBill", primaryjoin="and_(InpatientBill.id ==DoctorFeeBill.inpatient_bill_id)")


# FROM CMS
class Inpatient_payment(Base):
    __tablename__ = "inpatient_payments"    
    id = Column(CHAR(36), primary_key=True)
    or_no = Column(String(255),nullable=False,unique=True)
    inpatient_bill_id = Column(CHAR(36), ForeignKey("inpatient_bills.id"), nullable=False)
    total_amount_paid = Column(Float,nullable=False)
    payment_term_id = Column(CHAR(36),  nullable=False)                 #COMMENT KO MUNA ForeignKey("payment_terms.id"),
    date_of_payment = Column(DateTime (timezone=True), nullable=False, server_default=func.now())      
    patient_cash_payment_id = Column(CHAR(36),nullable=True)            #COMMENT KO MUNA , ForeignKey("patient_cash_payments.id")
    patient_check_payment_id = Column(CHAR(36),nullable=True)           #COMMENT KO MUNA , ForeignKey("patient_check_payments.id")
    payment_method_id = Column(CHAR(36),nullable=False)                 #COMMENT KO MUNA , ForeignKey("payment_methods.id")
     
    status = Column(String(255), nullable=False, server_default="Active")
    created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=False)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())
#/////////////////////////////////////////////////////////////////////////////////////////////////////////////////#



class AccountsReceivableLedger(Base):
    __tablename__ = "accounts_receivable_ledgers"    
    id = Column(CHAR(36), primary_key=True)
    inpatient_bill_id = Column(CHAR(36), ForeignKey("inpatient_bills.id"), nullable=False)
    inpatient_payment_id = Column(String(36), ForeignKey("inpatient_payments.id"),nullable=False)
    amount_outstanding = Column(Float, nullable=False, server_default="0")
    days_overdue = Column(Numeric,nullable=False) 
    status = Column(String(255), nullable=False, server_default="Pending")
    created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
    created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
    updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
    updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())









########################################### ACCOUNTS PAYABLE
class Maintenance_provider(Base):
    __tablename__ = 'maintenance_providers'

    maintenance_provider_id = Column(String(36), primary_key=True, default=text('UUID()'))
    maintenance_provider_name = Column(String(255), nullable=True)
    maintenance_provider_contact = Column(String(255), nullable=True)
    maintenance_provider_email = Column(String(255), nullable=True) 
    active_status = Column(String(255), nullable=True, default=('Active'))
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    maintenance = relationship('Maintenance')


class Maintenance(Base):
    __tablename__ = 'maintenances'

    maintenance_id = Column(String(36), primary_key=True, default=text('UUID()'))
    maintenance_provider_id = Column(String(36), ForeignKey('maintenance_providers.maintenance_provider_id'), nullable=False)
    asset_id = Column(String(36), nullable=False) #COMMENT MUNA  ForeignKey('assets.asset_id')
    maintenance_name = Column(String(255), nullable=True)
    maintenance_details = Column(String(255), nullable=True)
    maintenance_cost = Column(Numeric, nullable=True)
    maintenance_day = Column(Integer, nullable=True)
    maintenance_due = Column(DateTime, nullable=True)
    maintenance_completed = Column(DateTime, nullable=True)
    maintenance_repeatable = Column(String(255), nullable=True)
    maintenance_status = Column(String(255), nullable=True)
    remarks = Column(String(255), nullable=True)
    active_status = Column(String(255), nullable=True, default=('Active'))
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))

    Maintenance_provider = relationship('Maintenance_provider', back_populates='maintenance', lazy='joined')



class Maintenance_Report(Base):
    __tablename__ = 'maintenance_reports'

    maintenance_report_id = Column(String(36), primary_key=True, default=text('UUID()'))
    maintenance_id = Column(String(36), ForeignKey('maintenances.maintenance_id'), nullable=False)
    maintenance_cost = Column(Numeric, nullable=True)
    completed_date = Column(DateTime, nullable=True)
    remarks = Column(Text, nullable=True)
    created_at = Column(DateTime, default=text('NOW()'))
    updated_at = Column(DateTime, onupdate=text('NOW()'))
    maintenance_details = relationship('Maintenance', foreign_keys=[maintenance_id], lazy='joined')


    
# class AP_MaintenanceReport(Base):
#     __tablename__ = 'accounts_payable_maintenance_reports'
#     accounts_payable_id = Column(String(36), ForeignKey("accounts_payable_ledgers.id"), nullable=True)
#     maintenance_id = Column(String(36), ForeignKey('maintenance_reports.maintenance_report_id'), nullable=False)

# class AP_PurchaseInvoice(Base):
#     __tablename__ = 'ap_purchase_invoice'
#     accounts_payable_id = Column(String(36), ForeignKey("accounts_payable_ledgers.id"), nullable=True)
#     purchare_order_invoice_id = Column(String(36), ForeignKey('invoice.id'), nullable=False)
    



# class PurchaseOrder(Base):
#     __tablename__ = "purchase_order"

#     id = Column(String(255), primary_key=True)   #default=uuid.uuid4    
#     purchase_order_number = Column(Integer, unique=True, index=True)

#     order_date = Column(Date, nullable=True, index=True)
#     expected_delivery_date = Column(Date, nullable=True, index=True)
    
#     notes = Column(String(255), nullable=True, index=True)
#     status = Column(String(255), nullable=True, index=True,default="active")


#     subtotal = Column(Float, nullable=False, index=True)
#     discount = Column(Float, nullable=False, index=True)
#     tax = Column(Float, nullable=False, index=True)
#     total_amount = Column(Float, nullable=False, index=True)


# class Invoice(Base):
#     __tablename__ = "invoice"
#     id = Column(String(255), primary_key=True)
#     prepared_by = Column(String(255), nullable=True, index=True)
#     message = Column(TEXT, nullable=True, index=True)
    
#     status = Column(String(255), nullable=True, index=True,default="Pending")
#     invoice_date = Column(Date, nullable=False, index=True)
#     due_date = Column(Date, nullable=False, index=True)
#     billing_address = Column(String(255), nullable=False, index=True)
#     amount_paid = Column(String(255), nullable=True, index=True,default=0)
    

#     # relation with vendor
#     purchase_order_id = Column(String(255), 
#         ForeignKey("purchase_order.id", onupdate='CASCADE'),unique=True, nullable=False)

#     purchase_order = relationship("PurchaseOrder")

#     # relation with vendor
#     # created_by = Column(String(255), ForeignKey("vendor.id", onupdate='CASCADE'), nullable=True)
#     # updated_by = Column(String(255), ForeignKey("vendor.id", onupdate='CASCADE'), nullable=True)
#     # creator = relationship("Vendor",foreign_keys=[created_by])
#     # updater = relationship("Vendor",foreign_keys=[updated_by])
     
#     created_by = Column(CHAR(36), ForeignKey("users.id"),nullable=False)
#     created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
#     updated_by = Column(CHAR(36),ForeignKey("users.id"),nullable=True)
#     updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())



class PurchaseOrderVendorPayment(Base):
        __tablename__ = "purchase_order_vendor_payments"    
        id = Column(CHAR(36), primary_key=True)
        or_no = Column(String(255),nullable=False,unique=True)
        purchase_order_vendor_bill_id = Column(CHAR(36),  ForeignKey("purchase_order_invoice.id"), nullable=False)
        total_amount_paid = Column(Float,nullable=False)
        date_of_payment = Column(DateTime (timezone=True), nullable=False, server_default=func.now())

        payment_term_id = Column(CHAR(36), ForeignKey("payment_terms.id"), nullable=False) 
        payment_method_id = Column(CHAR(36),ForeignKey("payment_method.id"),nullable=False) 
        
        status = Column(String(255), nullable=False, server_default="Active")
        
        created_by = Column(CHAR(36), ForeignKey("users.id"), nullable=False)
        created_at = Column(DateTime (timezone=True), nullable=False, server_default=func.now())
        updated_by = Column(CHAR(36), ForeignKey("users.id"), nullable=True)
        updated_at = Column(DateTime (timezone=True), nullable=True, onupdate=func.now())



        #relation with payment method
        payment_method = relationship("PaymentMethod", back_populates="purchase_order_vendor_payments")

        #relation with payment terms
        payment_terms = relationship("PaymentTerms", back_populates="purchase_order_vendor_payments")

       
       