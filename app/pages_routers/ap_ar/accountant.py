from fastapi import APIRouter
from fastapi import Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates


templates = Jinja2Templates(directory="front_end/templates")

accountant_pages = APIRouter()



@accountant_pages.get("/AR_AP/login", response_class=HTMLResponse)
async def login(request: Request):
    return templates.TemplateResponse("login.html", {"request": request})

@accountant_pages.get("/AR_AP/admin/main_dashboard", response_class=HTMLResponse)#, tags=['Main Dashboard'])
async def user_main_dashboard(request: Request):
    return templates.TemplateResponse("internal/ap_ar/main_dashboard/main_dashboard.html", {"request": request})

@accountant_pages.get("/AR_AP/admin/dashboard", response_class=HTMLResponse)
async def dashboard_page(request: Request):
    return templates.TemplateResponse("internal/ap_ar/dashboard.html", {"request": request})

@accountant_pages.get("/AR_AP/admin/employees", response_class=HTMLResponse)
async def employee_page(request: Request):
    return templates.TemplateResponse("internal/ap_ar/employee.html", {"request": request})

@accountant_pages.get("/AR_AP/admin/users", response_class=HTMLResponse)
async def user_page(request: Request):
    return templates.TemplateResponse("internal/ap_ar/user.html", {"request": request})

@accountant_pages.get("/AR_AP/admin/account_settings", response_class=HTMLResponse)
async def account_settings(request: Request):
    return templates.TemplateResponse("internal/ap_ar/account_settings.html", {"request": request})

@accountant_pages.get("/AR_AP/admin/profile", response_class=HTMLResponse)
async def profile(request: Request):
    return templates.TemplateResponse("internal/ap_ar/profile.html", {"request": request})

@accountant_pages.get("/AR_AP/admin/inpatient_bills", response_class=HTMLResponse)
async def method(request: Request):
    return templates.TemplateResponse("internal/ap_ar/inpatient_bill.html", {"request": request})


@accountant_pages.get("/AR_AP/admin/bill", response_class=HTMLResponse)
async def method(request: Request):
    return templates.TemplateResponse("internal/ap_ar/bill.html", {"request": request})


@accountant_pages.get("/AR_AP/admin/fees&rates", response_class=HTMLResponse)
async def fee(request: Request):
    return templates.TemplateResponse("internal/ap_ar/fee.html", {"request": request})