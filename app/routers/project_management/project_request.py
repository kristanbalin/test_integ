from fastapi import APIRouter, Depends, status
from sqlalchemy.orm import Session
from typing import List
from app.repository.project_management import project_request
from ... import database
from app.security import oauth2

from app.schemas.procurement_management.project_request import ProjectRequest,ShowProjectRequest
from app.schemas.human_capital_management.user import User


router = APIRouter(
    prefix="/api/v1/project-request",
    tags=['Project Request']
)
get_db = database.get_db


# get all
@router.get('/status/{status}', response_model=List[ShowProjectRequest])
def get( status,db : Session = Depends(get_db),current_user: User = Depends(oauth2.get_current_user)):#, current_user: User = Depends(oauth2.get_current_user)
    return project_request.get(status,db)


# create
@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: ProjectRequest, db : Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)
    return project_request.create(request, db)


# delete
@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete(id,db : Session = Depends(get_db),current_user: User = Depends(oauth2.get_current_user)):#, current_user: User = Depends(oauth2.get_current_user)
    return project_request.delete(id, db)


# update
@router.put('/{id}',status_code=status.HTTP_202_ACCEPTED)
def update(id, request: ProjectRequest, db : Session = Depends(get_db),current_user: User = Depends(oauth2.get_current_user)):#, current_user: User = Depends(oauth2.get_current_user)
    return project_request.update(id, request, db)


# get one
@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowProjectRequest)
def get_one(id, db : Session = Depends(get_db), current_user: User = Depends(oauth2.get_current_user)):
    return project_request.get_one(id, db)
    