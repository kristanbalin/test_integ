from AR_AP.schemas.discount_privillages import CreateDiscount, ShowDiscount, UpdateDiscount
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository.patient_management import discount_privillages

# from AR_AP.repository.discount_privillages import discount_privillages

router = APIRouter(
    prefix="/discount_privillages",
    tags=['Discount']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowDiscount])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return discount_privillages.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowDiscount])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return discount_privillages.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowDiscount)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return discount_privillages.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowDiscount)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return discount_privillages.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateDiscount, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return discount_privillages.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateDiscount, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return discount_privillages.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return discount_privillages.completed(id,updated_by,db)
