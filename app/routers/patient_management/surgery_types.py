from AR_AP.schemas.surgery_types import CreateSurgery_type, ShowSurgery_type, UpdateSurgery_type
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import surgery_types

# from AR_AP.repository.surgery_types import surgery_types

router = APIRouter(
    prefix="/surgery_types",
    tags=['Surgery type']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowSurgery_type])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return surgery_types.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowSurgery_type])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return surgery_types.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowSurgery_type)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return surgery_types.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowSurgery_type)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return surgery_types.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateSurgery_type, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return surgery_types.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateSurgery_type, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return surgery_types.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def deactivated(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return surgery_types.deactivated(id,updated_by,db)