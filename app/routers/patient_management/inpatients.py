from AR_AP.schemas import inpatient_bills
from AR_AP.schemas.inpatients import CreateInpatient, ShowInpatient, UpdateInpatient
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository.patient_management import inpatients

# from AR_AP.repository.inpatients import inpatients

router = APIRouter(
    prefix="/inpatients",
    tags=['Inpatient']
)

get_db = database.get_db


@router.get('/patient/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowInpatient])
def find_all_patient_without_bill(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return inpatients.find_all_patient_without_bill(db)


@router.get('/room_number/{room_number}', status_code=status.HTTP_200_OK, response_model=List[ShowInpatient])
def find_by_room_number(room_number, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return inpatients.find_by_room_number(room_number, db)

@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowInpatient])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return inpatients.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowInpatient])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return inpatients.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowInpatient)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return inpatients.find_one(id, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateInpatient, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return inpatients.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateInpatient, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return inpatients.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return inpatients.completed(id,updated_by,db)