from AR_AP.schemas.patient_rooms import CreatePatientRoom, ShowPatientRoom, UpdatePatientRoom
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import patient_rooms

# from AR_AP.repository.patient_rooms import patient_rooms

router = APIRouter(
    prefix="/patient_rooms",
    tags=['Patient Room']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowPatientRoom])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return patient_rooms.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowPatientRoom])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return patient_rooms.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowPatientRoom)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return patient_rooms.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowPatientRoom)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return patient_rooms.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreatePatientRoom, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return patient_rooms.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdatePatientRoom, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return patient_rooms.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return patient_rooms.completed(id,updated_by,db)