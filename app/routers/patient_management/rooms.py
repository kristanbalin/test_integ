from AR_AP.schemas.rooms import CreateRoom, ShowRoom, UpdateRoom
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import rooms

# from AR_AP.repository.rooms import rooms

router = APIRouter(
    prefix="/rooms",
    tags=['Room']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowRoom])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return rooms.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowRoom])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return rooms.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowRoom)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return rooms.find_one(id, db)


@router.get('/{room_id}', status_code=status.HTTP_200_OK, response_model=ShowRoom)
# ,current_user:User = Depends(oauth2.get_current_user)):
def find_one_by_room_id(room_id, db: Session = Depends(get_db)):
    return rooms.find_one_by_room_id(room_id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowRoom)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return rooms.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateRoom, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return rooms.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateRoom, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return rooms.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return rooms.completed(id,updated_by,db)