from AR_AP.schemas.insurances import CreateInsurance, ShowInsurance, UpdateInsurance
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository.patient_management import insurances

# from AR_AP.repository.insurances import insurances

router = APIRouter(
    prefix="/insurances",
    tags=['Insurance']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowInsurance])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return insurances.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowInsurance])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return insurances.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowInsurance)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return insurances.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowInsurance)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return insurances.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateInsurance, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return insurances.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateInsurance, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return insurances.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return insurances.completed(id,updated_by,db)