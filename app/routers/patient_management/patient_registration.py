from AR_AP.schemas.patient_registration import CreatePatientRegistration, ShowPatientRegistration, UpdatePatientRegistration
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import patient_registration

# from AR_AP.repository.patient_registration import patient_registration

router = APIRouter(
    prefix="/patient_registration",
    tags=['Patient Registration']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowPatientRegistration])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return patient_registration.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowPatientRegistration])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return patient_registration.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowPatientRegistration)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return patient_registration.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowPatientRegistration)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return patient_registration.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreatePatientRegistration, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return patient_registration.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdatePatientRegistration, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return patient_registration.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return patient_registration.completed(id,updated_by,db)