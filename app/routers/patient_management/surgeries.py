from AR_AP.schemas.surgeries import CreateSurgery, ShowSurgery, UpdateSurgery
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import surgeries

# from AR_AP.repository.surgeries import surgeries

router = APIRouter(
    prefix="/surgeries",
    tags=['Surgery']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowSurgery])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return surgeries.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowSurgery])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return surgeries.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowSurgery)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return surgeries.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowSurgery)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return surgeries.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateSurgery, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return surgeries.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateSurgery, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return surgeries.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return surgeries.completed(id,updated_by,db)