from AR_AP.schemas.pharmacy_bill import CreatePharmacyBill, ShowPharmacyBill, UpdatePharmacyBill
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import pharmacy_bill

# from AR_AP.repository.pharmacy_bill import pharmacy_bill

router = APIRouter(
    prefix="/pharmacy_bill",
    tags=['Pharmacy Bill']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowPharmacyBill])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return pharmacy_bill.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowPharmacyBill])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return pharmacy_bill.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowPharmacyBill)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return pharmacy_bill.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowPharmacyBill)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return pharmacy_bill.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreatePharmacyBill, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return pharmacy_bill.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdatePharmacyBill, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return pharmacy_bill.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return pharmacy_bill.completed(id,updated_by,db)
