from AR_AP.schemas.lab_requests_bill import CreateLabRequestBill, ShowLabRequestBill, UpdateLabRequestBill
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import lab_requests_bill

# from AR_AP.repository.lab_requests_bill import lab_requests_bill

router = APIRouter(
    prefix="/lab_requests_bill",
    tags=['Lab Request Bill']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowLabRequestBill])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return lab_requests_bill.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowLabRequestBill])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return lab_requests_bill.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowLabRequestBill)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return lab_requests_bill.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowLabRequestBill)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return lab_requests_bill.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateLabRequestBill, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return lab_requests_bill.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateLabRequestBill, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return lab_requests_bill.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return lab_requests_bill.completed(id,updated_by,db)
