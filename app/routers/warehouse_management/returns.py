from fastapi import APIRouter, Depends, status
from sqlalchemy.orm import Session
from typing import List
from app.repository.warehouse_management import returns
from ... import database
from app.security import oauth2

from app.schemas.procurement_management.returns import Returns,ShowReturns
from app.schemas.human_capital_management.user import User


router = APIRouter(
    prefix="/api/v1/returns",
    tags=['Returns']
)
get_db = database.get_db


# get all
@router.get('/', response_model=List[ShowReturns])
def get( db : Session = Depends(get_db),current_user: User = Depends(oauth2.get_current_user)):#, current_user: User = Depends(oauth2.get_current_user)
    return returns.get(db)


# create
@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: Returns, db : Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)
    return returns.create(request, db)


# delete
@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete(id,db : Session = Depends(get_db),current_user: User = Depends(oauth2.get_current_user)):#, current_user: User = Depends(oauth2.get_current_user)
    return returns.delete(id, db)


# update
@router.put('/{id}',status_code=status.HTTP_202_ACCEPTED)
def update(id, request: Returns, db : Session = Depends(get_db),current_user: User = Depends(oauth2.get_current_user)):#, current_user: User = Depends(oauth2.get_current_user)
    return returns.update(id, request, db)


# get one
@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowReturns)
def get_one(id, db : Session = Depends(get_db), current_user: User = Depends(oauth2.get_current_user)):
    return returns.get_one(id, db)
    