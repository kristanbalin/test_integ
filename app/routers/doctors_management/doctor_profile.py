from AR_AP.schemas.doctor_profile import CreateDoctor_profile, ShowDoctor_profile, UpdateDoctor_profile
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import doctor_profile

# from AR_AP.repository.doctor_profile import doctor_profile

router = APIRouter(
    prefix="/doctor_profile",
    tags=['Doctor Profile']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowDoctor_profile])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return doctor_profile.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowDoctor_profile])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return doctor_profile.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowDoctor_profile)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return doctor_profile.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowDoctor_profile)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return doctor_profile.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateDoctor_profile, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return doctor_profile.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateDoctor_profile, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return doctor_profile.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return doctor_profile.completed(id,updated_by,db)