from AR_AP.schemas.user import User
from AR_AP.schemas.medicine import CreateMedicine, Medicine, ShowMedicine,UpdateMedicine,DeleteMedicine
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
from sqlalchemy.orm import Session
from ..repository import medicine

router = APIRouter(
    prefix="/medicine",
    tags=['Medicine']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowMedicine])
# 
def datatable(db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicine.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowMedicine])
# ,current_user:User = Depends(oauth2.get_current_user)):
def find_all(db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicine.find_all(db)

@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowMedicine)
def find_one(id, db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicine.find_one(id,db)

@router.post('/', status_code=status.HTTP_201_CREATED, response_model=CreateMedicine)
def create(request: CreateMedicine, db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return medicine.create(request,db)

@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateMedicine, db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicine.update(id,request,db)

@router.delete('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def delete(id,updated_by: str, db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicine.delete(id,updated_by,db)

@router.get('/meds', status_code=status.HTTP_200_OK)
def meds(db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicine.meds(db)