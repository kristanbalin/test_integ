from AR_AP.schemas.user import User
from AR_AP.schemas.medicalsupplies import CreateMedicalSupplies, MedicalSupplies, ShowMedicalSupplies,UpdateMedicalSupplies,DeleteMedicalSupplies
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
from sqlalchemy.orm import Session
from ..repository import medicalsupplies

router = APIRouter(
    prefix="/medicalsupplies",
    tags=['Medical Supply']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowMedicalSupplies])
# 
def datatable(db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicalsupplies.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowMedicalSupplies])
# ,current_user:User = Depends(oauth2.get_current_user)):
def find_all(db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicalsupplies.find_all(db)

@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowMedicalSupplies)
def find_one(id, db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicalsupplies.find_one(id,db)

@router.post('/', status_code=status.HTTP_201_CREATED, response_model=CreateMedicalSupplies)
def create(request: CreateMedicalSupplies, db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return medicalsupplies.create(request,db)

@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateMedicalSupplies, db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicalsupplies.update(id,request,db)

@router.delete('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def delete(id,updated_by: str, db: Session = Depends(get_db),current_user:User = Depends(oauth2.get_current_user)):
    return medicalsupplies.delete(id,updated_by,db)

