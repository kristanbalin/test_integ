from AR_AP.schemas.medicine_pr import CreateMedicine_PR, ShowMedicine_PR, UpdateMedicine_PR
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import medicine_pr

# from AR_AP.repository.medicine_pr import medicine_pr

router = APIRouter(
    prefix="/medicine_pr",
    tags=['Medicine_PR']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowMedicine_PR])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return medicine_pr.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowMedicine_PR])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return medicine_pr.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowMedicine_PR)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return medicine_pr.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowMedicine_PR)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return medicine_pr.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateMedicine_PR, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return medicine_pr.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateMedicine_PR, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return medicine_pr.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return medicine_pr.completed(id,updated_by,db)