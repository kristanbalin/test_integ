from AR_AP.schemas.user import User
from AR_AP.schemas.prescription import ShowPrescription,Prescription,CreatePrescription
from typing import List
from fastapi import APIRouter, Depends, status, HTTPException
from .. import database, oauth2
from sqlalchemy.orm import Session
from ..repository.pharmacy_management import prescription

router = APIRouter(
    prefix="/prescription",
    tags=['Prescription']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[Prescription])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return prescription.datatable(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=Prescription)
def find_one(id, db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return prescription.find_one(id,db)

@router.post('/', status_code=status.HTTP_201_CREATED, response_model=CreatePrescription)
def create(request: CreatePrescription, db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return prescription.create(request,db)
