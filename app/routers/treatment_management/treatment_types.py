from AR_AP.schemas.treatment_types import CreateTreatment_type, ShowTreatment_type, UpdateTreatment_type
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import treatment_types

# from AR_AP.repository.treatment_types import treatment_types

router = APIRouter(
    prefix="/treatment_types",
    tags=['Treatment type']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowTreatment_type])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return treatment_types.datatable(db)

@router.get('/find_all', status_code=status.HTTP_200_OK, response_model=List[ShowTreatment_type])
def find_all(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return treatment_types.find_all(db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowTreatment_type)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return treatment_types.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowTreatment_type)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return treatment_types.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateTreatment_type, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return treatment_types.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateTreatment_type, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return treatment_types.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return treatment_types.completed(id,updated_by,db)