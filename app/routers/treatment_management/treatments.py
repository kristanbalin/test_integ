from AR_AP.schemas.treatments import CreateTreatment, ShowTreatment, UpdateTreatment
from AR_AP.schemas.user import User
from typing import List
from fastapi import APIRouter, Depends, status
from .. import database, oauth2
# , oauth2
from sqlalchemy.orm import Session
from ..repository import treatments

# from AR_AP.repository.treatments import treatments

router = APIRouter(
    prefix="/treatments",
    tags=['Treatment']
)

get_db = database.get_db


@router.get('/datatable', status_code=status.HTTP_200_OK, response_model=List[ShowTreatment])
def datatable(db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return treatments.datatable(db)

@router.get('/find_all_for_billing/{id}', status_code=status.HTTP_200_OK, response_model=List[ShowTreatment])
def find_all_for_billing(id, db: Session = Depends(get_db)):#,current_user:User = Depends(oauth2.get_current_user)):
    return treatments.find_all_for_billing(id, db)


@router.get('/{id}', status_code=status.HTTP_200_OK, response_model=ShowTreatment)
def find_one(id, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return treatments.find_one(id, db)

# @router.get('/find_by_name/{term_name}', status_code=status.HTTP_200_OK, response_model=ShowTreatment)
# def find_by_term_name(term_name:str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
#     return treatments.find_by_term_name(term_name, db)

@router.post('/', status_code=status.HTTP_201_CREATED)
def create(request: CreateTreatment, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return treatments.create(request, db)


@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, request: UpdateTreatment, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return treatments.update(id, request, db)


@router.put('/{id}/{updated_by}', status_code=status.HTTP_200_OK)
def completed(id, updated_by: str, db: Session = Depends(get_db)):#, current_user: User = Depends(oauth2.get_current_user)):
    return treatments.completed(id,updated_by,db)